<?php if(count($list) > 0) : ?>
    <div class="box tm-padding">
        <div class="box-body no-padding">            
            <?php echo $list ?>
        </div>
        <div class="box-footer tm-padding tm-nopadding-right tm-nopadding-bottom text-right">
            <?php echo $pagination ?>
        </div>
    </div>
<?php else : ?>
    <?php $this->load->view('admin/blank'); ?>
<?php endif ?>