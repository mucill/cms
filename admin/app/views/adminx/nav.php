<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <?php foreach($this->menu->access($this->session->userdata('group_id')) as $key => $menu_access) : ?>
                <?php if(count($menu_access['sub']) > 0 ) : ?>
                    <li  class="treeview">
                        <?php if(substr(trim($menu_access['path']), 0, 7) == 'http://') : ?>
                        <a href="<?php echo $menu_access['path'] ?>" target="_blank"><i class="ion <?php echo $menu_access['icon'] ?>"></i><span><?php echo $menu_access['menu']?></span> <i class="fa fa-angle-left pull-right"></i></a>
                        <?php else : ?>
                        <a href="<?php echo ($menu_access['path']!='#')? site_url($menu_access['path']) : $menu_access['path'] ?>"><i class="ion <?php echo $menu_access['icon'] ?>"></i><span><?php echo $menu_access['menu']?></span> <i class="fa fa-angle-left pull-right"></i></a>
                        <?php endif ?>
                         <ul class="treeview-menu">
                            <?php foreach ($menu_access['sub'] as $key => $sub) : ?>
                                <li>
                                    <a href="<?php echo site_url($key) ?>/"><?php echo $sub?></a>
                                </li>                
                            <?php endforeach ?>
                        </ul>
                    </li>
                <?php else : ?>
                <li>
                    <?php if(substr(trim($menu_access['path']), 0, 7) == 'http://') : ?>
                    <a href="<?php echo $menu_access['path'] ?>" target="_blank"><i class="ion <?php echo $menu_access['icon'] ?>"></i><span><?php echo $menu_access['menu']?></span> <i class="fa fa-angle-left pull-right"></i></a>
                    <?php else : ?>
                    <a href="<?php echo ($menu_access['path']!='#')? site_url($menu_access['path']) : '#' ?>"><i class="ion <?php echo $menu_access['icon'] ?>"></i><span><?php echo $menu_access['menu']?></span></a>
                    <?php endif ?>

                </li>
                <?php endif ?>
            <?php endforeach; ?>
        </ul>
    </section>
</aside>