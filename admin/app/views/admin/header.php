<?php if (!$this->ion_auth->logged_in()) { redirect('auth/login', 'refresh'); } ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <link rel="apple-touch-icon" href="<?php echo base_url() .IMG ?>apple-touch-icon.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url() . IMG ?>favicon.png">

    <link rel="stylesheet" href="<?php echo base_url() . PLUGINS ?>bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() . PLUGINS ?>font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url() . PLUGINS ?>Ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url() . PLUGINS ?>animate.css/animate.min.css">
    <link rel="stylesheet" href="<?php echo base_url() . PLUGINS ?>colorbox/colorbox.css">
    <link rel="stylesheet" href="<?php echo base_url() . PLUGINS ?>datepicker/datepicker3.css">
    <link rel="stylesheet" href="<?php echo base_url() . PLUGINS ?>trumbowyg/ui/trumbowyg.min.css">

    <link rel="stylesheet" href="<?php echo base_url() . ADM_CSS ?>AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url() . ADM_CSS ?>skins/skin-black.min.css">
    <?php if(isset($css_files)):
    foreach($css_files as $file): ?><link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; endif; ?>
    <link rel="stylesheet" href="<?php echo base_url() . ADM_CSS?>custom.css">

    <script src="<?php echo base_url() . PLUGINS ?>jquery/jquery.min.js"></script>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-black sidebar-mini">
    <div class="wrapper">
        <header class="main-header">
            <a href="<?php echo site_url('admin') ?>" class="logo">
                <span class="logo-mini"><b>C</b>MS</span>
                <span class="logo-lg"><b>C</b>MS</span>
            </a>

            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <div class="hidden-sm tm-page-title">
                    <h1 class="hidden-xs">
                        <?php echo $title ?>
                        <small><?php echo (isset($title_desc)) ? $title_desc : '' ?></small>
                    </h1>
                </div>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <?php if($this->menu->crud_access('write')) : ?>
                        <?php if(isset($button_link)) : ?>
                        <li class="dropdown action-menu">
                            <a href="<?php echo $button_link ?>">
                            <?php
                            switch($this->uri->segment(2)) {
                                default :
                                echo '<i class="ion ion-plus"></i>';
                                break;

                                case 'add' :
                                case 'update' :
                                echo '<i class="ion ion-arrow-left-c"></i>';
                                break;
                            } ?>
                            </a>
                        </li>
                        <?php endif ?>

                        <?php if(($this->uri->segment(2) != 'add') && ($this->uri->segment(2) != 'update') && ($this->uri->segment(1) != 'dashboard') && (!isset($set_dropdown)) ) : ?>
                        <li class="action-menu">
                            <a style="cursor: pointer;" class="printarea"> <i class="ion ion-printer"></i></a>
                        </li>
                        <!--
                        <li class="dropdown action-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="ion ion-printer"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="#" class="printarea">Current</a></li>
                                <li><a href="<?php echo site_url($this->uri->segment(1).'/prints') ?>">All</a></li>
                            </ul>
                        </li>
                        <li class="dropdown action-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="ion ion-archive"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">CSV</a></li>
                                <li><a href="#">XLS</a></li>
                                <li><a href="#">PDF</a></li>
                            </ul>
                        </li>
                        -->
                        <li class="dropdown action-menu tm-search">
                            <a class="search" style="cursor: pointer;"> <i class="ion ion-search"></i> </a>
                            <form action="<?php echo site_url($this->uri->segment(1)) ?>" method="post" class="animated fadeIn hidden">
                            <input type="text" name="q" id="keywords" placeholder="Search ...">
                            </form>
                        </li>
                        <?php endif ?>
                        <?php endif ?>
                        <!-- User Account Menu -->
                        <li class="dropdown action-menu">
                            <a href="<?php echo site_url('auth/logout')?>" class="logout"> <i class="ion ion-power"></i></a>
                        </li>
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- The user image in the navbar-->
                                <img src="<?php echo base_url() . ADM_IMG ?>user2-160x160.jpg" class="user-image" alt="User Image">
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs"><?php echo $this->header->account_name($this->session->userdata('user_id'))?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header">
                                    <img src="<?php echo base_url() . ADM_IMG ?>user2-160x160.jpg" class="img-circle" alt="User Image">
                                    <p>
                                        <?php echo $this->header->account_name($this->session->userdata('user_id'))?>
                                    </p>
                                </li>
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?php echo site_url('auth/update/' . $this->session->userdata('user_id')) ?>" class="btn tm-btn">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?php echo site_url('auth/logout') ?>" class="btn tm-btn logout">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </div>
            </nav>
        </header>

        <!-- Left side column. contains the logo and sidebar -->
        <?php $this->load->view('admin/nav') ?>
