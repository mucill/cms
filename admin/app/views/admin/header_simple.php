<header class="main-header">
    <a href="<?php echo site_url('admin') ?>" class="logo">
        <span class="logo-mini"><b>R</b>MS</span>
        <span class="logo-lg"><b><?php echo $title ?></b></span>
    </a>

    <nav class="navbar navbar-static-top" role="navigation">
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <?php if($this->menu->crud_access('write')) : ?>
                <?php if(isset($button_link)) : ?>
                <li class="dropdown action-menu">
                    <a href="<?php echo $button_link ?>">
                    <?php 
                    switch($this->uri->segment(2)) {
                        default :
                        echo '<i class="ion ion-plus"></i>'; 
                        break;
                        
                        case 'add' :  
                        case 'update' :  
                        echo '<i class="ion ion-arrow-left-c"></i>'; 
                        break;

                    } ?>
                    </a>
                </li>
                <?php endif ?>
                <?php endif ?>

                <li class="dropdown action-menu tm-search">
                    <a href="#" class="search"> <i class="ion ion-search"></i> </a>
                    <form action="<?php echo current_url() ?>" method="get" class="animated fadeIn hidden">
                    <input type="text" placeholder="Search ...">
                    </form>
                </li>

            </ul>
        </div>
    </nav>
</header>