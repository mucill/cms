<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Redirect</title>
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<link rel="apple-touch-icon" href="<?php echo base_url() .IMG ?>apple-touch-icon.png">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url() . IMG ?>favicon.png">

<style type="text/css">

::selection { background-color: #E13300; color: white; }
::-moz-selection { background-color: #E13300; color: white; }

body {
    background-color: #fff;
    margin: 40px;
    font: 12pt/0.5 normal Helvetica, Arial, sans-serif;
    color: #4F5155;
    text-transform: uppercase;
    padding-top: 75px;
}

a {
    color: #ccc;
    background-color: transparent;
    font-weight: normal;
    font-size: 8px;
    text-decoration: none;
}

h1 {
    color: #444;
    font-family: Helvetica, Arial, serif;
    font-weight: 100;
    font-size: 18pt;
    background-color: transparent;
    letter-spacing: 4px;
}

#container {
    margin: 10px;
    text-align: center;
}

p {
    margin: 0 auto;
    letter-spacing: 5px;
    font-size: 10px;
    width: 50%;
    text-align: center;
    line-height: 2;
}

.btn {
    background-color: #333;
    color: #fff;
    padding:15px 20px;
    border-radius: 4px;
    border: none;
    text-decoration: none;
    font-size: 10px;
    letter-spacing: 4px;
}

</style>
</head>
<body>
<div id="container" class="tm-padding text-center">
    <img src="<?php echo base_url().IMG.'progress.gif' ?>" alt="Welcome" width="240">
    <h1><?php echo $heading ?></h1>
    <p>Redirect Page In <span id="countdown">2</span> seconds</p>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <a href="<?php echo $button_link ?>" id="goback" class="btn">Or Clik Here</a>
</div>
<script src="<?php echo base_url().PLUGINS.'jquery/jquery.min.js' ?>"></script>
<script type="text/javascript">
// Redirect Timer
function doUpdate() {
    $('#countdown').each(function() {
        var count = parseInt($(this).html());
        if (count !== 0) {
            $(this).html(count - 1);
        } else {
            window.location = $('#goback').attr('href');
        }
    });
};
setInterval(doUpdate, 1000);
</script>
</body>
</html>
