<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <?php foreach($this->menu->access($this->session->userdata('group_id')) as $key => $menu_access) : ?>
                <?php if(count($menu_access['sub']) > 0 ) : ?>
                    <li  class="treeview">
                        <a href="<?php echo ($menu_access['path']!='#')? site_url($menu_access['path']) : $menu_access['path'] ?>"><i class="fa <?php echo $menu_access['icon'] ?>"></i><span><?php echo $menu_access['menu']?></span> <i class="fa fa-angle-left pull-right"></i></a>
                         <ul class="treeview-menu">
                            <?php foreach ($menu_access['sub'] as $key => $sub) : ?>
                                <li>
                                    <a href="<?php echo site_url($key)?>/"><?php echo $sub?></a>
                                </li>                
                            <?php endforeach ?>
                        </ul>
                    </li>
                <?php else : ?>
                <li><a href="<?php echo ($menu_access['path']!='#')? site_url($menu_access['path']) : '#' ?>"><i class="fa <?php echo $menu_access['icon'] ?>"></i><span><?php echo $menu_access['menu']?></span></a></li>
                <?php endif ?>
            <?php endforeach; ?>
        </ul>
    </section>
</aside>