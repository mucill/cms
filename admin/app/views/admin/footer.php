      <!-- Main Footer -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          Made with love by <strong>Eddy Subratha</strong>. <a href="https://www.wikiwand.com/en/MIT_License">MIT License</a> 
        </div>
        Thank you for using <a href="http://www.garapic.com">Garapic</a>. 
      </footer>
    </div><!-- ./wrapper -->

    <script src="<?php echo base_url().PLUGINS ?>print-area/demo/jquery.PrintArea.js"></script>

    <script src="<?php echo base_url().PLUGINS ?>bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url().PLUGINS ?>colorbox/jquery.colorbox-min.js"></script>
    <script src="<?php echo base_url().PLUGINS ?>datepicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url() . PLUGINS ?>trumbowyg/trumbowyg.min.js"></script>

    <script src="<?php echo base_url().ADM_JS ?>app.min.js"></script>
    <?php if(isset($js_files)):
    foreach($js_files as $file): ?><script src="<?php echo $file; ?>" type="text/javascript" charset="utf-8"></script>
    <?php endforeach; endif; ?>  
    <?php if(isset($scripts)): ?>
    <script type="text/javascript"><?php echo $scripts; ?></script>
    <?php endif; ?>  
    <script src="<?php echo base_url().ADM_JS ?>custom.js"></script>
  </body>
</html>
