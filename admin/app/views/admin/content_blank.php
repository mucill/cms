<?php if (!$this->ion_auth->logged_in()) { redirect('auth/login', 'refresh'); } ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title ?> - IPCRMS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <link rel="apple-touch-icon" href="<?php echo base_url() .IMG ?>apple-touch-icon.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url() . IMG ?>favicon.png">

    <link rel="stylesheet" href="<?php echo base_url() . PLUGINS ?>bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() . PLUGINS ?>font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url() . PLUGINS ?>Ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url() . PLUGINS ?>animate.css/animate.min.css">
    <link rel="stylesheet" href="<?php echo base_url() . ADM_CSS ?>AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url() . ADM_CSS ?>skins/skin-black.min.css">
    <?php if(isset($css_files)):
    foreach($css_files as $file): ?><link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; endif; ?>
    <link rel="stylesheet" href="<?php echo base_url() . ADM_CSS?>custom.css">
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
    <?php if (isset($header)) { echo $header; } ?>
    <?php echo $content ?>

    <script src="<?php echo base_url().PLUGINS ?>jquery/jquery.min.js"></script>
    <script src="<?php echo base_url().PLUGINS ?>bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url().ADM_JS ?>app.min.js"></script>
    <?php if(isset($js_files)):
    foreach($js_files as $file): ?><script src="<?php echo $file; ?>"></script>
    <?php endforeach; endif; ?>
    <?php if(isset($scripts)): ?>
        <script type="text/javascript"><?php echo $scripts; ?>"></script>
    <?php endif; ?>
    <script src="<?php echo base_url().ADM_JS ?>custom.js"></script>

</body>
</html>

