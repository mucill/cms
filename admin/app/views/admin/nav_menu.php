                    <ul class="nav navbar-nav">
                        <?php if($this->menu->crud_access('write')) : ?>
                        <?php if(isset($button_link)) : ?>
                        <li class="dropdown action-menu">
                            <a href="<?php echo $button_link ?>">
                            <?php 
                            switch($this->uri->segment(2)) {
                                default :
                                echo '<i class="ion ion-plus"></i>'; 
                                break;
                                
                                case 'add' :  
                                case 'update' :  
                                echo '<i class="ion ion-arrow-left-c"></i>'; 
                                break;

                            } ?>
                            </a>
                        </li>
                        <?php endif ?>

                        <?php if(($this->uri->segment(2) != 'add') && ($this->uri->segment(2) != 'update') && ($this->uri->segment(1) != 'dashboard') && (!isset($set_dropdown)) ) : ?>
                        <li class="dropdown action-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="ion ion-printer"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="#" class="printarea">Current</a></li>
                                <li><a href="<?php echo site_url($this->uri->segment(1).'/prints') ?>">All</a></li>
                            </ul>
                        </li>
                        <?php endif ?>

                        <?php if($export) : ?>
                        <li class="dropdown action-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="ion ion-archive"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">CSV</a></li>
                                <li><a href="#">XLS</a></li>
                                <li><a href="#">PDF</a></li>
                            </ul>
                        </li>
                        <?php endif ?>
    
                        <li class="dropdown action-menu tm-search">
                            <a href="#" class="search"> <i class="ion ion-search"></i> </a>
                            <form action="<?php echo current_url() ?>" method="post" class="animated fadeIn hidden">
                            <input type="text" placeholder="Search ...">
                            </form>
                        </li>
                        <?php endif ?>                  
                        <!-- User Account Menu -->
                        <li class="dropdown action-menu">
                            <a href="<?php echo site_url('auth/logout')?>" class="logout"> <i class="ion ion-power"></i></a>
                        </li>
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- The user image in the navbar-->
                                <img src="<?php echo base_url() . ADM_IMG ?>user2-160x160.jpg" class="user-image" alt="User Image">
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs"><?php echo $this->header->account_name($this->session->userdata('user_id'))?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header">
                                    <img src="<?php echo base_url() . ADM_IMG ?>user2-160x160.jpg" class="img-circle" alt="User Image">
                                    <p>
                                        <?php echo $this->header->account_name($this->session->userdata('user_id'))?>
                                    </p>
                                </li>
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?php echo site_url('members/update/' . $this->session->userdata('user_id')) ?>" class="btn tm-btn">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?php echo site_url('auth/logout') ?>" class="btn tm-btn logout">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>

                    </ul>