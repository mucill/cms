<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function get_conf($name = '')
{
	$CI 		=& get_instance();
	$sql 		= "SELECT value FROM cms_config WHERE name='$name'";
	$query 		= $CI->db->query($sql);
	if ($query->num_rows() > 0) {
		$setting	= $query->row();
		return $setting->value;
	}
}

function get_remote_url()
{
	$remote_url 		= $this->get_conf('site_url');
	return $remote_url;
}

function get_remote_path()
{
	$resource_path 		= MEDIA;
	return get_remote_url().$resource_path;
}

function get_user_counter()
{
	$CI 		=& get_instance();
	$count 		= $CI->db->count_all('cms_stats');

	if($count > 0) {
		return $count;
	} else {
		return 0;
	}
}

function get_user_online()
{
	$CI 		=& get_instance();

	/* Define how long the maximum amount of time the session can be inactive. */
	define("MAX_IDLE_TIME", 3);
	if ( $directory_handle = opendir( session_save_path() ) ) {
		$count = 0;
		while ( false !== ( $file = readdir( $directory_handle ) ) ) {
			if($file != '.' && $file != '..'){
				if(time()- fileatime(session_save_path() . '/' . $file) < MAX_IDLE_TIME * 60) {
					$count++;
				}
			}
		}
		$sql = "INSERT IGNORE INTO cms_stats(`sessid`) VALUES ('".session_id()."')  ";
		$CI->db->query($sql);
		closedir($directory_handle);
		return $count;
	} else {
		return false;
	}
}

function arrange_date_format($string_date)
{
    return date($this->get_conf('date_format'), strtotime($string_date));
}

function write_log($log_type, $log_module, $log_msg)
{
    $CI = &get_instance();
    $data = array(
        'flag'          => $log_type,
        'module'        => $log_module,
        'users_id'      => $CI->session->userdata('user_id'),
        'message'       => $log_msg
    );

    $CI->db->insert('cms_logs', $data);
}

function gen_random_string($max_string = 20) {
    $letters = 'ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789';
    $len = strlen($letters);
    $letter = $letters[rand(0, $len - 1)];

    $word = '';
    for ($i = 0; $i < $max_string; $i++) {
        $letter = $letters[rand(0, $len - 1)];
        $word .= $letter;
    }

    return $word;
}

function debug($string, $break = true)
{
    echo '<pre>';
    print_r($string);
    echo '</pre>';
	if($break) exit;
}
