<?php if(validation_errors()) : ?>
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Error</strong>
    <hr>
    <?php echo validation_errors(); ?>
</div>
<?php endif ?>
<form action="<?php echo current_url() ?>" method="POST" class="form-horizontal" role="form" enctype="multipart/form-data">
<input type="hidden" name="id" value="<?php echo $form->pub_id ?> ">
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Please complete the form below.</h3>
    </div>
    <div class="box-body tm-padding ">            
        <div class="form-group tm-nopadding">
            <label class="col-sm-4">Type</label>
            <div class="col-sm-8 col-md-3">
                <?php echo form_dropdown('type', $this->pub_type, set_value('type', $form->pub_type), 'class="form-control"' ) ?>
            </div>
        </div>                              
        <div class="form-group tm-nopadding">
            <label class="col-sm-4">Title</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="title" value="<?php echo set_value('title', $form->pub_title) ?>">
            </div>
        </div>
        <div class="form-group tm-nopadding">
            <label class="col-sm-4">Author <em><small>- use comma for more authors</small></em></label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="author" value="<?php echo set_value('title', $form->pub_partner) ?>">
            </div>
        </div>         
        <div class="form-group tm-nopadding">
            <label class="col-sm-4">ISBN</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" name="isbn" value="<?php echo set_value('isbn', $form->pub_isbn) ?>">
            </div>
        </div>
        <div class="form-group tm-nopadding">
            <label class="col-sm-4">Year of Publication</label>
            <div class="col-sm-3">
                <?php echo form_dropdown('year', array_combine(range(date('Y'), 1965), range(date('Y'), 1965)), set_value('year', $form->pub_year), 'class="form-control"' ) ?>
            </div>
        </div>
        <div class="form-group tm-nopadding">
            <label class="col-sm-4">Link</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="link" value="<?php echo set_value('link', $form->pub_link) ?>" placeholder="http://">
            </div>
        </div>
        <div class="form-group tm-nopadding">
            <label class="col-sm-4">Upload Document</label>
            <div class="col-sm-8 no-padding">
            <input type="file" name="userfile">
            <?php if($form->pub_file != '') : ?>
            <div style="margin-left:15px;">
            <a href="<?php echo '../../' . REPOSITORY . $form->pub_file ?> "><?php echo $form->pub_file ?></a>
            </div>
            </div>
            <?php endif ?>
        </div>
        <div class="form-group tm-nopadding">
            <label class="col-sm-12">Abstract</label>
            <div class="col-sm-12">
                <textarea name="abstract" class="form-control" cols="30" rows="10"><?php echo set_value('abstract', $form->pub_abstract) ?></textarea>     
            </div>
        </div>
    </div>
    <div class="panel-footer text-right">
    <a href="<?php echo site_url('publications') ?>" class="btn tm-btn">Cancel</a>
    <button type="submit" class="btn tm-btn">Submit</button>
    </div>
</div>
</form>