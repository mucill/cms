<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {

    private $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->data['title']        = 'Dashboard';
        $this->data['title_desc']   = 'Summary';

        // for debug purpose
        $this->output->enable_profiler(false);
    }

    public function index()
    {
        $this->data['css_files'] = array(
            base_url() . PLUGINS. "bootstrap-calendar/css/calendar.min.css"
        );

        $this->data['js_files'] = array(
            base_url() . PLUGINS. "underscore/underscore-min.js",
            base_url() . PLUGINS. "bootstrap-calendar/js/calendar.min.js",
            base_url() . PLUGINS. "Chart.js/src/Chart.Core.js",
            base_url() . PLUGINS. "Chart.js/src/Chart.Line.js",
            base_url() . ADM_JS. "pages/dashboard2.js"
        );

        $this->data['blogs']     = $this->db->count_all('page');
        $this->data['members']    = $this->db->count_all('member');
        $this->data['events']     = $this->db->count_all('event');
        $this->data['visitors']      = $this->db->count_all('stats');
        $this->data['visitors_monthly']        = $this->db->where('MONTH(updated_at)', date('m'))->get('stats')->num_rows();
        $this->data['visitors_average']        = ceil($this->db->where('MONTH(updated_at)', date('m'))->get('stats')->num_rows() / $this->db->get('stats')->num_rows());

        $counter = '';

        for($i=1; $i<=12; $i++ ) {
            $this->db->where('MONTH(updated_at)', $i);
            $get_visitor = $this->db->get('stats')->num_rows();
            $counter .= $get_visitor.',';
        }

        $counter = (string)substr($counter,0,-1);

        $this->data['scripts'] = '
          var salesChartData = {
            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Des"],
            datasets: [
              {
                label: "Programs",
                fillColor: "rgb(210, 214, 222)",
                strokeColor: "rgb(210, 214, 222)",
                pointColor: "rgb(210, 214, 222)",
                pointStrokeColor: "#c1c7d1",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgb(220,220,220)",
                data: ['.$counter.']
              }
            ]
          };
        ';

        $this->data['content']  = $this->load->view('dashboard/home', $this->data, TRUE);;
        $this->load->view('admin/content', $this->data);  
    }

}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */