<!-- Info boxes -->
<div class="row">
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-gray"><i class="ion ion-ios-albums-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">BLOGS</span>
				<span class="info-box-number"><?php echo $blogs ?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->

	<!-- fix for small devices only -->
	<div class="clearfix visible-sm-block"></div>

	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-gray"><i class="ion ion-ios-checkmark-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">MEMBERS</span>
				<span class="info-box-number"><?php echo $members ?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->

	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-gray"><i class="ion ion-ios-paper-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Events</span>
				<span class="info-box-number"><?php echo $events ?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->

	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-gray"><i class="ion ion-ios-people-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">VISITOR</span>
				<span class="info-box-number"><?php echo $visitors ?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->
	
</div><!-- /.row -->

<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Visitors</h3>
			</div><!-- /.box-header -->
			<div class="box-body tm-padding">
				<div class="row">
					<div class="col-md-8">
						<div class="chart-responsive">
							<!-- Sales Chart Canvas -->
							<canvas id="salesChart" height="125"></canvas>
						</div><!-- /.chart-responsive -->
					</div><!-- /.col -->
					<div class="col-md-4">
						<div class="progress-group">
							<span class="progress-text" style="font-size:12px; line-height:22px;">Total</span>
							<span class="progress-number" style="font-size:12px; line-height:22px;"><?php echo $visitors ?></span>
							<div class="progress sm">
								<div class="progress-bar" style="width: 80%"></div>
							</div>
						</div><!-- /.progress-group -->
						<div class="progress-group">
							<span class="progress-text" style="font-size:12px; line-height:22px;">This Month</span>
							<span class="progress-number" style="font-size:12px; line-height:22px;"><b><?php echo $visitors_monthly ?></b></span>
							<div class="progress sm">
								<div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
							</div>
						</div><!-- /.progress-group -->
						<div class="progress-group">
							<span class="progress-text" style="font-size:12px; line-height:22px;">Average Per Day</span>
							<span class="progress-number" style="font-size:12px; line-height:22px;"><b><?php echo $visitors_average ?></b></span>
							<div class="progress sm">
								<div class="progress-bar progress-bar-pastel" style="width: 80%; background-color: #ccc"></div>
							</div>
						</div><!-- /.progress-group -->
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- ./box-body -->
		</div><!-- /.box -->
	</div><!-- /.col -->
</div><!-- /.row -->