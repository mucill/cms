<?php if(validation_errors()) : ?>
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Error</strong>
    <hr>
    <?php echo validation_errors(); ?>
</div>
<?php endif ?>
<form action="<?php echo current_url() ?>" method="POST" class="form-horizontal" role="form">
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Please complete the form below.</h3>
    </div>
    <ul class="nav nav-tabs" role="tablist" style="padding-left: 10px;">
        <?php foreach($language as $lang) : ?>
        <li role="presentation" <?php echo ($lang->is_default == 1) ? 'class="active"' : '' ?>><a href="#<?php echo $lang->shortname ?>" aria-controls="<?php echo $lang->shortname ?>" role="tab" data-toggle="tab" style="padding: 25px 15px;"><?php echo $lang->name ?></a></li>
        <?php endforeach ?>
    </ul>
    <div class="box-body tab-content tm-padding">
            <?php if(count($dropdown_groups) > 0 ) : ?>
            <?php foreach($language as $lang) : ?>
            <div class="tab-pane <?php echo ($lang->is_default == 1) ? 'active' : '' ?>" id="<?php echo $lang->shortname ?>">
                <div class="form-group">
                    <label class="col-sm-4">Menu's Name in <?php echo $lang->name ?></label>
                    <div class="col-sm-8">
                    <input type="text" name="name[<?php echo $lang->shortname ?>]" value="<?php echo set_value("name[".$lang->shortname."]") ?>" class="form-control">
                    </div>
                </div>                              
            </div>                              
            <?php endforeach?>
            <?php endif ?>
            <div class="form-group">
                <label for="" class="col-sm-4">Groups</label>
                <div class="col-sm-8">
                <?php 
                if(count($dropdown_groups) > 0 ) {
                    echo form_dropdown('groups', $dropdown_groups, 0, array('class'=>'form-control'));                    
                } else {
                    echo '<p style="padding-top: 10px;">You need a Group to create a new navigation. Let\'s <a href="'.site_url('navgroups/add').'">create a Group</a>.</p>';
                }
                ?>
                </div>
            </div>
            <?php if(count($dropdown_groups) > 0 ) : ?>
            <div class="form-group">
                <label for="" class="col-sm-4">Parents</label>
                <div class="col-sm-8">
                <?php 
                echo form_dropdown('parent', $dropdown_opt, 0, array('class'=>'form-control'));    
                ?>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-4">Link To</label>
                <div class="col-sm-8">
                    <div role="tabpanel">
                         <!-- Nav tabs -->
                         <ul class="nav nav-tabs" role="tablist" id="linkto">
                             <li role="presentation" class="active">
                                 <a href="#page" aria-controls="tab" role="tab" data-toggle="tab">Page</a>
                             </li>
                             <li role="presentation">
                                 <a href="#category" aria-controls="tab" role="tab" data-toggle="tab">Category</a>
                             </li>
                             <li role="presentation">
                                 <a href="#module" aria-controls="tab" role="tab" data-toggle="tab">Module</a>
                             </li>
                             <li role="presentation">
                                 <a href="#web" aria-controls="tab" role="tab" data-toggle="tab">Website</a>
                             </li>
                         </ul>
                        <input type="hidden" name="type" id="type" value="#page">
                         <!-- Tab panes -->
                         <div class="tab-content">
                             <div role="tabpanel" class="tab-pane fade in active" id="page">
                                <?php if(isset($pages)) : ?>
                                <?php echo form_dropdown('page', $pages, set_value('page'), 'class="form-control"') ?>
                                <?php else :?>
                                <p style="padding-top: 10px;">No pages found. You need to <a href="<?php echo site_url('pages/add') ?>">add pages</a> here</p>
                                <?php endif ?>
                             </div>
                             <div role="tabpanel" class="tab-pane fade in" id="category">
                                <?php if(isset($category)) : ?>
                                <?php echo form_dropdown('category', $category, set_value('category'), 'class="form-control"') ?>
                                <?php else :?>
                                <p style="padding-top: 10px;">No categories found. You need to <a href="<?php echo site_url('category/add') ?>">add category</a> here</p>
                                <?php endif ?>
                             </div>
                             <div role="tabpanel" class="tab-pane fade in" id="web">
                                <?php echo form_input('web', set_value('web','http://'), 'class="form-control" placeholder="http://"') ?>
                             </div>
                             <div role="tabpanel" class="tab-pane fade in" id="module">
                                <?php echo form_input('module', set_value('module'), 'class="form-control" placeholder="Example: /gallery/list"') ?>
                             </div>
                         </div>
                     </div> 
                </div>
            </div>         
            <?php endif ?>                     
    </div>
    <div class="panel-footer text-right">
    <a href="<?php echo site_url('nav') ?>" class="btn tm-btn">Cancel</a>
    <button type="submit" class="btn tm-btn">Submit</button>
    </div>
</div>
</form>

<script>
    $('#linkto a').click(function(){
        $('#type').val(($(this).attr('href')));
    })
</script>