<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General extends MX_Controller {

    private $table_name     = 'cms_config';
    private $url            = 'general';
    private $perpage        = 10;
    private $data           = array();
    private $update         = '';
    private $delete         = '';

    public function __construct()
    {
        parent::__construct();

        // for security check
        $this->menu->has_access();

        // for header
        $this->data          = $this->header->button_header();
        $this->data['title'] = 'General Configuration';

        // for button link
        $this->data['button_link'] = (($this->uri->segment(2) == 'add') || ($this->uri->segment(2) == 'update')) ? site_url($this->url) : site_url($this->url).'/add' ;

        // for debug purpose only
        $this->output->enable_profiler(false);
    }

    public function index()
    {
        // security check
        $this->menu->check_access('update');

        $this->data['button_link'] = null;
        $this->data['button_text'] = null;

        // get current data
        $this->db
        ->select('groups')
        ->from($this->table_name)
        ->order_by('groups','desc')
        ->group_by('groups');
        $query = $this->db->get();

        // if no data
        if($query->num_rows() <= 0 ) {
            return show_error('Your data may has been removed or changes by someone.', '' , 'Data Not Found');
            exit;
        }

        // return data
        $this->data['groups']   = $query->result();

        // get value
        $values                 = array();
        $query                  = $this->db->get($this->table_name);
        foreach($query->result() as $key => $form) {  
            $values[$form->groups][$key]['label']     = $form->label;
            $values[$form->groups][$key]['name']      = $form->name;
            $values[$form->groups][$key]['value']     = $form->value;
        }

        $this->data['form'] = $values;

        // debug($values);
        
        // validation rules
        $this->form_validation->set_rules('config[]', 'Value', 'required');

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('general/edit', $this->data, true);
        } else {
            // if not break the rules 
            $config = $this->input->post('config');

            // start transaction
            $this->db->trans_start();
            foreach($config as $index => $values ) {
                $this->db->where('name', $index);
                $this->db->update($this->table_name, array('value' => $values));                
            }
            $this->db->trans_complete();

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('general/edit', $this->data, true);
            } else {
                $direct                     = site_url($this->url);
                $this->data['button_link']  = $direct;
                $this->data['message']      = 'Your record has been updated.';
                $this->data['content']      = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        // make an output
        $this->load->view('admin/content', $this->data);        

    }
}

/* End of file General.php */
/* Location: ./application/modules/admin/controllers/General.php */
