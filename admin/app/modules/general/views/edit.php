<?php if(validation_errors()) : ?>
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Error</strong>
        <hr>
        <?php echo validation_errors(); ?>
    </div>
<?php endif ?>
<form action="<?php echo current_url() ?>" method="POST" class="form-horizontal" role="form">
    <div class="row">
        <div class="box box-default">
            <ul class="nav nav-tabs tm-pills " role="tablist">
                <?php foreach($groups as $key => $group) : ?>
                <li role="presentation" <?php echo ($key == 0) ? 'class="active"' : '' ?>><a href="#<?php echo $group->groups ?>" aria-controls="home" role="tab" data-toggle="tab"><?php echo strtoupper($group->groups) ?></a></li>
                <?php endforeach ?>
            </ul>
            <div class="tab-content tm-padding">
                <?php foreach($groups as $key => $group) : ?>   
                <div role="tabpanel" class="tab-pane tm-nopadding-top <?php echo ($key == 0) ? 'active' : '' ?>" id="<?php echo $group->groups ?>">
                <?php foreach($form[$group->groups] as $config ) : ?>
                    <div class="form-group col-md-4">
                        <label for="site_name"><?php echo $config['label']?></label>
                    </div>
                    <div class="col-md-8">
                        <input type="text" id="site_name" name="config[<?php echo $config['name'] ?>]" value="<?php echo $config['value'] ?>" class="form-control">
                    </div>
                    <div class="clearfix"></div>
                <?php endforeach ?>
                </div>
                <?php endforeach ?>
            </div>
            <div class="panel-footer text-right">
                <button type="submit" class="btn tm-btn">Submit</button>
            </div>
        </div>
    </div>
</form>

