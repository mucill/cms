<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Packages extends MX_Controller {

    private $table_name     = 'cms_modules';
    private $url            = 'packages';
    private $perpage        = 10;
    private $data           = array();
    private $update         = '';
    private $delete         = '';

    public function __construct()
    {
        parent::__construct();

        // for security check
        $this->menu->has_access();

        // for header
        $this->data          = $this->header->button_header();
        $this->data['title'] = 'Modules';

        // for button link
        $this->data['button_link'] = (($this->uri->segment(2) == 'add') || ($this->uri->segment(2) == 'update')) ? site_url($this->url) : site_url($this->url).'/add' ;

        // for debug purpose only
        $this->output->enable_profiler(false);
    }

    public function index( $offset = 0 )
    {
        // pagination 
        $this->db
        ->select('id')
        ->from($this->table_name);
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        // $this->db
        // ->select('id, name, parent, show, path, has_write, has_update, has_delete, orders')
        // ->from($this->table_name)
        // ->order_by('orders, parent')
        // ->limit($this->perpage, $offset);
        // $query = $this->db->get();

        $sql = 'SELECT *
                FROM cms_modules feat
                ORDER BY
                    CASE 
                        WHEN parent = 0 THEN orders
                        ELSE (
                            SELECT orders
                            FROM cms_modules parent 
                            WHERE parent.id = feat.parent
                        ) END,       
                    CASE WHEN parent = 0 THEN 1 END DESC, orders
                LIMIT '.$offset.', '.$this->perpage;
        $query = $this->db->query($sql);

        // if not empty
        if($query->num_rows() > 0) {
            // return to table data
            $sort = array();
            for($i=0; $i < $this->db->count_all($this->table_name); $i++) { $sort[$i] = $i; }

            foreach($query->result() as $list) {
                // check for update access
                if($this->menu->crud_access('update')) {
                    $this->update = '<a href="'.site_url($this->url.'/'.'update'.'/'.$list->id).'" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>';
                }

                // check for delete access
                if($this->menu->crud_access('delete')) {
                    $this->delete = '<a href="'.site_url($this->url.'/'.'delete'.'/'.$list->id).'" class="delete btn btn-xs btn-default"><i class="fa fa-trash"></i></a>';
                }

                $table_row = array(
                                    ($list->parent == 0) ? '<strong>'.$list->name.'</strong>' : '<span class="text-muted">'.$list->name.'</span>', 
                                    $this->getParent($list->parent),
                                    ($list->show) ? 'Show' : '<strong class="text-danger">Hidden</strong>',
                                    '<em>'.$list->path.'</em>',
                                    ($list->has_write) ? '<div class="text-center"><i class="fa fa-check"></i></div>' : '',
                                    ($list->has_update) ? '<div class="text-center"><i class="fa fa-check"></i></div>' : '',
                                    ($list->has_delete) ? '<div class="text-center"><i class="fa fa-check"></i></div>' : '',
                                    form_dropdown('short', $sort, $list->orders, 'class="form-control input-sm short" data-id="'.$list->id.'"'));

                // check for additional link access
                if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                    $table_row = array_merge($table_row, array($this->delete.' '.$this->update));
                }

                $this->table->add_row($table_row);
            }

            // generate table header
            $this->table->set_template(array('table_open' => '<table class="table">'));
            if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                $this->table->set_heading('Modules', 'Parent', 'Show', 'Path', '<div class="text-center">Write</div', '<div class="text-center">Update</div>', '<div class="text-center">Delete</div>', 'Orders', '');
            } else {
                $this->table->set_heading('Modules', 'Parent', 'Show', 'Path', '<div class="text-center">Write</div', '<div class="text-center">Update</div>', '<div class="text-center">Delete</div>', 'Orders');
            }

            // output
            $this->data['list']         = $this->table->generate(); 
            $this->data['content']      = $this->load->view('admin/list', $this->data, TRUE);
        } else {
            // if no data
            $this->data['content']      = $this->load->view('admin/blank', $this->data, TRUE);                    
        }

        $this->data['scripts'] = '
            $(".short").change(function(){
                $.post("'.site_url('packages/menuOrder').'", {"id" : $(this).data("id"), "short" : $(this).val() })
                .done(function(){
                    location.reload();
                });
            })';

        // make an output
        $this->load->view('admin/content', $this->data);
    }

    // Add a new item
    public function add()
    {
        // check for write access
        $this->menu->check_access('write');

        // get parents
        $this->db
        ->select('id, name')
        ->from($this->table_name)
        ->where('parent', '0')
        ->where('show', '1')
        ->order_by('id');
        $parents = $this->db->get();
        $set_parent[] = 'As Parents';
        if($parents->row()) {  
            foreach($parents->result() as $parent) {
                $set_parent[$parent->id] = $parent->name;
            } 
        }

        $this->data['dropdown_opt'] = $set_parent;        
        $orders                     = (int)$this->db->count_all($this->table_name);

        // validation rules
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('parent', 'Parent', 'required');
        $this->form_validation->set_rules('show', 'Show /  Hide', 'required');
        $this->form_validation->set_rules('path', 'Classname', 'required');     
        $this->form_validation->set_rules('icon', 'Icon', 'trim');

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('add', $this->data, true);
        } else {
            // if not break the rules 
            $record =   array(
                        'name'          => $this->input->post('name'), 
                        'parent'        => $this->input->post('parent'), 
                        'show'          => $this->input->post('show'), 
                        'path'          => $this->input->post('path'), 
                        'icon'          => $this->input->post('icon'), 
                        'has_write'     => $this->input->post('has_write'),
                        'has_update'    => $this->input->post('has_update'),
                        'has_delete'    => $this->input->post('has_delete'),
                        'orders'        => $orders
            );

            // start transaction
            $this->db->trans_start();
            $this->db->insert($this->table_name, $record);
            $this->db->trans_complete();

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('add', $this->data, true);
            } else {
                $this->data['message']  = 'Your record has been saved.';
                $this->data['content']  = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        // make an output
        $this->load->view('admin/content', $this->data);        
    }

    //Update one item
    public function update( $id = NULL )
    {
        // security check
        $this->menu->check_access('update');

        // get current data
        $query = $this->db->get_where($this->table_name, array('id'=> $id));

        // if no data
        if($query->num_rows() <= 0 ) {
            return show_error('Your data may has been removed or changes by someone.', '' , 'Data Not Found');
            exit;
        }

        // return data
        $this->data['form']     = $query->row();

        // get parents
        $this->db
        ->select('id, name')
        ->from($this->table_name)
        ->where('parent', '0')
        ->where('show', '1')
        ->order_by('id');
        $parents = $this->db->get();
        $set_parent[] = 'As Parents';
        if($parents->row()) {  
            foreach($parents->result() as $parent) {
                $set_parent[$parent->id] = $parent->name;
            } 
        }

        $this->data['dropdown_opt'] = $set_parent;        

        // validation rules
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('parent', 'Parent', 'required');
        $this->form_validation->set_rules('show', 'Show /  Hide', 'required');
        $this->form_validation->set_rules('path', 'Classname', 'required');     
        $this->form_validation->set_rules('icon', 'Icon', 'trim');

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('edit', $this->data, true);
        } else {
            // if not break the rules 
            $record =   array(
                        'name'          => $this->input->post('name'), 
                        'parent'        => $this->input->post('parent'), 
                        'show'          => $this->input->post('show'), 
                        'path'          => $this->input->post('path'), 
                        'icon'          => $this->input->post('icon'), 
                        'has_write'     => $this->input->post('has_write'),
                        'has_update'    => $this->input->post('has_update'),
                        'has_delete'    => $this->input->post('has_delete')
            );

            // start transaction
            $this->db->trans_start();
            $this->db->where('id', $this->input->post('id'));
            $this->db->update($this->table_name, $record);
            $this->db->trans_complete();

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('edit', $this->data, true);
            } else {
                $direct                     = site_url($this->url);
                $this->data['button_link']  = $direct;
                $this->data['message']      = 'Your record has been updated.';
                $this->data['content']      = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        // make an output
        $this->load->view('admin/content', $this->data);        

    }

    //Delete one item
    public function delete( $id = NULL )
    {
        // security check
        $this->menu->check_access('delete');

        // we need to check 
        // the current priviledges
        $query = $this->db->get_where('cms_priviledges', array('modules_id'=> $id));

        // if no data
        if($query->num_rows() > 0 ) {
            return show_error('This module was used in Priviledges.', '' , 'Process Denied');
            exit;
        }

        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->delete($this->table_name);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $message = 'Fail while removed your record.';
        } else {
            $message = 'Your record has been removed.';
        }

        $this->data['button_link']      = site_url('packages');
        $this->data['message']          = $message;
        $this->data['content']          = $this->load->view('admin/redirect', $this->data, TRUE);
        $this->load->view('admin/content', $this->data);        
    }

    public function getParent($parent_id)
    {   
        $output = '';
        if(isset($parent_id) && ($parent_id != 0)) {
            $this->db
            ->select('name')
            ->from($this->table_name)
            ->where('id', $parent_id);
            $parents = $this->db->get();
            if($parents->num_rows() > 0 ){
                $parent = $parents->row();
                $output = $parent->name;            
            }
        }
        return $output;
    }

    public function menuOrder() 
    {
        $id         = $this->input->post('id');
        $orders     = $this->input->post('short');
        $this->db->where('id', $id);
        $this->db->update($this->table_name, array('orders' => $orders ));
    }

}

/* End of file Packages.php */
/* Location: ./application/modules/admin/controllers/Packages.php */
