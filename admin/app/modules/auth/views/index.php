<?php if(isset($_POST['q']) || $this->uri->total_segments() > 3 ) : ?>
    <p class="well no-shadow">
    <a href="<?php echo site_url($this->uri->segment(1)) ?>" class="btn-link pull-right">Clear Filter</a>
    Found(s) <strong><?php echo count($users) ?></strong> data for <strong class="text-red"><?php echo ($this->uri->segment(3) != null) ? $this->uri->segment(3) : $_POST['q'] ?></strong>
    </p>
<?php endif ?>

<?php if(count($users) > 0) : ?>
    <div class="box tm-padding">
        <div class="box-body no-padding">
            <table class="table">
            	<thead>
				<tr>
					<th><?php echo lang('index_fname_th');?></th>
					<th><?php echo lang('index_lname_th');?></th>
					<th><?php echo lang('index_email_th');?></th>
					<th><?php echo lang('index_groups_th');?></th>
					<th><?php echo lang('index_status_th');?></th>
					<th><?php echo lang('index_action_th');?></th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($users as $user):?>
					<tr>

			            <td><?php echo highlight_phrase($user->first_name, (isset($_POST['q'])) ? $_POST['q'] : '' ,'<span style="color: red; text-decoration: underline;">', '</span>');?></td>
			            <td><?php echo highlight_phrase($user->last_name, (isset($_POST['q'])) ? $_POST['q'] : '' ,'<span style="color: red; text-decoration: underline;">', '</span>');?></td>
			            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
						<td>
							<?php foreach ($user->groups as $group):?>
								<?php echo anchor("/groups/update/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')) ;?><br />
			                <?php endforeach?>
						</td>
						<td><?php echo ($user->active) ? anchor("/auth/deactivate/".$user->id, lang('index_active_link'), 'class="activate"') : anchor("/auth/activate/". $user->id, lang('index_inactive_link'), 'class="activate"');?></td>
						<td><?php echo anchor("/auth/update/".$user->id, 'Edit') ;?></td>
					</tr>
				<?php endforeach;?>
				</tbody>
            </table>
        </div>
        <div class="box-footer tm-padding tm-nopadding-right tm-nopadding-bottom text-right">
        <?php echo @$pagination ?>
        </div>
    </div>
<?php else : ?>
    <?php $this->load->view('admin/blank');?>
<?php endif ?>

<script>
    $('.activate').click(function(){
        var set_status = '';
        if(( $(this).text() == 'Inactive')) {
            if(confirm('Are you sure to activated this account ?')) {
                return true;
            } else {
                return false
            }
        }
        return true;
    });
</script>
