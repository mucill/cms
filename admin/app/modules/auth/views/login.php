<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <link rel="apple-touch-icon" href="<?php echo base_url() .IMG ?>apple-touch-icon.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url() . IMG ?>favicon.png">

    <link rel="stylesheet" href="<?php echo base_url() . PLUGINS ?>bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() . PLUGINS ?>Ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url() . ADM_CSS ?>AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url() . ADM_CSS ?>custom.css">
    <style>

    .frontpage {
        margin-top: 15px;
        background-color: #fff !important;
        color: #ccc;
        font-size: 12px;
        box-shadow: none !important;
    }

    .frontpage:focus,
    .frontpage:hover {
        color: #000 !important;
    }

    footer {
        text-align: center;
        font-size: 10pt;
        color: #444;
    }

    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url() .PLUGINS ?>jquery/jquery.min.js"></script>

</head>
<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="<?php echo base_url() ?>"><b>LOG</b>IN</a>
        </div><!-- /.login-logo -->
        <div class="login-box-body">
            <?php if($message) : ?>
            <div class="alert alert-danger tm-alert">
            <small><?php echo $message;?></small>
            </div>
            <?php endif ?>
            <?php echo form_open("auth/login");?>
                <div class="form-group has-feedback">
                    <?php echo form_input($identity,'', 'class="form-control" placeholder="Email"') ?>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <?php echo form_input($password,'','class="form-control" placeholder="Password"');?>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                        <a href="<?php echo site_url('../../') ?>" class="btn btn-primary btn-block btn-flat frontpage">Back to Frontpage</a>
                    </div><!-- /.col -->
                </div>
            <?php echo form_close();?>
        </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
    <footer>&copy; Garapic 2016</footer>
    <script> $('#identity').focus(); </script>
</body>
</html>
