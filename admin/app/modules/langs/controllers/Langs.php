<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Langs extends MX_Controller {

    private $table_name     = 'cms_lang';
    private $url            = 'langs';
    private $perpage        = 10;
    private $data           = array();
    private $update         = '';
    private $delete         = '';

    public function __construct()
    {
        parent::__construct();

        // for security check
        $this->menu->has_access();

        // for header
        $this->data          = $this->header->button_header();
        $this->data['title'] = 'Language';

        // for button link
        $this->data['button_link'] = (($this->uri->segment(2) == 'add') || ($this->uri->segment(2) == 'update')) ? site_url($this->url) : site_url($this->url).'/add' ;

        // for debug purpose only
        $this->output->enable_profiler(false);
    }

    public function index( $offset = 0 )
    {
        // pagination 
        $this->db
        ->select('id')
        ->from($this->table_name);
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('id, shortname, name, is_default, updated_at')
        ->from($this->table_name)
        ->order_by('name')
        ->limit($this->perpage, $offset);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {
            foreach($query->result() as $list) {
                // check for update access
                if($this->menu->crud_access('update')) {
                    $this->update = '<a href="'.site_url($this->url.'/'.'update'.'/'.$list->id).'" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>';
                }

                // check for delete access
                if($this->menu->crud_access('delete')) {
                    if($list->is_default == 0 ) {
                        $this->delete = '<a href="'.site_url($this->url.'/'.'delete'.'/'.$list->id).'" class="delete btn btn-xs btn-default"><i class="fa fa-trash"></i></a>';                    
                    } else {
                        $this->delete = '';
                    }
                }

                // return to table data
                $table_row = array(
                    $list->name,
                    $list->shortname,
                    ($list->is_default) ? 'Default' : '<a href="#" class="default" data-id="'.$list->id.'">Set As Default</a>' , 
                    $list->updated_at
                );

                // check for additional link access
                if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                    $table_row = array_merge($table_row, array($this->delete.' '.$this->update));
                }

                $this->table->add_row($table_row);
            }

            // generate table header
            $this->table->set_template(array('table_open' => '<table class="table">'));
            if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                $this->table->set_heading('Language', 'Shortname', 'Default', 'Updated', '');
            } else {
                $this->table->set_heading('Language', 'Shortname', 'Default', 'Updated');
            }

            // output
            $this->data['list']         = $this->table->generate(); 
            $this->data['content']      = $this->load->view('admin/list', $this->data, TRUE);
        } else {
            // if no data
            $this->data['content']      = $this->load->view('admin/blank', $this->data, TRUE);                    
        }

        // script
        $this->data['scripts'] = '
            $(".default").click(function(){
                $.post("'.site_url('langs/ajax_default').'",{ "id" : $(this).data("id")})
                .done(function(){
                    location.reload();
                })
            });
        ';

        // make an output
        $this->load->view('admin/content', $this->data);
    }

    // Add a new item
    public function add()
    {
        // check for write access
        $this->menu->check_access('write');

        // validation rules
        $this->form_validation->set_rules("langs", "Name of language", 'required');
        $this->form_validation->set_rules("short", "Shortname", 'required');

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('langs/add', $this->data, true);
        } else {
            // save to category first
            $record = array(
                'name'          => $this->input->post('langs'),
                'shortname'     => $this->input->post('short'),
                'updated_at'    => date('Y-m-d h:i:s')
            );

            // start transaction
            $this->db->trans_start();
            $this->db->insert($this->table_name, $record);
            $id = $this->db->insert_id();

            if($this->input->post('default') != null ) {
                $this->db->update($this->table_name, array('is_default' => 0));
                $this->db->where('id', $id);
                $this->db->update($this->table_name, array('is_default' => 1));                                
            }

            $this->db->trans_complete();

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('langs/add', $this->data, true);
            } else {
                $this->data['message']  = 'Your record has been saved.';
                $this->data['content']  = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        // make an output
        $this->load->view('admin/content', $this->data);        
    }

    // Add a new item
    public function update($id = null)
    {
        // check for write access
        $this->menu->check_access('update');

        $this->db
        ->from($this->table_name)
        ->where('id', $id);
        $query = $this->db->get();

        // return data
        $this->data['form']     = $query->row();

        // validation rules
        $this->form_validation->set_rules("langs", "Name of language", 'required');
        $this->form_validation->set_rules("short", "Shortname", 'required');

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('langs/edit', $this->data, true);
        } else {
            // save to category first
            $record = array(
                'name'          => $this->input->post('langs'),
                'shortname'     => $this->input->post('short'),
                'updated_at'    => date('Y-m-d h:i:s')
            );

            // start transaction
            $this->db->trans_start();
            $this->db->where('id', $id);
            $this->db->update($this->table_name, $record);

            if($this->input->post('default') != null ) {
                $this->db->update($this->table_name, array('is_default' => 0));
                $this->db->where('id', $id);
                $this->db->update($this->table_name, array('is_default' => 1));                                
            }

            $this->db->trans_complete();

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('langs/edit', $this->data, true);
            } else {
                $this->data['message']  = 'Your record has been saved.';
                $this->data['content']  = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        // make an output
        $this->load->view('admin/content', $this->data);        
    }


    //Delete one item
    public function delete( $id = NULL )
    {
        // security check
        $this->menu->check_access('delete');

        $this->data['button_link'] = site_url($this->url);

        // check for language
        $this->db
        ->where('is_default', '1')
        ->where('id', $id);
        $query = $this->db->get($this->table_name);

        if($query->num_rows() > 0) {
            $message = 'Cannot remove default language';
        } else {        
            $this->db->trans_start();
            $this->db
            ->where('is_default', '0')
            ->where('id', $id);
            $this->db->delete($this->table_name);
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $message = 'Failed while removed your record.';
            } else {
                $message = 'Your record has been removed.';
            }
        }
        $this->data['message']          = $message;
        $this->data['content']          = $this->load->view('admin/redirect', $this->data, TRUE);
        $this->load->view('admin/content', $this->data);        
    }

    public function ajax_default()
    {
        // set all is_default value to 0
        $this->db->update($this->table_name, array('is_default' => 0));

        // update selected is_default
        $this->db->where('id', $this->input->post('id'));
        $this->db->update($this->table_name, array('is_default' => 1));
        echo $this->input->post('id');
    }
}

/* End of file Category.php */
/* Location: ./application/modules/admin/controllers/Category.php */
