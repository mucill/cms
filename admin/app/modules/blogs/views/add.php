<?php if(validation_errors()) : ?>
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Error</strong>
        <hr>
        <?php echo validation_errors(); ?>
    </div>
<?php endif ?>
<form action="<?php echo current_url() ?>" method="POST" class="form-horizontal" role="form" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-8">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Content</h3>
                </div>
                <ul class="nav nav-tabs" role="tablist" style="padding-left: 10px;">
                    <?php foreach($language as $lang) : ?>
                        <li role="presentation" <?php echo ($lang->is_default == 1) ? 'class="active"' : '' ?>><a href="#<?php echo $lang->shortname ?>" aria-controls="<?php echo $lang->shortname ?>" role="tab" data-toggle="tab" style="padding: 25px 15px;"><?php echo $lang->name ?></a></li>
                    <?php endforeach ?>
                </ul>
                <div class="box-body tm-padding tm-nopadding-top tm-nopadding-bottom tab-content">
                    <?php foreach($language as $lang) : ?>
                        <div class="tab-pane <?php echo ($lang->is_default == 1) ? 'active' : '' ?>" id="<?php echo $lang->shortname ?>">

                            <div class="form-group">
                                <label>Title in <?php echo $lang->name ?></label>
                                <input type="text" id="title_<?php echo $lang->shortname ?>" name="title[<?php echo $lang->shortname ?>]" class="form-control" value="<?php echo set_value("title[".$lang->shortname."]") ?>" />
                            </div>

                            <div class="form-group">
                                <label>URL Alias</label>
                                <div class="input-group">
                                    <span class="input-group-addon" style="color: #eee"><?php echo base_url('read') ?>/</span>
                                    <input type="text" id="slug_<?php echo $lang->shortname ?>" name="slug[<?php echo $lang->shortname ?>]" class="form-control" value="<?php echo set_value("slug[".$lang->shortname."]") ?>" />
                                </div>
                            </div>

                            <div class="form-group required">
                                <label>Content in <?php echo $lang->name ?></label>
                                <div class="summernote-wrapper">
                                    <textarea spellcheck="false" name="body[<?php echo $lang->shortname ?>]" id="body[<?php echo $lang->shortname ?>]" class="form-control editor" rows="10"><?php echo set_value("body[".$lang->shortname."]") ?></textarea>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label>Summary in <?php echo $lang->name ?></label>
                                <textarea spellcheck="false" name="intro[<?php echo $lang->shortname ?>]]" class="form-control" rows="3"><?php echo set_value("intro[".$lang->shortname."]") ?></textarea>
                            </div>

                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Featured Photo</h3>
                </div>
                <div class="panel-body">
                    <div class="full-width text-center">
                        <input type="hidden" id="file_id" name="file_id" value="">
                        <img class="img img-responsive" id="featured_attach" style="margin-bottom:20px;" src="">
                        <button type="button" data-toggle="modal" data-target="#attach" class="btn btn-rounded btn-inverse btn-cons">Browse</button>
                        <button type="button" id="remove-img" class="btn btn-rounded btn-inverse btn-cons hidden">Remove</button>
                    </div>
                </div>
            </div>
            <div class="box box-default" >
                <div class="box-header with-border">
                    <h3 class="box-title <?php if(form_error('category') != '') :?>text-white<?php endif ?>">Category</h3>
                </div>
                <div class="box-body tm-padding tm-nopadding-top tm-nopadding-bottom">
                    <div class="form-group">

                        <?php foreach($categories as $cat): ?>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="category" id="radio<?php echo $cat->id ?>" value="<?php echo $cat->id ?>" <?php echo ($cat->is_default == 1) ? 'checked' : '' ?>>
                                    <div style="padding-top: 3px;"><?php echo $cat->name ?></div>
                                </label>
                            </div>
                        <?php endforeach ?>

                    </div>
                </div>
            </div>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Published
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="input-group date" >
                                <input type="text" class="form-control datepicker" name="published" value="<?php echo mdate('%m/%d/%Y') ?>">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <input type="text" class="form-control timepicker" name="published_time" value="<?php echo mdate('%h:%i %A') ?>">
                        </div>
                    </div>
                    <br>
                    <?php echo form_dropdown('status', array('draft' => 'Draft', 'publish' => 'Published'), set_value('status'), 'class="form-control"') ?>
                </div>
            </div>
            <?php echo form_hidden('id', $this->uri->segment(3)) ?>
            <div class="row">
                <div class="col-md-6"><a href="<?php echo site_url('blogs') ?>" class="btn tm-btn btn-block">Cancel</a></div>
                <div class="col-md-6"><button type="submit" class="btn tm-btn btn-block">Submit</button></div>
            </div>

        </div>

    </div>
</form>

<div class="modal fade bs-example-modal-lg" id="attach" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?php echo $content_file ?>
        </div>
    </div>
</div>

<script>
    $('.tm-action').hide();

    <?php foreach($language as $lang) : ?>
    $('#title_<?php echo $lang->shortname ?>').keyup(function(){
        $('#slug_<?php echo $lang->shortname ?>').val(slug($(this).val()));
    })
    <?php endforeach ?>

    $('#remove-img').click(function(){
        $('#file_id').val('');
        $('#featured_attach').attr('src','').removeAttr('style');
        $('#remove-img').addClass('hidden');
    });

    $('#attach form').submit(function(e){
        e.preventDefault();
        $.ajax({
            url: "ajax_attach", // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,        // To send DOMDocument or non processed data file it is set to false
            success: function(data)   // A function to be called if request succeeds
            {
                var obj = $.parseJSON(data);
                $('#file_id').val(obj.attach_id);
                $('#featured_attach').attr('src', obj.attach_file).attr('style', 'padding-bottom:20px;');
                $('#remove-img').removeClass('hidden');
                $('#attach form').trigger("reset");
                $.get("<?php echo site_url('files/ajaxindex') ?>", function(data){
                    $('#files').html(data);
                });
            }
        });
    });

    $('#files a').each(function(index){
        $(this).click(function(){
            id  = $(this).attr('data-id');
            src = $(this).parent('td').parent().children('td').children('div').children('img').attr('src');
            $('#file_id').val(id);
            $('#featured_attach').attr('src', src).attr('style','margin-bottom:20px;');
            $('#remove-img').removeClass('hidden');
        });
    });
</script>
