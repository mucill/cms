<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blogs extends MX_Controller {

    private $table_name     = 'page';
    private $url            = 'blogs';
    private $perpage        = 10;
    private $data           = array();
    private $update         = '';
    private $delete         = '';

	public function __construct()
	{
		parent::__construct();

        // for security check
		$this->menu->has_access();

        // load library
        $this->load->library('upload');

        // for header
        $this->data          = $this->header->button_header();
        $this->data['title'] = 'Blogs';

        // for button link
        $this->data['button_link'] = (($this->uri->segment(2) == 'add') || ($this->uri->segment(2) == 'update')) ? site_url($this->url) : site_url($this->url).'/add';

        //for debug set as true
        $this->output->enable_profiler(false);
	}

	// -----------------------------------------------------------------------------------
	// List
	// -----------------------------------------------------------------------------------
    public function index( $offset = 0 )
    {
        // pagination
        $this->db
        ->select('id')
        ->from($this->table_name)
        ->where('type','post');

        if($this->session->userdata('group_id') !== 0) {
            $this->db->where('users_id', $this->session->userdata('user_id'));
        }

        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('a.id, a.status, a.updated_at, b.title, b.intro, d.name category')
        ->from($this->table_name . ' a')
        ->join('page_lang b', 'b.id = a.id', 'left')
        ->join('category c', 'c.id =  a.category_id', 'left')
        ->join('category_lang d', 'd.id =  c.id', 'left')
        ->where('b.lang', $this->session->userdata('lang'))
        ->where('a.type','post')
        ->order_by('a.updated_at')
        ->group_by('a.id')
        ->limit($this->perpage, $offset);

        if($this->session->userdata('group_id') !== 0) {
            $this->db->where('users_id', $this->session->userdata('user_id'));
        }

        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {
            foreach($query->result() as $list) {
                // check for update access
                if($this->menu->crud_access('update')) {
                    $this->update = '<a href="'.site_url($this->url.'/'.'update'.'/'.$list->id).'" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>';
                }

                // check for delete access
                if($this->menu->crud_access('delete')) {
                    $this->delete = '<a href="'.site_url($this->url.'/'.'delete'.'/'.$list->id).'" class="delete btn btn-xs btn-default"><i class="fa fa-trash"></i></a>';
                }

                // return to table data
                $table_row = array(
                    $list->title,
                    $list->category,
                    $list->status,
                );

                // check for additional link access
                if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                    $table_row = array_merge($table_row, array($this->delete.' '.$this->update));
                }

                $this->table->add_row($table_row);
            }

            // generate table header
            $this->table->set_template(array('table_open' => '<table class="table">'));
            if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                $this->table->set_heading('Title', 'Category', 'Status', '');
            } else {
                $this->table->set_heading('Title', 'Category', 'Status');
            }

            // output
            $this->data['list']         = $this->table->generate();
            $this->data['content']      = $this->load->view('admin/list', $this->data, TRUE);
        } else {
            // if no data
            $this->data['content']      = $this->load->view('admin/blank', $this->data, TRUE);
        }

        // make an output
        $this->load->view('admin/content', $this->data);
    }

    public function add()
    {
        // check for write access
        $this->menu->check_access('write');

        // get language list
        $this->db->order_by('is_default','desc');
        $query = $this->db->get('lang');
        $this->data['language'] = $query->result();

        // get category
        $this->db
        ->select('a.id, a.name, b.is_default')
        ->from('category_lang a')
        ->join('category b', 'a.id = b.id', 'left')
        ->where('b.type', 'post')
        ->where('a.lang', $this->session->userdata('lang'));
        $query                      = $this->db->get();
        $this->data['categories']   = $query->result() ;

        // get folder
        $this->db
        ->select('a.id, b.name, b.slug')
        ->from('category a')
        ->join('category_lang b', 'b.id = a.id', 'left')
        ->where('a.type', 'folder')
        ->group_by('a.id');
        $query  = $this->db->get();
        $this->data['folder'] = $query->result();
        foreach($this->data['folder'] as $fold) {
            $this->data['folders'][$fold->id] = $fold->name;
        }

        // get files
        $media_perpage  = 5;
        $off            = (isset($offset)) ? $offset : 0;

        // pagination
        $this->db
        ->select('a.id')
        ->from('media a')
        ->join('media_lang b', 'b.id = a.id', 'left')
        ->group_by('a.id');
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $media_perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url('/files/ajaxindex'), $count, $media_perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        $this->db
        ->select('a.id, a.filename, a.size, a.alias, b.alt')
        ->from('media a')
        ->join('media_lang b', 'b.id = a.id', 'left')
        ->order_by('a.updated_at')
        ->limit($media_perpage, $off)
        ->group_by('a.id');
        $query = $this->db->get();
        $this->data['files'] = $query->result();

        // load view
        $this->data['media_upload']     = $this->load->view('files/add', $this->data, true);
        $this->data['media_list']       = $this->load->view('files/list', $this->data, true);
        $this->data['content_file']     = $this->load->view('files/upload', $this->data, true);

        $this->data['js_files'] = array(
            base_url() . PLUGINS. 'twbs-pagination/jquery.twbsPagination.min.js',
            base_url() . PLUGINS. 'rumbowyg/trumbowyg.min.js'
        );

        $this->data['scripts'] = '
            $(".editor").trumbowyg({
                btns: [
                    ["viewHTML"],
                    ["formatting"],
                    "btnGrp-semantic",
                    ["superscript", "subscript"],
                    ["link"],
                    ["insertImage"],
                    "btnGrp-justify",
                    "btnGrp-lists",
                    ["removeformat"],
                    ["fullscreen"]
                ]
            });
            $(".pagination").twbsPagination({
                totalPages: '.ceil($count / $media_perpage).',
                visiblePages: 2,
                onPageClick: function (event, page) {
                    $.post("'.site_url('files/ajaxindex').'/" + page, function(data) {
                        $("#files").html(data);
                    })
                }
            });';

        // validation rules
        foreach($this->data['language'] as $lang) {
            $this->form_validation->set_rules("title[".$lang->shortname."]", "Title in ".$lang->name, 'required');
        }

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('add', $this->data, true);
        } else {
            $date = date("Y-m-d H:i:s", strtotime(set_value('published').' '.set_value('published_time')));

            // Bila validasi sukses
            $data = array(
                'category_id'   => set_value('category'),
                'status'        => set_value('status'),
                'media_id'      => set_value('file_id'),
                'updated_at'    => $date,
                'users_id'      => $this->session->userdata('user_id'),
                'type'          => 'post'
            );
            // debug($data);

            if ($this->db->insert('page', $data)) {
                $id = $this->db->insert_id();
                foreach($this->input->post('title') as $key => $val) {
                    write_log('BLOGS_ADD', 'blogs', 'Add a new post with title '.$this->input->post('title')[$lang->shortname]);
                    $record = array(
                            'lang'  => $key,
                            'id'    => $id,
                            'title' => $this->input->post('title')[$key],
                            'intro' => $this->input->post('intro')[$key],
                            'body'  => html_entity_decode($this->input->post('body')[$key], ENT_QUOTES, 'UTF-8'),
                            'slug'  => $this->input->post('slug')[$key]
                    );
                    $this->db->insert('page_lang', $record);
                }

                $this->data['message']  = 'Your record has been saved.';
                $this->data['content']  = $this->load->view('admin/redirect', $this->data, true);

            } else {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('category/add', $this->data, true);
            }

            $this->data['content']  = $this->load->view('admin/redirect', $this->data, true);
        }

        // make an output
        $this->load->view('admin/content', $this->data);

    }

    public function update( $id = NULL )
    {
        // security check
        $this->menu->check_access('update');

        // get language list
        $this->db->order_by('is_default','desc');
        $query = $this->db->get('lang');
        $this->data['language'] = $query->result();

        // get category
        $this->db
        ->select('a.id, a.name, b.is_default')
        ->from('category_lang a')
        ->join('category b', 'a.id = b.id', 'left')
        ->where('b.type', 'post')
        ->where('a.lang', $this->session->userdata('lang'));
        $query                      = $this->db->get();
        $this->data['categories']   = $query->result() ;

        // get folder
        $this->db
        ->select('a.id, b.name, b.slug')
        ->from('category a')
        ->join('category_lang b', 'b.id = a.id', 'left')
        ->where('a.type', 'folder')
        ->group_by('a.id');
        $query  = $this->db->get();
        $this->data['folder'] = $query->result();
        foreach($this->data['folder'] as $fold) {
            $this->data['folders'][$fold->id] = $fold->name;
        }

        // get files
        $media_perpage  = 5;
        $off            = (isset($offset)) ? $offset : 0;

        // pagination
        $this->db
        ->select('a.id')
        ->from('media a')
        ->join('media_lang b', 'b.id = a.id', 'left')
        ->group_by('a.id');
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $media_perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url('/files/ajaxindex'), $count, $media_perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        $this->db
        ->select('a.id, a.filename, a.size, a.alias, b.alt')
        ->from('media a')
        ->join('media_lang b', 'b.id = a.id', 'left')
        ->order_by('a.updated_at')
        ->limit($media_perpage, $off)
        ->group_by('a.id');
        $query = $this->db->get();
        $this->data['files'] = $query->result();

        // load view
        $this->data['media_upload']     = $this->load->view('files/add', $this->data, true);
        $this->data['media_list']       = $this->load->view('files/list', $this->data, true);
        $this->data['content_file']     = $this->load->view('files/upload', $this->data, true);

        $this->data['js_files'] = array(
            base_url() . PLUGINS. 'twbs-pagination/jquery.twbsPagination.min.js',
            base_url() . PLUGINS. 'rumbowyg/trumbowyg.min.js'
        );

        $this->data['scripts'] = '
            $(".editor").trumbowyg({
                btns: [
                    ["viewHTML"],
                    ["formatting"],
                    "btnGrp-semantic",
                    ["superscript", "subscript"],
                    ["link"],
                    ["insertImage"],
                    "btnGrp-justify",
                    "btnGrp-lists",
                    ["removeformat"],
                    ["fullscreen"]
                ]
            });
            $(".pagination").twbsPagination({
                totalPages: '.ceil($count / $media_perpage).',
                visiblePages: 2,
                onPageClick: function (event, page) {
                    $.post("'.site_url('files/ajaxindex').'/" + page, function(data) {
                        $("#files").html(data);
                    })
                }
            });';

        // create output by available language
        foreach($this->data['language'] as $lang) {
            // get current data
            $this->db
            ->from($this->table_name.' a')
            ->join('page_lang b', 'b.id = a.id', 'left')
            ->where('a.id', $id)
            ->where('b.lang', $lang->shortname);
            $query = $this->db->get();

            // return data
            if($query->num_rows() > 0 ) {
                $this->data['form'][$lang->shortname]     = $query->row();
            } else {
                // If no data return. Avoid error message
                $this->data['form'][$lang->shortname]     = new stdClass();
                $this->data['form'][$lang->shortname]->title     = null;
                $this->data['form'][$lang->shortname]->slug     = null;
                $this->data['form'][$lang->shortname]->body     = null;
                $this->data['form'][$lang->shortname]->intro     = null;
            }
        }

        // get current media (if available)
        $this->db
        ->select('alias')
        ->from('media')
        ->where('id', $this->data['form'][$this->session->userdata('lang')]->media_id );
        $query = $this->db->get();

        $this->data['media'] = $query->row();

        // validation rules
        foreach($this->data['language'] as $lang) {
            $this->form_validation->set_rules("title[".$lang->shortname."]", "Title in ".$lang->name, 'required');
        }

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('edit', $this->data, true);
        } else {
            $date = date("Y-m-d H:i:s", strtotime(set_value('published').' '.set_value('published_time')));

            // Bila validasi sukses
            $record = array(
                'category_id'   => set_value('category'),
                'status'        => set_value('status'),
                'media_id'      => set_value('file_id'),
                'updated_at'    => $date,
                'type'          => 'post',
                'users_id'      => $this->session->userdata('user_id')
            );
            // debug($data);

            // start transaction
            $this->db->trans_start();
            $this->db->where('id', $this->input->post('id'));
            $this->db->update($this->table_name, $record);
            $this->db->trans_complete();

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('edit', $this->data, true);
            } else {
                foreach($this->input->post('title') as $key => $val) {
                    write_log('BLOGS_UPDATE', 'blogs', 'Updated a post with title '.$this->input->post('title')[$lang->shortname]);
                    $record = array(
                            'title' => $this->input->post('title')[$key],
                            'intro' => $this->input->post('intro')[$key],
                            'body'  => html_entity_decode($this->input->post('body')[$key], ENT_QUOTES, 'UTF-8'),
                            'slug'  => $this->input->post('slug')[$key]
                    );
                    $this->db
                    ->where('lang', $key)
                    ->where('id', $this->input->post('id'));
                    $this->db->update('page_lang', $record);
                }
                // debug($record);

                $this->data['message']  = 'Your record has been saved.';
                $this->data['content']  = $this->load->view('admin/redirect', $this->data, true);
            }

            $this->data['content']  = $this->load->view('admin/redirect', $this->data, true);
        }

        // make an output
        $this->load->view('admin/content', $this->data);

    }


    public function add_upload()
    {
        $this->data['language']     = array();
        $this->data['folder']       = array();
        $this->data['files']        = array();
        $this->data['categories']   = array();
        // $this->data['pagination']   = '';
        $this->data['media_upload'] = $this->load->view('files/add', $this->data, true);
        $this->data['media_list']   = $this->load->view('files/list', $this->data, true);
        $this->data['content']      = $this->load->view('files/upload', $this->data, true);
        $this->load->view('admin/content', $this->data);
    }

	// -----------------------------------------------------------------------------------
	// Delete Item
	// -----------------------------------------------------------------------------------
    public function delete($id)
    {
        // security check
        $this->menu->check_access('delete');

        $this->data['button_link'] = site_url($this->url);

	    $query 		= $this->db->get_where('page_lang', array('id' => $id, 'lang' => $this->session->userdata('lang')));
	    $page 		= $query->row();
	    $this->general_model->delete('page','id',$id);
	    $this->general_model->delete('page_lang','id',$id);
		write_log('BLOGS_DELETE', 'blogs', 'Remove a post with title "'.$page->title.'"');

        $this->data['message']  = 'Your record has been removed.';
        $this->data['content']  = $this->load->view('admin/redirect', $this->data, true);

        // HARUS ADA - Proses keluaran untuk seluruh halaman
        $this->load->view('admin/content', $this->data);

    }

    // attach file
    public function ajax_attach()
    {
            // if not break the rules
            $file_name     = '';
            if (!empty($_FILES['userfile']['name'])) {
                $config['upload_path']          = MEDIA;
                $config['allowed_types']        = 'gif|jpg|png';
                $this->upload->initialize($config);
                $this->upload->do_upload('userfile');
                $metafile                       = $this->upload->data();
                $file_name                      = $metafile['file_name'];
                $file_type                      = $metafile['file_type'];
                $file_size                      = $metafile['file_size'];
            }

            // firstly, save to media
            $record = array(
                'category_id'   => $this->input->post('folder') ,
                'type'          => $file_type,
                'alias'         => md5($file_name),
                'filename'      => $file_name,
                'size'          => $file_size,
                'created_at'    => date('Y-m-d h:i:s'),
                'updated_at'    => date('Y-m-d h:i:s')
            );

            // start transaction
            $this->db->insert('media', $record);
            $id = $this->db->insert_id();
            write_log('MEDIA_UPLOADS', 'ajax_attach', 'Upload a media with filename '.$file_name);

            // then repeat by language
            foreach($this->input->post('alt') as $key => $val) {
                $record = array(
                    'lang'      => $key,
                    'id'        => $id,
                    'alt'      => $val,
                    'desc'      => $this->input->post('desc')[$key]
                );
                $this->db->insert('media_lang', $record);
            }

            // return photo
            $return = array(
                'attach_id'     => $id,
                'attach_file'   => base_url() . MEDIA . '/' . $file_name
            );

            echo json_encode($return);

    }

}

/* End of file bank.php */
