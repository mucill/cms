<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Translate extends MX_Controller {

    private $table_name     = 'cms_translate';
    private $url            = 'translate';
    private $perpage        = 25;
    private $data           = array();
    private $update         = '';
    private $delete         = '';

    public function __construct()
    {
        parent::__construct();

        // for security check
        $this->menu->has_access();

        // for header
        $this->data          = $this->header->button_header();
        $this->data['title'] = 'Translate';

        // for button link
        $this->data['button_link'] = (($this->uri->segment(2) == 'add') || ($this->uri->segment(2) == 'update')) ? site_url($this->url) : site_url($this->url).'/add' ;

        // for debug purpose only
        $this->output->enable_profiler(false);
    }

    public function index( $offset = 0 )
    {
        // pagination 
        $this->db
        ->select('id')
        ->from($this->table_name);
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('a.id, a.slug, GROUP_CONCAT(b.translate SEPARATOR " / ") translate')
        ->from($this->table_name.' a')
        ->join('translate_lang b', 'b.id = a.id', 'left')
        ->group_by('a.id')
        ->order_by('a.slug')
        ->limit($this->perpage, $offset);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {
            foreach($query->result() as $list) {
                // check for update access
                if($this->menu->crud_access('update')) {
                    $this->update = '<a href="'.site_url($this->url.'/'.'update'.'/'.$list->id).'" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>';                    
                }

                // check for delete access
                if($this->menu->crud_access('delete')) {
                    $this->delete = '<a href="'.site_url($this->url.'/'.'delete'.'/'.$list->id).'" class="delete btn btn-xs btn-default"><i class="fa fa-trash"></i></a>';
                }

                // return to table data
                $table_row = array(
                    $list->slug,
                    $list->translate
                );

                // check for additional link access
                if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                    $table_row = array_merge($table_row, array($this->delete.' '.$this->update));
                }

                $this->table->add_row($table_row);
            }

            // get language list
            $this->db->order_by('is_default','desc');
            $query = $this->db->get('lang');
            $this->data['language'] = $query->result();

            $lang_title = '';
            // validation rules
            foreach($this->data['language'] as $lang) {
                $lang_title .= $lang->name . ' / ';
            }

            $lang_title = substr($lang_title, 0, -2);


            // generate table header
            $this->table->set_template(array('table_open' => '<table class="table">'));
            if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                $this->table->set_heading('Shortname', $lang_title, '');
            } else {
                $this->table->set_heading('Shortname', $lang_title);
            }

            // output
            $this->data['list']         = $this->table->generate(); 
            $this->data['content']      = $this->load->view('admin/list', $this->data, TRUE);
        } else {
            // if no data
            $this->data['content']      = $this->load->view('admin/blank', $this->data, TRUE);                    
        }

        // make an output
        $this->load->view('admin/content', $this->data);
    }

    // Add a new item
    public function add()
    {
        // check for write access
        $this->menu->check_access('write');

        // get language list
        $this->db->order_by('is_default','desc');
        $query = $this->db->get('lang');
        $this->data['language'] = $query->result();

        // validation rules
        foreach($this->data['language'] as $lang) {
            $this->form_validation->set_rules("trans[".$lang->shortname."]", "Translate to ".$lang->name, 'required');
        }

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('translate/add', $this->data, true);
        } else {
            // save to category first
            $record = array(
                'slug'          => $this->input->post('slug')
            );

            // start transaction
            $this->db->insert($this->table_name, $record);
            $id = $this->db->insert_id();

            // then repeat by language
            foreach($this->input->post('trans') as $key => $val) {
                $records = array(
                    'id'            => $id,
                    'lang'          => $key,
                    'translate'     => $val
                );
                $this->db->insert('translate_lang', $records);
            }

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('translate/add', $this->data, true);
            } else {
                $this->data['message']  = 'Your record has been saved.';
                $this->data['content']  = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        // make an output
        $this->load->view('admin/content', $this->data);        
    }

    // Add a new item
    public function update($id = NULL)
    {
        // check for write access
        $this->menu->check_access('update');

        // get language list
        $this->db->order_by('is_default','desc');
        $query = $this->db->get('lang');
        $this->data['language'] = $query->result();

        // create output by available language
        foreach($this->data['language'] as $lang) {
            // get current data
            $this->db
            ->from($this->table_name.' a')
            ->join('translate_lang b', 'b.id = a.id', 'left')
            ->where('a.id', $id)
            ->where('b.lang', $lang->shortname);
            $query = $this->db->get();

            // return data
            $this->data['form'][$lang->shortname]     = $query->row();
        }
        // debug($this->data['form']);

        // validation rules
        foreach($this->data['language'] as $lang) {
            $this->form_validation->set_rules("trans[".$lang->shortname."]", "Translate to ".$lang->name, 'required');
        }

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('translate/edit', $this->data, true);
        } else {
            // save to category first
            $data = array(
                'slug'    => $this->input->post('slug')
            );

            // start transaction
            $this->db->where('id', $this->input->post('id'));
            $this->db->update($this->table_name, $data);

            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('translate/edit', $this->data, true);
            } else {
                // then repeat by language
                foreach($this->input->post('trans') as $key => $val) {
                    $record = array(
                        'translate'     => $val
                    );

                    $this->db
                    ->where('lang', $key)
                    ->where('id', $this->input->post('id'));
                    $this->db->update('cms_translate_lang', $record);
                }

                $this->data['message']  = 'Your record has been saved.';
                $this->data['content']  = $this->load->view('admin/redirect', $this->data, true);
            }
        }

        // make an output
        $this->load->view('admin/content', $this->data);        
    }


    //Delete one item
    public function delete( $id = NULL )
    {
        // security check
        $this->menu->check_access('delete');

        $this->data['button_link'] = site_url($this->url);

        $this->db->trans_start();
        $this->db->delete($this->table_name, array('id' => $id));
        $this->db->delete('translate_lang', array('id' => $id));
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $message = 'Fail while removed your record.';
        } else {
            $message = 'Your record has been removed.';
        }
        $this->data['message']          = $message;
        $this->data['content']          = $this->load->view('admin/redirect', $this->data, TRUE);
        $this->load->view('admin/content', $this->data);        
    }

    public function getParent($parent_id)
    {   
        $output = '-';
        if(isset($parent_id) && ($parent_id != 0)) {
            $this->db
            ->select('category')
            ->from($this->table_name)
            ->where('id', $parent_id);
            $parents = $this->db->get();
            if($parents->num_rows() > 0 ){
                $parent = $parents->row();                
                $output = $parent->category;            
            }
        }
        return $output;
    }

    public function ajax_default()
    {
        // set all is_default value to 0
        $this->db->update($this->table_name, array('is_default' => 0));

        // update selected is_default
        $this->db->where('id', $this->input->post('id'));
        $this->db->update($this->table_name, array('is_default' => 1));
        echo $this->input->post('id');
    }


}

/* End of file Translate.php */
/* Location: ./application/modules/admin/controllers/Translate.php */
