<?php if(validation_errors()) : ?>
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Error</strong>
    <hr>
    <?php echo validation_errors(); ?>
</div>
<?php endif ?>
<form action="<?php echo current_url() ?>" method="POST" class="form-horizontal" role="form">
<input type="hidden" name="id" value="<?php echo $form->id ?>">
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Please complete the form below.</h3>
    </div>
    <div class="box-body tm-padding">   
        <div class="form-group">
            <label for="name" class="col-sm-4">Widget's Name</label>
            <div class="col-sm-8">
                <input type="text" name="name" value="<?php echo set_value("name", $form->name) ?>" class="form-control" id="" placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label for="content" class="col-sm-12">Content</label>
            <div class="col-sm-12">
                <div id="editor"><?php echo set_value("content", $form->content) ?></div>
                <textarea name="content" id="content" class="hidden"><?php echo set_value("content", $form->content) ?></textarea>
            </div>
        </div>
    </div>
    <div class="panel-footer text-right">
    <a href="<?php echo site_url('widgets') ?>" class="btn tm-btn">Cancel</a>
    <button type="submit" class="btn tm-btn">Submit</button>
    </div>
</div>
</form>