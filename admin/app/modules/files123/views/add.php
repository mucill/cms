<?php if(validation_errors()) : ?>
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Error</strong>
    <hr>
    <?php echo validation_errors(); ?> 
</div>
<?php endif ?>
<form action="<?php echo current_url() ?>" method="POST" class="form-horizontal" role="form" enctype="multipart/form-data">
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Upload file.</h3>
    </div>
    <div class="box-body tm-padding"> 
        <div class="form-group">
            <label for="" class="col-sm-4">Name</label>
            <div class="col-sm-8">
            <input type="text" name="alt" value="<?php echo set_value("alt") ?>" class="form-control">
            </div>
        </div>              
        <div class="form-group">
            <label for="" class="col-sm-4">Description </label>
            <div class="col-sm-8">
            <textarea name="desc" class="form-control"><?php echo set_value("desc") ?></textarea>
            </div>
        </div>              
        <div class="form-group">
            <label for="" class="col-sm-4">Folder</label>
            <div class="col-sm-8">
            <?php if(isset($folders) > 0 ) : ?>
            <?php echo form_dropdown('folder', $folders, '', 'class="form-control"') ?>
            <?php else : ?>
            <p style="padding-top: 10px;">Create <a href="<?php echo site_url('folders/add') ?>">New Folder</a></p>
            <?php endif ?>
            </div>
        </div>                              
        <div class="form-group">
            <label for="" class="col-sm-4">Choose File</label>
            <div class="col-sm-8" style="padding-top:5px;">
                <input type="file" name="userfile">
            </div>
        </div>                              
    </div>
    <div class="panel-footer text-right">
    <a href="<?php echo site_url('files') ?>" class="btn tm-btn">Cancel</a>
    <button type="submit" class="btn tm-btn">Submit</button>
    </div>
</div>
</form>