<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Members extends MX_Controller {

    private $table_name     = 'cms_member';
    private $url            = 'members';
    private $perpage        = 5;
    private $data           = array();
    private $update         = '';
    private $delete         = '';

    public function __construct()
    {
        parent::__construct();

        // for security check
        $this->menu->has_access();

        // load model
        $this->load->model(array('auth_model','crud_model'));
        $this->load->library(array('media'));            

        // for header
        $this->data          = $this->header->button_header();
        $this->data['title'] = 'Members';

        // for button link
        $this->data['button_link'] = (($this->uri->segment(2) == 'add') || ($this->uri->segment(2) == 'update')) ? site_url($this->url) : site_url($this->url).'/add' ;


        $this->data['propinsi']       = array(
                                    '11' => 'Aceh',
                                    '12' => 'Sumatera Utara',
                                    '13' => 'Sumatera Barat',
                                    '14' => 'Riau',
                                    '15' => 'Jambi',
                                    '16' => 'Sumatera Selatan',
                                    '17' => 'Bengkulu',
                                    '18' => 'Lampung',
                                    '19' => 'Kepulauan Bangka Belitung',
                                    '21' => 'Kepulauan Riau',
                                    '31' => 'Dki Jakarta',
                                    '32' => 'Jawa Barat',
                                    '33' => 'Jawa Tengah',
                                    '34' => 'Di Yogyakarta',
                                    '35' => 'Jawa Timur',
                                    '36' => 'Banten',
                                    '51' => 'Bali',
                                    '52' => 'Nusa Tenggara Barat',
                                    '53' => 'Nusa Tenggara Timur',
                                    '61' => 'Kalimantan Barat',
                                    '62' => 'Kalimantan Tengah',
                                    '63' => 'Kalimantan Selatan',
                                    '64' => 'Kalimantan Timur',
                                    '65' => 'Kalimantan Utara',
                                    '71' => 'Sulawesi Utara',
                                    '72' => 'Sulawesi Tengah',
                                    '73' => 'Sulawesi Selatan',
                                    '74' => 'Sulawesi Tenggara',
                                    '75' => 'Gorontalo',
                                    '76' => 'Sulawesi Barat',
                                    '81' => 'Maluku',
                                    '82' => 'Maluku Utara',
                                    '91' => 'Papua Barat',
                                    '94' => 'Papua');

        // for debug purpose only
        $this->output->enable_profiler(false);
    }
    
    public function index( $offset = 0 )
    {

        $this->data['button_link']      = site_url('../../member/register');

        // pagination 
        $this->db
        ->select('member_id')
        ->from($this->table_name);
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('*')
        ->from($this->table_name)
        ->order_by('realname')
        ->limit($this->perpage, $offset);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {
            foreach($query->result() as $list) {
                // check for update access
                $this->update = '<a href="'.site_url($this->url.'/'.'update'.'/'.$list->member_id).'"><i class="ion-ios-list-outline"></i></a>';                    

                // return to table data
                $table_row = array(
                    $list->member_code,
                    $list->realname,
                    '<a href="mailto:'.$list->email.'">'.$list->email.'</a>',
                    ($list->status == 'pending') ? '<a class="activate" href="'.site_url('members/activate').'/'.$list->member_id.'">'.ucwords($list->status).'</a>' : '<a class="deactivate" href="'.site_url('members/deactivate').'/'.$list->member_id.'">'.ucwords($list->status).'</a>'
                );

                // check for additional link access
                $table_row = array_merge($table_row, array($this->update));

                $this->table->add_row($table_row);
            }

            // generate table header
            $this->table->set_template(array('table_open' => '<table class="table">'));
            if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                $this->table->set_heading('Member ID', 'Name', 'Email', 'Status', '');
            } else {
                $this->table->set_heading('Member ID', 'Name', 'Email', 'Status');
            }

            // output
            $this->data['list']         = $this->table->generate(); 
            $this->data['content']      = $this->load->view('admin/list', $this->data, TRUE);
        } else {
            // if no data
            $this->data['message']      = 'Sorry, no members were found.<br>Use registration form on public access.';                    
            $this->data['content']      = $this->load->view('admin/blank', $this->data, TRUE);                    
        }


        $this->data['scripts']          = '
            $(".activate").click(function(){
                if(confirm("Are you sure to approve this account ? ")) {
                    return true;
                }
                return false;
            });

            $(".deactivate").click(function(){
                if(confirm("Are you sure to pending this account ? ")) {
                    return true;
                }
                return false;
            });

        ';

        // make an output
        $this->load->view('admin/content', $this->data);
    }

    public function update( $id = null )
    {
        $query = $this->db->get_where($this->table_name, array('member_id'=> $id));

        if($query->num_rows() > 0) {
            // profile
            $this->data['prof']             = $query->row(); 

            // occupation
            $query = $this->db->get_where('member_office', array('member_id'=> $id));
            $this->data['occu']             = $query->row(); 

            // education
            $query = $this->db->get_where('member_edu', array('member_id'=> $id));
            $this->data['educ']             = $query->result(); 

            // employment
            $query = $this->db->get_where('member_jabatan', array('member_id'=> $id));
            $this->data['employ']           = $query->result(); 

            // publication
            $query = $this->db->get_where('member_publikasi', array('member_id'=> $id));
            $this->data['public']           = $query->result(); 

            // research
            $query = $this->db->get_where('member_riset', array('member_id'=> $id));
            $this->data['research']         = $query->result(); 

            // dedication
            $query = $this->db->get_where('member_abdi', array('member_id'=> $id));
            $this->data['dedication']       = $query->result(); 

            // output
            $this->data['content']          = $this->load->view('members/dashboard', $this->data, true);

        } else {
            $this->data['button_link']      = null;
            $this->data['message']          = 'Member not found';
            $this->data['content']          = $this->load->view('admin/blank', $this->data, true);
        }

        $this->load->view('admin/content', $this->data);
    }

    public function activate( $id = null )
    {
        $this->db->where('member_id', $id);
        $this->db->update($this->table_name, array('status' => 'approved'));
        $this->data['button_link']  = site_url('members');
        $this->data['message']      = 'Member has been set as approved account.';
        $this->data['content']      = $this->load->view('admin/redirect', $this->data, true);
        $this->load->view('admin/content', $this->data);
    }

    public function deactivate($id)
    {
        $this->db->where('member_id', $id);
        $this->db->update($this->table_name, array('status' => 'pending'));
        $this->data['button_link']  = site_url('members');
        $this->data['message']      = 'Member account has been set as pending account';
        $this->data['content']      = $this->load->view('admin/redirect', $this->data, true);
        $this->load->view('admin/content', $this->data);
    }

}
