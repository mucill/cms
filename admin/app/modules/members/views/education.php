<div class="box">
    <?php if(count($educ) > 0 ) : ?>
    <?php foreach($educ as $edu) : ?>
    <div class="box-header">
        <h3 class="box-title"><?php echo $edu->sarjana ?></h3>
    </div>
    <div class="box-body">
        <table class="table">
            <tr>
                <th>Name of Faculty</th>
                <td><?php echo $edu->jurusan ?></td>
            </tr>
            <tr>
                <th>University</th>
                <td><?php echo $edu->kampus ?></td>
            </tr>
            <tr>
                <th>Year</th>
                <td><?php echo $edu->lulus ?></td>
            </tr>
            <tr>
                <th>Title of Research</th>
                <td><?php echo $edu->skripsi ?></td>
            </tr>
        </table>
    </div>
    <?php endforeach ?>
    <?php else : ?>
    <div class="box-header">
        <h3 class="box-title">Education</h3>
    </div>
    <div class="box-body">
        No description founded.
    </div>
    <?php endif ?>
</div>