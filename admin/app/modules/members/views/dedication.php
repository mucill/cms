<div class="box">
    <?php if(count($dedication) > 0 ) : ?>
    <?php foreach($dedication as $ded) : ?>
    <div class="box-header">
        <h3 class="box-title"><?php echo $ded->abdi_title ?></h3>
    </div>
    <div class="box-body">
        <table class="table">
            <tr>
                <th>Stakeholder</th>
                <td><?php echo $ded->abdi_stake ?></td>
            </tr>
            <tr>
                <th>Periode</th>
                <td><?php echo $ded->abdi_year ?></td>
            </tr>
            <tr>
                <th>Partner</th>
                <td><?php echo $ded->abdi_partner ?></td>
            </tr>
        </table>
    </div>
    <?php endforeach ?>
    <?php else : ?>
        <div class="box-header">
            <h3 class="box-title">Dedication</h3>
        </div>
        <div class="box-body">
            No description founded.
        </div>
    <?php endif ?>
</div>