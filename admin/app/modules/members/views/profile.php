<div class="box">
    <div class="box-header">
        <h3 class="box-title">Profile</h3>
    </div>
	<div class="box-body">
		<?php if(!empty($occu->office_name)) : ?>
		<div class="row">
			<div class="col-md-6">
				<strong>Status</strong>
				<p><?php echo ucwords($prof->status) ?></p>

				<strong>Date of Birth</strong>
				<p><?php echo $prof->birth_city.', '.date('d M Y', strtotime($prof->birth_date)) ?></p>

				<strong>Address</strong>
				<p><?php echo $prof->home_addr.', '.$prof->home_city.', '.@$propinsi[$prof->home_prov] ?></p>

				<strong>Gender</strong>
				<p><?php echo $prof->gender ?></p>
			</div>
			<div class="col-md-6">
			
				<strong>Website</strong>
				<p><a href="http://<?php echo $prof->website ?>"><?php echo $prof->website ?></a></p>

				<strong>Facebook</strong>
				<p><?php echo $prof->socmed_fb ?></p>
				
				<strong>Twitter</strong>
				<p><?php echo $prof->socmed_tw ?></p>				
			</div>
		</div>
        <?php else : ?>
		No description founded.
        <?php endif ?>

	</div>
</div>
