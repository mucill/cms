<div class="box">
    <div class="box-header">
        <h3 class="box-title">Publication</h3>
    </div>
    <div class="box-body">
        <?php if(count($public)) : ?>
        <table class="table">
            <?php foreach($public as $pub) : ?>
            <tr><td><?php echo $prof->realname ?> (<?php echo $pub->pub_year ?>). <?php echo $pub->pub_title ?>.</td></tr>
            <?php endforeach ?>
        </table>
        <?php else :?>
        No description founded.
        <?php endif ?>
    </div>
</div>