<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Files extends MX_Controller {

    private $table_name     = 'media';
    private $url            = 'files';
    private $perpage        = 6;
    private $data           = array();
    private $update         = '';
    private $delete         = '';

    public function __construct()
    {
        parent::__construct();

        // for security check
        $this->menu->has_access();

        // load library
        $this->load->library('upload');
        $this->load->helper('text');

        // for header
        $this->data          = $this->header->button_header();
        $this->data['title'] = 'Media Files';

        // for button link
        $this->data['button_link'] = (($this->uri->segment(2) == 'add') || ($this->uri->segment(2) == 'update')) ? site_url($this->url) : site_url($this->url).'/add' ;

        // for debug purpose only
        $this->output->enable_profiler(false);
    }

    public function index( $offset = 0 )
    {
        // folders
        $this->db
        ->from('category a')
        ->join('category_lang b', 'b.id = a.id', 'join')
        ->where('a.type', 'folder')
        ->group_by('a.id');
        $query = $this->db->get();
        $this->data['folder']  = $query->result();

        // pagination
        $this->db
        ->select('a.id')
        ->from($this->table_name.' a')
        ->join('media_lang b', 'b.id = a.id', 'left')
        ->group_by('a.id');
        $query = $this->db->get();
        $count                      = $query->num_rows();

        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url.'/ajaxindex'), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // all data
        $this->db
        ->select('a.id, a.alias, a.filename, a.size, b.alt')
        ->from($this->table_name . ' a')
        ->join('media_lang b', 'b.id = a.id', 'left')
        ->order_by('a.updated_at','desc')
        ->group_by('a.id')
        ->limit($this->perpage, $offset);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {

            // return data
            $list                   = $query->result();
            $this->data['files']    = $list;

            // // check for update access
            // if($this->menu->crud_access('update')) {
            //     $this->update = '<a href="'.site_url($this->url.'/'.'update'.'/'.$list->id).'" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>';
            // }

            // // check for delete access
            // if($this->menu->crud_access('delete')) {
            //     $this->delete = '<a href="'.site_url($this->url.'/'.'delete'.'/'.$list->id).'" class="delete btn btn-xs btn-default"><i class="fa fa-trash"></i></a>';
            // }

            // // check for additional link access
            // if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
            //     $table_row = array_merge($table_row, array($this->delete.' '.$this->update));
            // }

        } else {
            $this->data['files'] = array();
        }

        $this->data['js_files'] = array(
            base_url() . PLUGINS. 'twbs-pagination/jquery.twbsPagination.min.js'
        );

        $this->data['scripts'] = '
            $(".pagination").twbsPagination({
                totalPages: '.ceil($count / $this->perpage).',
                onPageClick: function (event, page) {
                    $.post("'.site_url($this->url.'/ajaxindex').'/" + page, function(data) {
                        $("#files").html(data);
                    })
                }
            });';

        $this->data['content']      = $this->load->view('files/list', $this->data, TRUE);

        // make an output
        $this->load->view('admin/content', $this->data);
    }

    public function ajaxindex( $offset = 0, $filter = '' )
    {
        if($offset > 1 ) {
            $off = ($offset * $this->perpage ) / $offset;
        } else {
            $off = 0;
        }

        // all data
        $this->db
        ->select('a.id, a.alias, a.filename, a.size, b.alt')
        ->from($this->table_name . ' a')
        ->join('media_lang b', 'b.id = a.id', 'left')
        ->order_by('a.updated_at','desc')
        ->group_by('a.id')
        ->limit($this->perpage, $off);
        $query = $this->db->get();

        // if not empty
        // return data
        if($query->num_rows() > 0) {
            $list                   = $query->result();
            $this->data['files']    = $list;
        } else {
            $this->data['files'] = array();
        }

        $this->load->view('files/list_ajax', $this->data);
    }


    // Add a new item
    public function add()
    {
        // check for write access
        $this->menu->check_access('write');

        // get language list
        $query = $this->db->get('lang');
        $this->data['language'] = $query->result();

        // get folder
        $this->db
        ->select('a.id, b.name')
        ->from('category a')
        ->join('category_lang b', 'b.id = a.id', 'left')
        ->where('a.type', 'folder')
        ->group_by('a.id');
        $query  = $this->db->get();
        $folder = $query->result();
        foreach($folder as $fold) {
            $this->data['folders'][$fold->id] = $fold->name;
        }

        // validation rules
        $this->form_validation->set_rules("folder", "Folder", 'required');

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('files/add', $this->data, true);
        } else {

            // if not break the rules
            $file_name     = '';
            if (!empty($_FILES['userfile']['name'])) {
                $path_parts                     = pathinfo($_FILES["userfile"]["name"]);
                $extension                      = $path_parts['extension'];
                $config['upload_path']          = MEDIA;
                $config['allowed_types']        = 'gif|jpg|jpeg|png|pdf|doc|docx|xls|xlsx|ppt|pptx|csv|txt';
                $config['file_name']            = $_FILES['userfile']['name'];
                $config['remove_spaces']        = FALSE;
                $this->upload->initialize($config);
                $this->upload->do_upload('userfile');
                $metafile                       = $this->upload->data();
                $file_name                      = $_FILES["userfile"]["name"];
                $file_type                      = $metafile['file_type'];
                $file_size                      = $metafile['file_size'];
            }

            // firstly, save to media
            $record = array(
                'type'          => $file_type,
                'filename'      => $file_name,
                'size'          => $file_size,
                'alias'         => md5($file_name),
                'category_id'   => $this->input->post('folder') ,
                'created_at'    => date('Y-m-d h:i:s'),
                'updated_at'    => date('Y-m-d h:i:s')
            );

            // start transaction
            $this->db->insert($this->table_name, $record);
            $id = $this->db->insert_id();
            // then repeat by language
            foreach($this->input->post('alt') as $key => $val) {
                $record = array(
                    'lang'      => $key,
                    'id'        => $id,
                    'alt'      => $val,
                    'desc'      => $this->input->post('desc')[$key]
                );

                $this->db->insert('media_lang', $record);
            }
            // debug($data);

            // check for status
            $this->data['message']  = 'Your record has been saved.';
            $this->data['content']  = $this->load->view('admin/redirect', $this->data, TRUE);
        }

        // make an output
        $this->load->view('admin/content', $this->data);
    }

    // Update a new item
    public function update($id = NULL)
    {
        // check for write access
        $this->menu->check_access('update');

        // get language list
        $query = $this->db->get('lang');
        $this->data['language'] = $query->result();

        // get folder
        $this->db
        ->select('a.id, b.name')
        ->from('category a')
        ->join('category_lang b', 'b.id = a.id', 'left')
        ->where('a.type', 'folder')
        ->group_by('a.id');
        $query  = $this->db->get();
        $folder = $query->result();
        foreach($folder as $fold) {
            $this->data['folders'][$fold->id] = $fold->name;
        }


        // create output by available language
        foreach($this->data['language'] as $lang) {

            $this->db
            ->select('a.*, b.*')
            ->from($this->table_name . ' a')
            ->join('media_lang b', 'b.id = a.id', 'left')
            ->where('a.id', $id)
            ->where('b.lang', $lang->shortname);
            $query = $this->db->get();

            // return data
            $this->data['form'][$lang->shortname]     = $query->row();
        }

        // validation rules
        foreach($this->data['language'] as $lang) {
            $this->form_validation->set_rules("alt[".$lang->shortname."]", "Alternative name ".$lang->name, 'required');
        }

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('files/edit', $this->data, true);
        } else {

            // remove current file
            if($this->data['form'][$lang->shortname]->filename != '') {
                @unlink($config['upload_path'] . $this->data['form'][$lang->shortname]->filename);
            }

            // if not break the rules

            // avoid an error while no file uploaded
            $file_name                      = $this->data['form'][$lang->shortname]->filename;
            $file_type                      = $this->data['form'][$lang->shortname]->type;
            $file_size                      = $this->data['form'][$lang->shortname]->size;

            // do upload while a file is selected
            if (!empty($_FILES['userfile']['name'])) {
                $path_parts                     = pathinfo($_FILES["userfile"]["name"]);
                $extension                      = $path_parts['extension'];
                $config['upload_path']          = MEDIA;
                $config['allowed_types']        = 'gif|jpg|jpeg|png|pdf|doc|docx|xls|xlsx|ppt|pptx|csv';
                $config['file_name']            = $_FILES['userfile']['name'];
                $config['remove_spaces']        = FALSE;
                $this->upload->initialize($config);
                $this->upload->do_upload('userfile');
                $metafile                       = $this->upload->data();
                $file_name                      = $metafile['file_name'];
                $file_type                      = $metafile['file_type'];
                $file_size                      = $metafile['file_size'];
            }

            // firstly, save to media
            $record = array(
                'type'          => $file_type,
                'filename'      => $file_name,
                'size'          => $file_size,
                'alias'         => md5($file_name),
                'category_id'   => $this->input->post('folder') ,
                'created_at'    => date('Y-m-d h:i:s'),
                'updated_at'    => date('Y-m-d h:i:s')
            );

            // start transaction
            $this->db->where('id', $this->input->post('id'));
            $this->db->update($this->table_name, $record);

            // then repeat by language
            foreach($this->input->post('alt') as $key => $val) {
                $record = array(
                    'lang'      => $key,
                    'alt'      => $this->input->post('alt')[$key],
                    'desc'      => $this->input->post('desc')[$key]
                );

                $this->db
                ->where('lang', $key)
                ->where('id', $this->input->post('id'));
                $this->db->update('media_lang', $record);
            }
            // debug($data);

            // check for status
            $this->data['message']  = 'Your record has been saved.';
            $this->data['content']  = $this->load->view('admin/redirect', $this->data, TRUE);
        }

        // make an output
        $this->load->view('admin/content', $this->data);
    }

    //Delete one item
    public function delete( $id = NULL )
    {

        // security check
        $this->menu->check_access('delete');

        // get current filename
        $this->db
        ->select('filename')
        ->where('id', $id);
        $query  = $this->db->get($this->table_name);
        $file   = $query->row();

        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->delete($this->table_name);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $message = 'Fail while removed your record.';
        } else {
            // let's remove it
            $message = 'Your record has been removed.';
            @unlink(MEDIA . '/'. $file->filename);
        }
        $this->data['button_link']      = site_url($this->url);
        $this->data['message']          = $message;
        $this->data['content']          = $this->load->view('admin/redirect', $this->data, TRUE);
        $this->load->view('admin/content', $this->data);
    }

    public function getParent($parent_id)
    {
        $output = '-';
        if(isset($parent_id) && ($parent_id != 0)) {
            $this->db
            ->select('category')
            ->from($this->table_name)
            ->where('id', $parent_id);
            $parents = $this->db->get();
            if($parents->num_rows() > 0 ){
                $parent = $parents->row();
                $output = $parent->category;
            }
        }
        return $output;
    }

    public function folder( $filter = '', $offset = 0 )
    {
        // get folders
        $this->db
        ->from('category a')
        ->join('category_lang b', 'b.id = a.id', 'join')
        ->where('a.type', 'folder')
        ->group_by('a.id');
        $query = $this->db->get();
        $this->data['folder']  = $query->result();

        // pagination
        $this->db
        ->select('a.id')
        ->from($this->table_name.' a')
        ->join('category_lang b', 'b.id = a.category_id')
        ->where('b.slug', $filter)
        ->where('b.lang', $this->session->userdata('lang'));
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url('/files/folder').'/'.$filter, $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('a.id, a.filename, a.alias, a.size, b.alt')
        ->from($this->table_name . ' a')
        ->join('media_lang b', 'b.id = a.id', 'left')
        ->join('category_lang c', 'c.id = a.category_id')
        ->where('c.slug', $filter)
        ->where('b.lang', $this->session->userdata('lang'))
        ->order_by('a.updated_at')
        ->group_by('a.id')
        ->limit($this->perpage, $offset);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {
            // return data
            $list                   = $query->result();
            $this->data['files']    = $list;

        } else {
            $this->data['files'] = array();
        }

        // $this->data['js_files'] = array(
        //     base_url() . PLUGINS. 'twbs-pagination/jquery.twbsPagination.min.js'
        // );

        // $this->data['scripts'] = '
        //     $(".pagination").twbsPagination({
        //         totalPages: '.ceil($count/$this->perpage).',
        //         visiblePages: 5,
        //         onPageClick: function (event, page) {
        //             $.post("'.site_url($this->url.'/ajaxindex/').'/" + (page*'.($this->perpage-1).$this->uri->segment(3).'), function(data) {
        //                 $("#files").html(data);
        //             })
        //         }
        //     });';

        $this->data['content']      = $this->load->view('files/list', $this->data, TRUE);

        // make an output
        $this->load->view('admin/content', $this->data);
    }

    public function media($alias)
    {
        $this->db
        ->where('alias', $alias);
        $query = $this->db->get('media');
        if($query->num_rows() > 0 ) {
            $read   = $query->row();
            $mime   = $read->type;

            if((substr($mime, -3) == 'peg') || (substr($mime, -3) == 'jpg') || (substr($mime, -3) == 'png') || (substr($mime, -3) == 'gif')) {
                $path   = base_url() . MEDIA . '/' . $read->filename;
            } else {
                $path   = base_url() . MEDIA . '/blank.jpg';
                $mime   = 'image/jpg';
            }
        } else {
            $path   = base_url() . MEDIA . '/blank.jpg';
            $mime   = 'image/jpg';
        }
        // debug($path);
        header("Content-Type: ".$mime);
        readfile($path);
        exit;
    }


}

/* End of file Category.php */
/* Location: ./application/modules/admin/controllers/Category.php */
