<?php if(validation_errors()) : ?>
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Error</strong>
        <hr>
        <?php echo validation_errors(); ?>
    </div>
<?php endif ?>
<form action="<?php echo current_url() ?>" method="POST" class="form-horizontal" role="form">
    <div class="row">
        <div class="box box-default">
            <ul class="nav nav-tabs tm-pills " role="tablist">
                <li role="presentation" class="active"><a href="#media" aria-controls="home" role="tab" data-toggle="tab">Media</a></li>
                <li role="presentation"><a href="#upload" aria-controls="home" role="tab" data-toggle="tab">Upload</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane tm-nopadding-top active" id="media">
                    <?php echo $media_list ?>
                </div>
                <div role="tabpanel" class="tab-pane tm-nopadding-top" id="upload">
                    <?php echo $media_upload ?>
                </div>
            </div>
        </div>
    </div>
</form>