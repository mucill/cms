<script type="text/javascript">
function copyToClipboard(element) {
    var wto;
    clearTimeout(wto);
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).data('uri')).select();
    document.execCommand("copy");
    $(element).text('Copied');
    wto = setTimeout(function() {
        $(element).text('Copy Media Path');
    }, 5000);
    $temp.remove();
}
</script>
<?php if(validation_errors()) : ?>
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Error</strong>
        <hr>
        <?php echo validation_errors(); ?>
    </div>
<?php endif ?>
<form action="<?php echo current_url() ?>" method="POST" class="form-horizontal" role="form">
    <div class="row">

        <div class="col-lg-4">
            <div class="box box-default" >
                <div class="box-header with-border">
                    <h3 class="box-title">Folder</h3>
                </div>
                <div id="folder">
                <?php if(count($folder) > 0) : ?>
                <div class="list-group" style="padding: 10px;">
                    <a id="" href="<?php echo site_url('files') ?>" class="list-group-item tm-noborder <?php echo ($this->uri->total_segments() > 1 ) ? '' : 'active' ?>">All</a>
                    <?php foreach($folder as $fold): ?>
                    <a id="<?php echo $fold->slug ?>" href="<?php echo site_url('files/folder') .'/'. $fold->slug  ?>" class="list-group-item tm-noborder <?php echo ($this->uri->segment($this->uri->total_segments()) == $fold->slug || $this->uri->segment($this->uri->total_segments()-1) == $fold->slug ) ? 'active' :'' ?>"><?php echo $fold->name ?></a>
                    <?php endforeach ?>
                </div>
                <?php else : ?>
                <div class="text-center">
                    <a href="<?php echo base_url('folders/add') ?>">Create New Folder</a>
                </div>
                <?php endif ?>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo ($this->uri->total_segments() > 1) ? ucwords($this->uri->segment(3)) : '' ?> Files</h3>
                </div>
                <div class="box-body" id="files">
                    <?php if(count($files) > 0) : ?>
                    <table class="table">
                        <?php foreach($files as $file) : ?>
                            <tr>
                                <td width="48px">
                                    <div style="width:64px; overflow: hidden;">
                                    <img class="img-thumbnail" src="<?php echo site_url('files/media') . '/' . $file->alias ?>" alt="<?php echo $file->filename ?>" width="64px" />
                                    </div>
                                </td>
                                <td>
                                    <a href="#" data-id="<?php echo $file->id ?>" data-name="<?php echo ($file->alt != '') ? $file->alt : $file->filename ?>">
                                    <?php echo ($file->alt != '') ? $file->alt : $file->filename ?>
                                    </a>
                                    <span class="mailbox-attachment-size"><?php echo $file->size ?> KB | <strong style="cursor:pointer" onclick="copyToClipboard(this)" data-uri="<?php echo site_url('../../index.php/media/' . $file->alias) ?>">Copy Media Path<strong></span>
                                </td>
                                <td>
                                    <?php
                                    if($this->menu->crud_access('delete')) {
                                        echo '<a href="'.site_url('files/delete/'.$file->id).'" class="delete btn btn-xs btn-default"><i class="fa fa-trash"></i></a> ';
                                    }
                                    if($this->menu->crud_access('update')) {
                                        echo '<a href="'.site_url('files/update/'.$file->id).'" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a> ';
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </table>
                    <?php else :?>
                    <div class="text-center tm-padding">
                        No media available. <a href="<?php echo base_url('files/add') ?>">Upload New Media</a>
                    </div>
                    <?php endif ?>
                </div>
            </div>
            <?php echo $pagination ?>
        </div>

    </div>
</form>
