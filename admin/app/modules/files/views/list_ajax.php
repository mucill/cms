<script type="text/javascript">
function copyToClipboard(element) {
    var wto;
    clearTimeout(wto);
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).data('uri')).select();
    document.execCommand("copy");
    $(element).text('Copied');
    wto = setTimeout(function() {
        $(element).text('Copy Media Path');
    }, 5000);
    $temp.remove();
}
</script>
<?php if(count($files) > 0) : ?>
<table class="table">
    <?php foreach($files as $file) : ?>
        <tr>
            <td width="48px">
                <div style="width:64px; overflow: hidden;">
                <img class="img-thumbnail" src="<?php echo site_url('files/media') . '/' . $file->alias ?>" alt="<?php echo $file->filename ?>" width="64px" />
                </div>
            </td>
            <td>
                <a href="#" data-id="<?php echo $file->id ?>" data-name="<?php echo ($file->alt != '') ? $file->alt : $file->filename ?>">
                <?php echo ($file->alt != '') ? $file->alt : $file->filename ?>
                <span class="mailbox-attachment-size"><?php echo $file->size ?> KB | <strong style="cursor:pointer" onclick="copyToClipboard(this)" data-uri="<?php echo site_url('../../index.php/media/' . $file->alias) ?>">Copy Media Path<strong></span>
                </a>
            </td>
            <td>
                <?php
                if($this->menu->crud_access('delete')) {
                    echo '<a href="'.site_url('files/delete/'.$file->id).'" class="delete btn btn-xs btn-default"><i class="fa fa-trash"></i></a> ';
                }
                if($this->menu->crud_access('update')) {
                    echo '<a href="'.site_url('files/update/'.$file->id).'" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a> ';
                }
                ?>
            </td>
        </tr>
    <?php endforeach ?>
</table>
<?php else :?>
<div class="text-center">
    No Media Available. <a href="<?php echo base_url('files/add') ?>">Upload New Media</a>
</div>
<?php endif ?>
<script type="text/javascript">
    $('#files a').each(function(index){
        $(this).click(function(){
            id  = $(this).attr('data-id');
            src = $(this).parent('td').parent().children('td').children('div').children('img').attr('src');
            $('#file_id').val(id);
            $('#featured_attach').attr('src', src).attr('style','margin-bottom:20px;');
            $('#remove-img').removeClass('hidden');
        });
    });

    $('.delete').click(function(){
        if(confirm('Are you sure delete this record ? ')) {
            return true;
        } else {
            return false;
        }
    });
</script>
