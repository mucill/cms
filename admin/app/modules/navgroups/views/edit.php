<?php if(validation_errors()) : ?>
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Error</strong>
    <hr>
    <?php echo validation_errors(); ?>
</div>
<?php endif ?>
<form action="<?php echo current_url() ?>" method="POST" class="form-horizontal" role="form">
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Please complete the form below.</h3>
    </div>
    <div class="box-body tm-padding">            
            <div class="form-group">
                <label for="" class="col-sm-4">Groups's Name</label>
                <div class="col-sm-8">
                <input type="text" name="name" value="<?php echo set_value('name', $form->name) ?>" class="form-control" id="" placeholder="">
                </div>
            </div>           
            <div class="form-group">
                <label for="" class="col-sm-4">Shortname</label>
                <div class="col-sm-8">
                <input type="text" name="slugs" value="<?php echo set_value('slugs', $form->slug) ?>" class="form-control" id="" placeholder="">
                </div>
            </div>           
    </div>
    <div class="panel-footer text-right">
    <a href="<?php echo site_url('navgroups') ?>" class="btn tm-btn">Cancel</a>
    <button type="submit" class="btn tm-btn">Submit</button>
    </div>
</div>
</form>

<script>
    $('#linkto a').click(function(){
        $('#type').val(($(this).attr('href')));
    })
</script>