<?php if(validation_errors()) : ?>
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Error</strong>
    <hr>
    <?php echo validation_errors(); ?>
</div>
<?php endif ?>
<form action="<?php echo current_url() ?>" method="POST" class="form-horizontal" role="form">
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Please complete the form below.</h3>
    </div>
    <ul class="nav nav-tabs" role="tablist" style="padding-left: 10px;">
        <?php foreach($language as $lang) : ?>
        <li role="presentation" <?php echo ($lang->is_default == 1) ? 'class="active"' : '' ?>><a href="#<?php echo $lang->shortname ?>" aria-controls="<?php echo $lang->shortname ?>" role="tab" data-toggle="tab" style="padding: 25px 15px;"><?php echo $lang->name ?></a></li>
        <?php endforeach ?>
    </ul>
    <div class="box-body tab-content">
        <div class="form-group tm-nopadding" style="padding-top:50px; margin-bottom: 0;">
            <label for="" class="col-sm-4">Parents</label>
            <div class="col-sm-8">
                <select class="form-control" name="parents">
                    <option value="0">As Parent</option>
                    <?php foreach($cat_parents as $parent): ?>
                    <option value="<?php echo $parent->id ?>" <?php echo ($parent->id == $lang->id) ? 'selected' : '' ?>><?php echo $parent->name ?></option>
                    <?php endforeach ?>
                </select>
            </div>
        </div>
        <?php foreach($language as $lang) : ?>
            <div class="tab-pane <?php echo ($lang->is_default == 1) ? 'active' : '' ?>" id="<?php echo $lang->shortname ?>">
                <div class="form-group">
                    <label for="" class="col-sm-4">Category's Name in <?php echo $lang->name ?></label>
                    <div class="col-sm-8">
                    <input type="text" name="category[<?php echo $lang->shortname ?>]" value="<?php echo set_value("category[".$lang->shortname."]", $form[$lang->shortname]->name) ?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-4">URL Alias</label>
                    <div class="col-sm-8">
                    <input type="text" name="slug[<?php echo $lang->shortname ?>]" value="<?php echo set_value("slug[".$lang->shortname."]", $form[$lang->shortname]->slug) ?>" class="form-control">
                    </div>
                </div>
            </div>
        <?php endforeach ?>
        <div class="form-group tm-nopadding">
            <label for="" class="col-sm-4">Set as default</label>
            <div class="col-sm-8">
                <input type="checkbox" name="default" value="1" <?php echo ($form[$this->session->userdata('lang')]->is_default > 0) ? 'checked' : '' ?>>
            </div>
        </div>
    </div>
    <div class="panel-footer text-right">
    <input type="hidden" name="id" value="<?php echo $form[$this->session->userdata('lang')]->id ?>">
    <a href="<?php echo site_url('category') ?>" class="btn tm-btn">Cancel</a>
    <button type="submit" class="btn tm-btn">Submit</button>
    </div>
</div>
</form>
