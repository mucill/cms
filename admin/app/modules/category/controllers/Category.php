<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MX_Controller {

    private $table_name     = 'cms_category';
    private $url            = 'category';
    private $perpage        = 5;
    private $data           = array();
    private $update         = '';
    private $delete         = '';

    public function __construct()
    {
        parent::__construct();

        // for security check
        $this->menu->has_access();

        // for header
        $this->data          = $this->header->button_header();
        $this->data['title'] = 'Category';

        // for button link
        $this->data['button_link'] = (($this->uri->segment(2) == 'add') || ($this->uri->segment(2) == 'update')) ? site_url($this->url) : site_url($this->url).'/add' ;

        // for debug purpose only
        $this->output->enable_profiler(false);
    }

    public function index( $offset = 0 )
    {
        // pagination
        $this->db
        ->select('a.id')
        ->from($this->table_name . ' a')
        ->where('a.type', 'post');
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('a.id, a.is_default, b.name, a.parents')
        ->from($this->table_name . ' a')
        ->join('category_lang b', 'b.id = a.id', 'left')
        ->where('a.type', 'post')
        ->where('b.lang', $this->session->userdata('lang'))
        ->order_by('b.name')
        ->group_by('b.id')
        ->limit($this->perpage, $offset);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {
            foreach($query->result() as $list) {
                // check for update access
                if($this->menu->crud_access('update')) {
                    $this->update = '<a href="'.site_url($this->url.'/'.'update'.'/'.$list->id).'" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>';
                }

                // check for delete access
                if($this->menu->crud_access('delete')) {
                    if($list->is_default == 0) {
                        $this->delete = '<a href="'.site_url($this->url.'/'.'delete'.'/'.$list->id).'" class="delete btn btn-xs btn-default"><i class="fa fa-trash"></i></a>';
                    } else {
                        $this->delete = '';
                    }
                }

                // return to table data
                $table_row = array(
                    ($list->is_default == 1) ? '<strong>'.$list->name.'<strong>' : $list->name ,
                    ($list->is_default == 1) ? 'Default' : '<a href="#" class="default" data-id="'.$list->id.'">Set as default</a>'
                );

                // check for additional link access
                if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                    $table_row = array_merge($table_row, array($this->delete.' '.$this->update));
                }

                $this->table->add_row($table_row);
            }

            // generate table header
            $this->table->set_template(array('table_open' => '<table class="table">'));
            if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                $this->table->set_heading('Category', 'Default', '');
            } else {
                $this->table->set_heading('Category','Default');
            }

            // output
            $this->data['list']         = $this->table->generate();
            $this->data['content']      = $this->load->view('admin/list', $this->data, TRUE);
        } else {
            // if no data
            $this->data['content']      = $this->load->view('admin/blank', $this->data, TRUE);
        }

        // script
        $this->data['scripts'] = '
            $(".default").click(function(){
                $.post("'.site_url($this->url.'/ajax_default').'",{ "id" : $(this).data("id")})
                .done(function(){
                    location.reload();
                })
            });
        ';

        // make an output
        $this->load->view('admin/content', $this->data);
    }

    // Add a new item
    public function add()
    {
        // check for write access
        $this->menu->check_access('write');

        // get language list
        $this->db
        ->order_by('is_default','desc');
        $query = $this->db->get('lang');
        $this->data['language'] = $query->result();

        // get parents list
        $this->db
        ->from('category_lang a', true)
        ->join('category b','b.id = a.id', 'left')
        ->where('a.lang', $this->session->userdata('lang'))
        ->where('b.parents', 0)
        ->order_by('name');
        $query = $this->db->get();
        $this->data['cat_parents'] = $query->result();

        // validation rules
        foreach($this->data['language'] as $lang) {
            $this->form_validation->set_rules("category[".$lang->shortname."]", "Category's Name in ".$lang->name, 'required');
        }

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('category/add', $this->data, true);
        } else {
            // save to category first
            $record = array(
                'type'          => 'post',
                'parents'       => $this->input->post('parents'),
                'is_default'    => $this->input->post('default'),
                'created_at'    => date('Y-m-d h:i:s'),
                'updated_at'    => date('Y-m-d h:i:s')
            );

            // start transaction
            $this->db->insert($this->table_name, $record);
            $id = $this->db->insert_id();

            if($this->input->post('default') != null ) {
                $this->db->update($this->table_name, array('is_default' => 0));
                $this->db->where('id', $id);
                $this->db->update($this->table_name, array('is_default' => 1));
            }

            // then repeat by language
            foreach($this->input->post('category') as $key => $val) {
                $record = array(
                    'id'        => $id,
                    'lang'      => $key,
                    'name'      => $val,
                    'slug'      => $this->input->post('slug')[$key]
                );

                $this->db->trans_start();
                $this->db->insert('category_lang', $record);
                $this->db->trans_complete();
            }

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('category/add', $this->data, true);
            } else {
                $this->data['message']  = 'Your record has been saved.';
                $this->data['content']  = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        // make an output
        $this->load->view('admin/content', $this->data);
    }

    // Add a new item
    public function update($id = NULL)
    {
        // check for write access
        $this->menu->check_access('update');

        // get language list
        $this->db->order_by('is_default','desc');
        $query = $this->db->get('lang');
        $this->data['language'] = $query->result();

        // get parents list
        $this->db
        ->from('category_lang a', true)
        ->join('category b','b.id = a.id', 'left')
        ->where('a.lang', $this->session->userdata('lang'))
        ->where('b.parents', 0) // for parent only
        ->order_by('name');
        $query = $this->db->get();
        $this->data['cat_parents'] = $query->result();

        // create output by available language
        foreach($this->data['language'] as $lang) {
            // get current data
            $this->db
            ->from($this->table_name.' a')
            ->join('category_lang b', 'b.id = a.id', 'left')
            ->where('a.id', $id)
            ->where('b.lang', $lang->shortname);
            $query = $this->db->get();

            // if data found
            if($query->num_rows() > 0 ) {
                // return data
                $this->data['form'][$lang->shortname]       = $query->row();
            } else {
                // no data
                $this->data['form'][$lang->shortname]       = new stdClass();
                $this->data['form'][$lang->shortname]->name = null;
                $this->data['form'][$lang->shortname]->slug = null;
            }
        }
        // debug($this->data['form']);

        // validation rules
        foreach($this->data['language'] as $lang) {
            $this->form_validation->set_rules("category[".$lang->shortname."]", "Category's Name in ".$lang->name, 'required');
        }

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('category/edit', $this->data, true);
        } else {
            // save to category first
            $data = array(
                'type'          => 'post',
                'parents'       => $this->input->post('parents'),
                'is_default'    => $this->input->post('default'),
                'created_at'    => date('Y-m-d h:i:s'),
                'updated_at'    => date('Y-m-d h:i:s')
            );

            // start transaction
            $this->db->trans_start();
            $this->db->where('id', $this->input->post('id'));
            $this->db->update($this->table_name, $data);
            $this->db->trans_complete();

            if($this->input->post('default') != null ) {
                $this->db->update($this->table_name, array('is_default' => 0));
                $this->db->where('id', $id);
                $this->db->update($this->table_name, array('is_default' => 1));
            }

            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('category/edit', $this->data, true);
            } else {
                // then repeat by language
                foreach($this->input->post('category') as $key => $val) {
                    $record = array(
                        'name'      => $val,
                        'slug'      => $this->input->post('slug')[$key]
                    );

                    $this->db
                    ->where('lang', $key)
                    ->where('id', $this->input->post('id'));
                    $this->db->update('category_lang', $record);
                }

                $this->data['message']  = 'Your record has been saved.';
                $this->data['content']  = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        // make an output
        $this->load->view('admin/content', $this->data);
    }


    //Delete one item
    public function delete( $id = NULL )
    {
        // security check
        $this->menu->check_access('delete');

        $this->data['button_link'] = site_url($this->url);

        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->delete($this->table_name);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $message = 'Fail while removed your record.';
        } else {
            $message = 'Your record has been removed.';
        }
        $this->data['message']          = $message;
        $this->data['content']          = $this->load->view('admin/redirect', $this->data, TRUE);
        $this->load->view('admin/content', $this->data);
    }

    public function getParent($parent_id)
    {
        $output = '-';
        if(isset($parent_id) && ($parent_id != 0)) {
            $this->db
            ->select('category')
            ->from($this->table_name)
            ->where('id', $parent_id);
            $parents = $this->db->get();
            if($parents->num_rows() > 0 ){
                $parent = $parents->row();
                $output = $parent->category;
            }
        }
        return $output;
    }

    public function ajax_default()
    {
        // set all is_default value to 0
        $this->db->update($this->table_name, array('is_default' => 0));

        // update selected is_default
        $this->db->where('id', $this->input->post('id'));
        $this->db->update($this->table_name, array('is_default' => 1));
        echo $this->input->post('id');
    }


}

/* End of file Category.php */
/* Location: ./application/modules/admin/controllers/Category.php */
