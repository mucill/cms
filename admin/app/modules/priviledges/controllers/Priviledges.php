<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Priviledges extends MX_Controller {

    private $table_name     = 'cms_priviledges';
    private $url            = 'priviledges';
    private $perpage        = 5;
    private $data           = array();
    private $update         = '';
    private $delete         = '';

    public function __construct()
    {
        parent::__construct();

        // for security check
        $this->menu->has_access();

        // for header
        $this->data          = $this->header->button_header();
        $this->data['title'] = 'Priviledges';

        // for button link
        $this->data['button_link'] = (($this->uri->segment(2) == 'add') || ($this->uri->segment(2) == 'update')) ? site_url($this->url) : site_url($this->url).'/add' ;

        // for debug purpose only
        $this->output->enable_profiler(false);
    }

    public function index( $offset = 0 )
    {
        // pagination 
        $this->db
        ->select('a.groups_id, b.name, a.created, c.first_name, c.last_name')
        ->from($this->table_name.' a')
        ->join('cms_groups b', 'b.id = a.groups_id')
        ->join('cms_users c', 'c.id = a.created_by')
        ->group_by('a.groups_id');
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('a.groups_id, b.name, a.created, c.first_name, c.last_name')
        ->from($this->table_name.' a')
        ->join('cms_groups b', 'b.id = a.groups_id','left')
        ->join('cms_users c', 'c.id = a.created_by','left')
        ->group_by('a.groups_id')
        ->order_by('a.created', 'desc')
        ->limit($this->perpage, $offset);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {
            foreach($query->result() as $list) {
                // check for update access
                if($this->menu->crud_access('update')) {
                    $this->update = '<a href="'.site_url($this->url.'/'.'update'.'/'.$list->groups_id).'" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>';
                }

                // check for delete access
                if($this->menu->crud_access('delete')) {
                    $this->delete = '<a href="'.site_url($this->url.'/'.'delete'.'/'.$list->groups_id).'" class="delete btn btn-xs btn-default"><i class="fa fa-trash"></i></a>';
                }

                // return to table data
                $table_row = array(
                                    ucwords($list->name), 
                                    date('d M Y H:i:s A',strtotime($list->created)),
                                    ucwords($list->first_name.' '.$list->last_name)
                );

                // check for additional link access
                if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                    $table_row = array_merge($table_row, array($this->delete.' '.$this->update));
                }

                $this->table->add_row($table_row);
            }

            // generate table header
            $this->table->set_template(array('table_open' => '<table class="table">'));
            if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                $this->table->set_heading('Groups', 'Updated', 'By','');
            } else {
                $this->table->set_heading('Groups', 'Updated', 'By');                
            }

            // output
            $this->data['list']         = $this->table->generate(); 
            $this->data['content']      = $this->load->view('admin/list', $this->data, TRUE);
        } else {
            // if no data
            $this->data['content']      = $this->load->view('admin/blank', $this->data, TRUE);                    
        }

        // make an output
        $this->load->view('admin/content', $this->data);
    }

    // Add a new item
    public function add()
    {
        // check for write access
        $this->menu->check_access('write');

        // get user groups
        $dropdown_groups = array();
        $this->db
        ->select('a.id, a.name')
        ->from('cms_groups a')
        ->join($this->table_name.' b', 'b.groups_id = a.id','left')
        ->group_by('a.id');
        $query = $this->db->get();
        if($query->num_rows() > 0 ) {
            foreach($query->result() as $groups) {
                $dropdown_groups[$groups->id] = $groups->name;
            }
        }
        $this->data['dropdown_groups'] = $dropdown_groups;

        // get modules
        $menu = array();
        $this->db
        ->select('id, name, has_write, has_update, has_delete, show')
        ->from('cms_modules')
        ->where('parent', '0');
        $query = $this->db->get();
        if($query->num_rows() > 0 ) {
            foreach($query->result() as $modul) {
                $menu[$modul->id]['id']         = $modul->id;
                $menu[$modul->id]['menu']       = $modul->name;
                $menu[$modul->id]['show']       = $modul->show;
                $menu[$modul->id]['has_write']  = $modul->has_write;
                $menu[$modul->id]['has_update'] = $modul->has_update;
                $menu[$modul->id]['has_delete'] = $modul->has_delete;
                
                // get menu child
                $this->db
                ->select('id, name, has_write, has_update, has_delete, show')
                ->from('cms_modules')
                ->where('parent', $modul->id);
                $childs = $this->db->get(); 
                if($childs->num_rows() > 0 ) {
                    foreach ($childs->result() as $child) {
                        $menu[$modul->id]['sub'][$child->id]['id']          = $child->id;
                        $menu[$modul->id]['sub'][$child->id]['menu']        = $child->name;
                        $menu[$modul->id]['sub'][$child->id]['show']        = $child->show;
                        $menu[$modul->id]['sub'][$child->id]['has_write']   = $child->has_write;
                        $menu[$modul->id]['sub'][$child->id]['has_update']  = $child->has_update;
                        $menu[$modul->id]['sub'][$child->id]['has_delete']  = $child->has_delete;
                    }
                } else {
                    $menu[$modul->id]['sub'] = array();
                }
            }
        }

        $this->data['menu'] = $menu;

        // validation rules
        $this->form_validation->set_rules('modules[]', 'Modules', 'trim');

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('priviledges/add', $this->data, true);
        } else {
            // if not break the rules 
            // empty the current priviledged
            $this->db->delete($this->table_name, array('groups_id' => $this->input->post('groups')));

            // read input post
            $groups     = $this->input->post('groups');
            $modules    = $this->input->post('modules');
            $record     = array();
            foreach ($modules as $key => $modul) {
                $record[] = array(
                    'groups_id'     => $groups,
                    'modules_id'    => $modul['id'],
                    'created_by'    => $this->session->userdata('user_id'),
                    'created'       => date('Y-m-d H:i:s'),
                    'can_write'     => (isset($modul['write'])) ? 1 : 0,
                    'can_update'    => (isset($modul['update'])) ? 1 : 0,
                    'can_delete'    => (isset($modul['delete'])) ? 1 : 0
                );
            }
            // start transaction
            $this->db->trans_start();
            $this->db->insert_batch($this->table_name, $record);
            $this->db->trans_complete();

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('priviledges/add', $this->data, true);
            } else {
                $this->data['message']      = 'Your record has been saved.';
                $this->data['content']  = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        // change direct button
        $this->data['button_text'] = 'List';
        $this->data['button_link'] = site_url($this->url);

        // make an output
        $this->load->view('admin/content', $this->data);        
    }

    //Update one item
    public function update( $id = NULL )
    {
        // security check
        $this->menu->check_access('update');

        // get user groups
        $dropdown_groups = array();
        $this->db
        ->select('id, name')
        ->from('cms_groups')
        ->where('id', $id);
        $query      = $this->db->get();
        $groups     = $query->row();

        // make output
        $this->data['id']       = $groups->id;
        $this->data['groups']   = $groups->name;

        // get modules
        $menu = array();
        $this->db
        ->select('id, name, has_write, has_update, has_delete, show')
        ->from('cms_modules')
        ->where('parent', '0');
        $query = $this->db->get();
        if($query->num_rows() > 0 ) {
            foreach($query->result() as $modul) {
                $menu[$modul->id]['id']         = $modul->id;
                $menu[$modul->id]['menu']       = $modul->name;
                $menu[$modul->id]['show']       = $modul->show;
                $menu[$modul->id]['has_write']  = $modul->has_write;
                $menu[$modul->id]['has_update'] = $modul->has_update;
                $menu[$modul->id]['has_delete'] = $modul->has_delete;
                $menu[$modul->id]['can_write']  = 0;
                $menu[$modul->id]['can_update'] = 0;
                $menu[$modul->id]['can_delete'] = 0;
                $menu[$modul->id]['access']     = 0;

                // check for priviledges
                $this->db
                ->select('can_write, can_update, can_delete')
                ->from($this->table_name)
                ->where('groups_id', $id)
                ->where('modules_id', $modul->id);
                $check = $this->db->get(); 
                if($check->num_rows() > 0 ) {
                    // change variable value
                    $founded_parent = $check->row();
                    $menu[$modul->id]['can_write']  = $founded_parent->can_write;
                    $menu[$modul->id]['can_update'] = $founded_parent->can_update;
                    $menu[$modul->id]['can_delete'] = $founded_parent->can_delete;
                    $menu[$modul->id]['access']     = 1;
                }
                
                // get menu child
                $this->db
                ->select('id, name, has_write, has_update, has_delete, show')
                ->from('cms_modules')
                ->where('parent', $modul->id);
                $childs = $this->db->get();     
                if($childs->num_rows() > 0 ) {
                    foreach ($childs->result() as $child) {
                        // set output
                        $menu[$modul->id]['sub'][$child->id]['id']          = $child->id;
                        $menu[$modul->id]['sub'][$child->id]['menu']        = $child->name;
                        $menu[$modul->id]['sub'][$child->id]['show']        = $child->show;
                        $menu[$modul->id]['sub'][$child->id]['has_write']   = $child->has_write;
                        $menu[$modul->id]['sub'][$child->id]['has_update']  = $child->has_update;
                        $menu[$modul->id]['sub'][$child->id]['has_delete']  = $child->has_delete;                            
                        $menu[$modul->id]['sub'][$child->id]['can_write']   = 0;
                        $menu[$modul->id]['sub'][$child->id]['can_update']  = 0;
                        $menu[$modul->id]['sub'][$child->id]['can_delete']  = 0;
                        $menu[$modul->id]['sub'][$child->id]['access']      = 0;

                        // check for priviledges
                        $this->db
                        ->select('can_write, can_update, can_delete')
                        ->from($this->table_name)
                        ->where('groups_id', $id)
                        ->where('modules_id', $child->id);
                        $check = $this->db->get(); 
                        $founded_child                                      = $check->row();
                        if($check->num_rows() > 0 ) {
                            // change variable value
                            $menu[$modul->id]['sub'][$child->id]['can_write']   = $founded_child->can_write;
                            $menu[$modul->id]['sub'][$child->id]['can_update']  = $founded_child->can_update;
                            $menu[$modul->id]['sub'][$child->id]['can_delete']  = $founded_child->can_delete;
                            $menu[$modul->id]['sub'][$child->id]['access']      = 1;
                        }
                    }
                } else {
                    $menu[$modul->id]['sub'] = array();
                }
            }
        }

        // for menu output
        $this->data['menu'] = $menu;

        // validation rules
        $this->form_validation->set_rules('modules[]', 'Modules', 'trim');

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('priviledges/edit', $this->data, true);
        } else {
            // if not break the rules 
            // empty the current priviledged
            $this->db->delete($this->table_name, array('groups_id' => $this->input->post('groups')));

            // read input post
            $groups     = $this->input->post('groups');
            $modules    = $this->input->post('modules');
            $record     = array();
            foreach ($modules as $key => $modul) {
                $record[] = array(
                    'groups_id'     => $groups,
                    'modules_id'    => $modul['id'],
                    'created_by'    => $this->session->userdata('user_id'),
                    'created'       => date('Y-m-d H:i:s'),
                    'can_write'     => (isset($modul['write'])) ? 1 : 0,
                    'can_update'    => (isset($modul['update'])) ? 1 : 0,
                    'can_delete'    => (isset($modul['delete'])) ? 1 : 0
                );
            }

            // start transaction
            $this->db->trans_start();
            $this->db->insert_batch($this->table_name, $record);
            $this->db->trans_complete();

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error                   = $this->db->error();
                $this->data['message']      = $db_error['message'];
                $this->data['content']      = $this->load->view('priviledges/edit', $this->data, true);
            } else {
                // change direct button
                $this->data['button_text']  = 'List';
                $this->data['button_link']  = site_url($this->url);
                $this->data['message']      = 'Your record has been saved.';
                $this->data['content']      = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        // change direct button
        $this->data['button_text'] = 'List';
        $this->data['button_link'] = site_url($this->url);

        // make an output
        $this->load->view('admin/content', $this->data);        

    }

    //Delete one item
    public function delete( $id = NULL )
    {
        // security check
        $this->menu->check_access('delete');

        $this->data['button_link'] = site_url($this->url);

        // denied if group is admin
        if($id == 0) {
            $this->data['message']          = 'You can\' remove this Priviledges.';
        } else {        
            // delete by id
            $this->db->trans_start();
            $this->db->where('groups_id', $id);
            $this->db->delete($this->table_name);
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
            } else {
                $message = 'Your record has been removed.';
            }
            $this->data['message']          = $message;
            $this->data['content']          = $this->load->view('admin/redirect', $this->data, TRUE);
        }
        $this->load->view('admin/content', $this->data);        
    }
}

/* End of file Priviledges.php */
/* Location: ./application/modules/admin/controllers/Priviledges.php */
