<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends MX_Controller {

    private $table_name     = 'event';
    private $url            = 'events';
    private $perpage        = 5;
    private $data           = array();
    private $update         = '';
    private $delete         = ''; 

    public function __construct()
    {
        parent::__construct();

        // for security check
        $this->menu->has_access();

        // for header
        $this->data          = $this->header->button_header();
        $this->data['title'] = 'Events';

        // for button link
        $this->data['button_link'] = (($this->uri->segment(2) == 'add') || ($this->uri->segment(2) == 'update')) ? site_url($this->url) : site_url($this->url).'/add' ;

        // for debug purpose only
        $this->output->enable_profiler(false);
    }

    public function index( $offset = 0 )
    {
        // pagination
        $this->db
        ->select('a.id')
        ->from($this->table_name . ' a')
        ->join('event_lang b', 'b.id = a.id', 'left')
        ->group_by('b.id');
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('a.id, b.title, b.place, DATE_FORMAT(a.start_date,\'%d %M %Y\') start_date, DATE_FORMAT(a.due_date, \'%d %M %Y\') due_date')
        ->from($this->table_name . ' a')
        ->join('event_lang b', 'b.id = a.id', 'left')
        ->order_by('a.updated_at','desc')
        ->group_by('b.id')
        ->limit($this->perpage, $offset);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {
            foreach($query->result() as $list) {
                // check for update access
                if($this->menu->crud_access('update')) {
                    $this->update = '<a href="'.site_url($this->url.'/'.'update'.'/'.$list->id).'" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>';
                }

                // check for delete access
                if($this->menu->crud_access('delete')) {
                    $this->delete = '<a href="'.site_url($this->url.'/'.'delete'.'/'.$list->id).'" class="delete btn btn-xs btn-default"><i class="fa fa-trash"></i></a>';
                }

                // return to table data
                $table_row = array(
                    $list->title,
                    $list->place,                 
                    $list->start_date,
                    $list->due_date
                );

                // check for additional link access
                if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                    $table_row = array_merge($table_row, array($this->delete.' '.$this->update));
                }

                $this->table->add_row($table_row);
            }

            // generate table header
            $this->table->set_template(array('table_open' => '<table class="table">'));
            if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                $this->table->set_heading('Event', 'Place', 'From', 'Until', '');
            } else {
                $this->table->set_heading('Category', 'Place', 'From', 'Until');
            }

            // output
            $this->data['list']         = $this->table->generate(); 
            $this->data['content']      = $this->load->view('admin/list', $this->data, TRUE);
        } else {
            // if no data
            $this->data['content']      = $this->load->view('admin/blank', $this->data, TRUE);                    
        }

        // make an output
        $this->load->view('admin/content', $this->data);
    }

    // Add a new item
    public function add()
    {
        // check for write access
        $this->menu->check_access('write');

        // get language list
        $query = $this->db->get('lang');
        $this->data['language'] = $query->result();

        // validation rules
        foreach($this->data['language'] as $lang) {
            $this->form_validation->set_rules("title[".$lang->shortname."]", "Event in ".$lang->name, 'required');
            $this->form_validation->set_rules("place[".$lang->shortname."]", "Place in ".$lang->name, 'required');
        }
        $this->form_validation->set_rules("start", "Start Date", 'required');

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('events/add', $this->data, true);
        } else {
            // save to category first
            $record = array(
                'status'        => $this->input->post('status'),
                'start_date'    => date('Y-m-d', strtotime($this->input->post('start'))),
                'due_date'      => date('Y-m-d', strtotime($this->input->post('due'))),
                'users_id'      => $this->session->userdata('user_id'),
                'created_at'    => date('Y-m-d h:i:s'),
                'updated_at'    => date('Y-m-d h:i:s')
            );

            // start transaction
            $this->db->insert($this->table_name, $record);
            $id = $this->db->insert_id();

            // then repeat by language
            foreach($this->input->post('title') as $key => $val) {
                $record = array(                
                    'id'        => $id,
                    'lang'      => $key,
                    'title'     => $val,
                    'content'   => $this->input->post('desc')[$key],
                    'place'     => $this->input->post('place')[$key]
                );

                $this->db->trans_start();
                $this->db->insert('event_lang', $record);
                $this->db->trans_complete();
            }

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('events/add', $this->data, true);
            } else {
                $this->data['message']  = 'Your record has been saved.';
                $this->data['content']  = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        $this->data['js_files'] = array(
            base_url() . PLUGINS. 'rumbowyg/trumbowyg.min.js'
        );

        $this->data['scripts'] = '
            $(".editor").trumbowyg({
                btns: [
                    ["viewHTML"],
                    ["formatting"],
                    "btnGrp-semantic",
                    ["superscript", "subscript"],
                    ["link"],
                    ["insertImage"],
                    "btnGrp-justify",
                    "btnGrp-lists",
                    ["removeformat"],
                    ["fullscreen"]
                ]                
            });';

        // make an output
        $this->load->view('admin/content', $this->data);        
    }

    // Add a new item
    public function update($id = NULL)
    {
        // check for write access
        $this->menu->check_access('update');

        // get language list
        $query = $this->db->get('lang');
        $this->data['language'] = $query->result();

        // create output by available language
        foreach($this->data['language'] as $lang) {

            // get current data
            $this->db
            ->select('a.*, b.*')
            ->from($this->table_name . ' a')
            ->join('event_lang b', 'b.id = a.id', 'left')
            ->where('a.id', $id)
            ->where('b.lang', $lang->shortname)
            ->order_by('a.updated_at','desc');
            $query = $this->db->get();

            // return data
            $this->data['form'][$lang->shortname]     = $query->row();
        }


        // validation rules
        foreach($this->data['language'] as $lang) {
            $this->form_validation->set_rules("title[".$lang->shortname."]", "Event in ".$lang->name, 'required');
            $this->form_validation->set_rules("place[".$lang->shortname."]", "Place in ".$lang->name, 'required');
        }

        $this->form_validation->set_rules("start", "Start Date", 'required');

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('events/edit', $this->data, true);
        } else {
            // save to category first
            $data = array(
                'status'        => $this->input->post('status'),
                'start_date'    => date('Y-m-d', strtotime($this->input->post('start'))),
                'due_date'      => date('Y-m-d', strtotime($this->input->post('due'))),
                'users_id'      => $this->session->userdata('user_id'),
                'created_at'    => date('Y-m-d h:i:s'),
                'updated_at'    => date('Y-m-d h:i:s')
            );

            // start transaction
            $this->db->where('id', $this->input->post('id'));                
            $this->db->update($this->table_name, $data);

            // then repeat by language
            foreach($this->input->post('title') as $key => $val) {
                $record = array(                
                    'title'     => $val,
                    'content'   => $this->input->post('desc')[$key],
                    'place'     => $this->input->post('place')[$key]
                );

                $this->db->trans_start();
                $this->db
                ->where('lang', $key)    
                ->where('id', $id);
                $this->db->update('event_lang', $record);
                $this->db->trans_complete();
            }

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('events/edit', $this->data, true);
            } else {
                $this->data['message']  = 'Your record has been saved.';
                $this->data['content']  = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }
        $this->data['js_files'] = array(
            base_url() . PLUGINS. 'rumbowyg/trumbowyg.min.js'
        );

        $this->data['scripts'] = '
            $(".editor").trumbowyg({
                btns: [
                    ["viewHTML"],
                    ["formatting"],
                    "btnGrp-semantic",
                    ["superscript", "subscript"],
                    ["link"],
                    ["insertImage"],
                    "btnGrp-justify",
                    "btnGrp-lists",
                    ["removeformat"],
                    ["fullscreen"]
                ]                
            });';

        // make an output
        $this->load->view('admin/content', $this->data);        
    }

    //Delete one item
    public function delete( $id = NULL )
    {
        // security check
        $this->menu->check_access('delete');

        // direct link 
        $this->data['button_link'] = site_url('events');

        // transaction
        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->delete($this->table_name);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $message = 'Failed while removed your record.';
        } else {
            $this->db->where('id', $id);
            $this->db->delete('event_lang');
            $message = 'Your record has been removed.';
        }

        $this->data['message']          = $message;
        $this->data['content']          = $this->load->view('admin/redirect', $this->data, TRUE);
        $this->load->view('admin/content', $this->data);        
    }

    public function getParent($parent_id)
    {   
        $output = '-';
        if(isset($parent_id) && ($parent_id != 0)) {
            $this->db
            ->select('category')
            ->from($this->table_name)
            ->where('id', $parent_id);
            $parents = $this->db->get();
            if($parents->num_rows() > 0 ){
                $parent = $parents->row();                
                $output = $parent->category;            
            }
        }
        return $output;
    }

}

/* End of file Category.php */
/* Location: ./application/modules/admin/controllers/Category.php */
