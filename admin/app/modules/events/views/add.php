<?php if(validation_errors()) : ?>
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Error</strong>
    <hr>
    <?php echo validation_errors(); ?>
</div>
<?php endif ?>
<form action="<?php echo current_url() ?>" method="POST" class="form-horizontal" role="form">
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Please complete the form below.</h3>
    </div>
    <ul class="nav nav-tabs" role="tablist" style="padding-left: 10px;">
        <?php foreach($language as $lang) : ?>
        <li role="presentation" <?php echo ($lang->is_default == 1) ? 'class="active"' : '' ?>><a href="#<?php echo $lang->shortname ?>" aria-controls="<?php echo $lang->shortname ?>" role="tab" data-toggle="tab" style="padding: 25px 15px;"><?php echo $lang->name ?></a></li>
        <?php endforeach ?>
    </ul>
    <div class="box-body tab-content">            
        <?php foreach($language as $lang) : ?>
            <div class="tab-pane <?php echo ($lang->is_default == 1) ? 'active' : '' ?>" id="<?php echo $lang->shortname ?>">
                <div class="form-group">
                    <label for="" class="col-sm-4">Event in <?php echo $lang->name ?></label>
                    <div class="col-sm-8">
                    <input type="text" name="title[<?php echo $lang->shortname ?>]" value="<?php echo set_value("title[".$lang->shortname."]") ?>" class="form-control" id="" placeholder="">
                    </div>
                </div>              
                <div class="form-group">
                    <label for="" class="col-sm-4">Place</label>
                    <div class="col-sm-8">
                    <input type="text" name="place[<?php echo $lang->shortname ?>]" value="<?php echo set_value("place[".$lang->shortname."]") ?>" class="form-control" id="" placeholder="">
                    </div>
                </div>                              
                <div class="form-group">
                    <label for="" class="col-sm-4">Description in <?php echo $lang->name ?></label>
                    <div class="col-sm-8">
                    <textarea name="desc[<?php echo $lang->shortname ?>]" rows="5" class="form-control editor"><?php echo set_value("desc[".$lang->shortname."]") ?></textarea>
                    </div>
                </div>                              
            </div>
        <?php endforeach ?>
        <div class="form-group tm-nopadding">
            <label for="" class="col-sm-4">Start From</label>
            <div class="col-sm-8 col-md-3">
                <div class="input-group date" >
                    <input type="text" class="form-control datepicker" name="start" value="<?php echo set_value('start', date('d/m/Y') ) ?>">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
        </div>                              
        <div class="form-group tm-nopadding">
            <label for="" class="col-sm-4">Until ( If Available )</label>
            <div class="col-sm-8 col-md-3">
                <div class="input-group date" >
                    <input type="text" class="form-control datepicker" name="due" value="<?php echo set_value('due', date('d/m/Y')) ?>">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
        </div>                              
        <div class="form-group tm-nopadding">
            <label for="" class="col-sm-4">Status</label>
            <div class="col-sm-8 col-md-3">
                <?php echo form_dropdown('status', array('draft' => 'Draft', 'publish' => 'Published'), set_value('status', 'draft'), 'class="form-control"' ) ?>
            </div>
        </div>                              
    </div>
    <div class="panel-footer text-right">
    <a href="<?php echo site_url('category') ?>" class="btn tm-btn">Cancel</a>
    <button type="submit" class="btn tm-btn">Submit</button>
    </div>
</div>
</form>