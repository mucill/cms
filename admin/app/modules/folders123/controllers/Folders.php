<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Folders extends MX_Controller {

    private $table_name     = 'category';
    private $url            = 'folders';
    private $perpage        = 10;
    private $data           = array();
    private $update         = '';
    private $delete         = '';

    public function __construct()
    {
        parent::__construct();

        // for security check
        $this->menu->has_access();

        // for header
        $this->data                 = $this->header->button_header();
        $this->data['title']        = 'Folder';

        // set perpage output
        $this->perpage              =  get_conf('perpage');

        // for button link
        $this->data['button_link']  = (($this->uri->segment(2) == 'add') || ($this->uri->segment(2) == 'update')) ? site_url($this->url) : site_url($this->url).'/add' ;

        // for debug purpose only
        $this->output->enable_profiler(false);
    }

    public function index( $offset = 0 )
    {
        // pagination 
        $this->db
        ->select('id')
        ->from($this->table_name)
        ->where('type', 'post');
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('id, name, slug')
        ->from($this->table_name)
        ->where('type', 'folder')
        ->order_by('name')
        ->group_by('id')
        ->limit($this->perpage, $offset);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {
            foreach($query->result() as $list) {
                // check for update access
                if($this->menu->crud_access('update')) {
                    $this->update = '<a href="'.site_url($this->url.'/'.'update'.'/'.$list->id).'" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></a>';
                }

                // check for delete access
                if($this->menu->crud_access('delete')) {
                    $this->delete = '<a href="'.site_url($this->url.'/'.'delete'.'/'.$list->id).'" class="delete btn btn-xs btn-default"><i class="fa fa-trash"></i></a>';
                }

                // return to table data
                $table_row = array(
                    $list->name,
                    $list->slug
                );

                // check for additional link access
                if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                    $table_row = array_merge($table_row, array($this->delete.' '.$this->update));
                }

                $this->table->add_row($table_row);
            }

            // generate table header
            $this->table->set_template(array('table_open' => '<table class="table">'));
            if($this->menu->crud_access('update') || $this->menu->crud_access('delete')) {
                $this->table->set_heading('Folder', 'Alias', '');
            } else {
                $this->table->set_heading('Folder', 'Alias');
            }

            // output
            $this->data['list']         = $this->table->generate(); 
            $this->data['content']      = $this->load->view('admin/list', $this->data, TRUE);
        } else {
            // if no data
            $this->data['content']      = $this->load->view('admin/blank', $this->data, TRUE);                    
        }

        // make an output
        $this->load->view('admin/content', $this->data);
    }

    // Add a new item
    public function add()
    {
        // check for write access
        $this->menu->check_access('write');

        // validation rules
        $this->form_validation->set_rules("category", "Category", 'required');

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('folders/add', $this->data, true);
        } else {
            // save to category first
            $record = array(
                'type'          => 'folder',
                'is_default'    => $this->input->post('default'),
                'name'          => $this->input->post('category'),
                'slug'          => $this->input->post('slug'),
                'created_at'    => date('Y-m-d h:i:s'),
                'updated_at'    => date('Y-m-d h:i:s')
            );

            // start transaction
            $this->db->insert($this->table_name, $record);
            $id = $this->db->insert_id();

            // check for status
            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('category/add', $this->data, true);
            } else {
                $this->data['message']  = 'Your record has been saved.';
                $this->data['content']  = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        // make an output
        $this->load->view('admin/content', $this->data);        
    }

    // Add a new item
    public function update($id = NULL)
    {
        // check for write access
        $this->menu->check_access('write');


        // get current data
        $this->db
        ->from('category')
        ->where('id', $id)
        ->where('type', 'folder');
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            // return data
            $this->data['form']     = $query->row();
        } else {
            return show_error('Your data might has been removed or changes.','404','Data Not Found');
            exit;                
        }

        // validation rules
        $this->form_validation->set_rules("category", "Category's Name", 'required');

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['content'] = $this->load->view('folders/edit', $this->data, true);
        } else {
            // save to category first
            $record = array(
                'type'          => 'folder',
                'is_default'    => $this->input->post('default'),
                'name'          => $this->input->post('category'),
                'slug'          => $this->input->post('slug'),
                'created_at'    => date('Y-m-d h:i:s'),
                'updated_at'    => date('Y-m-d h:i:s')
            );

            // start transaction
            $this->db->trans_start();
            $this->db->where('id', $this->input->post('id'));
            $this->db->update($this->table_name, $record);
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $db_error               = $this->db->error();
                $this->data['message']  = $db_error['message'];
                $this->data['content']  = $this->load->view('folders/edit', $this->data, true);
            } else {
                $this->data['message']  = 'Your record has been saved.';
                $this->data['content']  = $this->load->view('admin/redirect', $this->data, TRUE);
            }
        }

        // make an output
        $this->load->view('admin/content', $this->data);        
    }

    //Delete one item
    public function delete( $id = NULL )
    {
        // security check
        $this->menu->check_access('delete');

        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->delete($this->table_name);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $message = 'Fail while removed your record.';
        } else {
            $message = 'Your record has been removed.';
        }
        $this->data['message']          = $message;
        $this->data['content']          = $this->load->view('admin/redirect', $this->data, TRUE);
        $this->load->view('admin/content', $this->data);        
    }

    public function getParent($parent_id)
    {   
        $output = '-';
        if(isset($parent_id) && ($parent_id != 0)) {
            $this->db
            ->select('category')
            ->from($this->table_name)
            ->where('id', $parent_id);
            $parents = $this->db->get();
            if($parents->num_rows() > 0 ){
                $parent = $parents->row();                
                $output = $parent->category;            
            }
        }
        return $output;
    }

}

/* End of file Category.php */
/* Location: ./application/modules/admin/controllers/Category.php */
