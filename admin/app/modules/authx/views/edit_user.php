<?php echo form_open(current_url(), 'class="form-horizontal" role="form"');?>
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Profile of <?php echo $first_name['value'] ?> <?php echo $last_name['value'] ?></h3>
        </div>
        <div class="box-body tm-padding">
            <div class="form-group">
                <label for="" class="col-sm-4">First Name</label>
                <div class="col-sm-8">
                <?php echo form_input($first_name,'','class="form-control"');?>
                </div>
            </div>
                 
            <div class="form-group">
                <label for="" class="col-sm-4">Last Name</label>
                <div class="col-sm-8">
                <?php echo form_input($last_name,'','class="form-control"');?>
                </div>
            </div>                                 
            <div class="form-group">
                <label for="" class="col-sm-4">Departmen</label>
                <div class="col-sm-8">
                <?php echo form_input($company,'','class="form-control"');?>
                </div>
            </div>                                 
            <div class="form-group">
                <label for="" class="col-sm-4">Phone</label>
                <div class="col-sm-8">
                <?php echo form_input($phone,'','class="form-control"');?>
                </div>
            </div>    
        </div>
    </div>
    <?php if ($this->ion_auth->is_admin()): ?>
    <div class="box">
        <div class="box-header with-border">
            <h1 class="box-title">Change User Group Priviledges</h1>
        </div>
        <div class="box-body tm-padding tm-nopadding-top tm-nopadding-bottom">
            <div class="form-group">
            <?php foreach ($groups as $group):?>
                <div class="col-md-3">
                <div class="radio">
              <?php
                  $gID=$group['id'];
                  $checked = null;
                  $item = null;
                  foreach($currentGroups as $grp) {
                      if ($gID == $grp->id) {
                          $checked= ' checked="checked"';
                      break;
                      }
                  }
              ?>
                <label>
                <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>><div style="margin-top:2px;"><div style="padding-top:2px;"><?php echo ucwords(htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8'));?></div></div>
                </label>
              </div>
              </div>
          <?php endforeach?>
          </div>
        </div>
    </div>
    <?php endif ?>
    <div class="box">
        <div class="box-header with-border">
            <h1 class="box-title">Change Password
            <br>
            <small>Just leave it blank to use your current password. Min. 8 characters</small>
            </h1>
        </div>
        <div class="box-body tm-padding">
            <div class="form-group">
                <label for="" class="col-sm-4">New Password</label>
                <div class="col-sm-8">
                <?php echo form_input($password,'','class="form-control"');?>
                </div>
            </div>                                 
            <div class="form-group">
                <label for="" class="col-sm-4">Retype New Password</label>
                <div class="col-sm-8">
                <?php echo form_input($password_confirm,'','class="form-control"');?>
                </div>
            </div>                                 
        </div>
    <div class="panel-footer text-right">
    <?php echo form_hidden($csrf); ?>
    <?php echo form_hidden('id', $user->id);?>
    <a href="<?php echo site_url('auth') ?>" class="btn tm-btn">Cancel</a>
    <button type="submit" class="btn tm-btn">Submit</button>
    </div>
    </div>

<?php echo form_close();?>