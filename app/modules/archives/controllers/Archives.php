<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Archives extends MX_Controller {

    public $data            = array();
    private $url            = 'archives';
    private $perpage        = 5;

    public function __construct()
    {
        parent::__construct();

        // page title
        $this->data['title'] = __('archives', false);

        // set default language
        $this->media->set_language();

        // for debug purpose
        $this->output->enable_profiler(false);
    }

    public function index($offset = 0)
    {
        // pagination
        $this->db
        ->select('a.id')
        ->from('page_lang a')
        ->where('b.type', 'post')
        ->where('a.lang', $this->session->userdata('lang'))
        ->join('page b', 'b.id = a.id');
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('a.id, a.title, a.intro, a.slug, b.created_at, b.media_id, b.updated_at, b.users_id, b.category_id')
        ->from('page_lang a')
        ->where('b.type', 'post')
        ->where('a.lang', $this->session->userdata('lang'))
        ->join('page b', 'b.id = a.id')
        ->limit($this->perpage, $offset );
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {

            // most view
            $this->data['view_most']      = $this->media->blog_view_most();
            $this->data['view_latest']    = $this->media->blog_view_latest();

            // galeri
            $this->data['gallery']        = $this->media->images_list(4, 5); // $cat_id, $limit
            $this->data['footer_gallery'] = $this->media->images_list(4, 6); // $cat_id, $limit

            // generate output
            $this->data['category']     = $this->media->category_list('blogs');
            $this->data['blog']         = $query->result();
            $this->data['content']      = $this->load->view('list', $this->data, true);

        } else {
            // if no data
            $this->data['message']      = 'No News Availble';
            $this->data['content']      = $this->load->view('master/blank', $this->data, true);
        }

        // Generate output
        $this->load->view('master/content', $this->data);
    }

    public function category($slugs = '', $offset = 0)
    {
        // pagination
        $this->db
        ->select('a.id')
        ->from('page_lang a')
        ->where('b.type', 'post')
        ->where('c.slug', $slugs)
        ->where('a.lang', $this->session->userdata('lang'))
        ->join('page b', 'b.id = a.id')
        ->join('category_lang c', 'c.id = b.category_id', 'left');
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('a.id, a.title, a.intro, a.slug, b.created_at, b.media_id, b.updated_at, b.users_id, b.category_id')
        ->from('page_lang a')
        ->where('b.type', 'post')
        ->where('c.slug', $slugs)
        ->where('a.lang', $this->session->userdata('lang'))
        ->join('page b', 'b.id = a.id')
        ->join('category_lang c', 'c.id = b.category_id', 'left')
        ->limit($this->perpage, $offset );
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {

            // most view
            $this->data['view_most']      = $this->media->blog_view_most();
            $this->data['view_latest']    = $this->media->blog_view_latest();

            // galeri
            $this->data['gallery']        = $this->media->images_list(4, 5); // $cat_id, $limit
            $this->data['footer_gallery'] = $this->media->images_list(4, 6); // $cat_id, $limit

            // generate output
            $this->data['category']     = $this->media->category_list('blogs');
            $this->data['blog']         = $query->result();
            $this->data['content']      = $this->load->view('list', $this->data, true);

        } else {
            // if no data
            $this->data['message']      = 'No News Availble';
            $this->data['content']      = $this->load->view('master/blank', $this->data, true);
        }

        // Generate output
        $this->load->view('master/content', $this->data);
    }

}
