<?php if(count($blog) > 0): ?>
<section class="container clearfix topmargin">
    <div class="postcontent nobottommargin clearfix">
        <div id="posts" class="small-thumbs">
            <?php foreach ($blog as $list): ?>
            <div class="entry clearfix">
                <?php if($list->media_id != 0): ?>
                <div class="entry-image">
                    <a href="<?php echo base_url('/read').'/'.$list->slug ?>"><img class="image_fade" src="<?php echo site_url('media/') .'/'. $this->media->images($list->media_id) ?>" alt="<?php echo $list->title ?>"></a>
                </div>
                <?php endif ?>
                <div class="entry-c">
                <div class="entry-title">
                    <h2><a href="<?php echo base_url('/read/'.$list->slug) ?>"><?php echo $list->title ?></a></h2>
                </div>
                <ul class="entry-meta clearfix">
                    <li><i class="icon-calendar3"></i> <?php echo mdate('%d.%m.%Y', strtotime($list->updated_at)) ?></li>
                    <li><i class="icon-user"></i> <?php echo $this->media->author($list->users_id) ?></li>
                    <li><i class="icon-folder"></i><a href="<?php echo site_url('archives/category'.'/'.$this->media->category_slug($list->category_id)) ?>"><?php echo $this->media->category($list->category_id) ?></a></li>
                </ul>
                <div class="entry-content">
                    <p><?php echo word_limiter($list->intro, 20) ?></p>
                    <a href="<?php echo base_url('/read'.'/'.$list->slug) ?>"class="more-link">Selengkapnya</a>
                </div>
                </div>
            </div>
            <?php endforeach ?>
            <?php echo $pagination ?>
        </div>
    </div>
    <?php $this->load->view('sidebar') ?>
</section>
<?php else: ?>
<?php //show_404() ?>
<?php endif ?>
