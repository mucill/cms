<section class="container clearfix topmargin">
    <div class="postcontent nobottommargin notopmmargin clearfix">
        <div class="single-post nobottommargin">
            <div class="entry clearfix noborder">
                <div class="entry-title">
                    <h2><?php echo $sch->title ?></h2>
                </div>

                <ul class="entry-meta clearfix">
                    <li><i class="icon-calendar3"></i> <?php echo mdate('%d.%m.%Y', strtotime($sch->start_date)) ?></li>
                    <li><i class="icon-user"></i> <?php echo $sch->place ?></li>
                </ul>

                <div class="entry-content notopmargin">
                    <?php echo $sch->content ?>
                </div>
                 

            </div>
                <p><a href="<?php echo site_url('schedule') ?>" class="button button-3d button-black">Back</a></p>
        </div>
    </div>
    <?php $this->load->view('sidebar') ?>
</section>