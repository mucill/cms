<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Instansi extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model(array('instansi_model', 'crudMod'));
		$this->load->library(array('form_validation','session'));
		$this->load->helper(array('text','date','form','security','string','url'));
	}
	
	public function index()
	{
		if(empty($this->session->userdata['email']))
		{
			redirect(base_url('login'));
			
		} else {
			
			$data = $this->session->userdata;
			$data['Avatar'] = $this->crudMod->getAvatar($data);
			$data['memBio'] = $this->instansi_model->getOff($data);
			
			$validasi = array(
				array(
					'field'			=> 'prof_offc_name',
					'label'			=> 'Nama Instansi',
					'rules'			=> 'required',
					'errors'		=> array(
						'required' 	=> 'Mohon isikan nama instansi anda.'
					),
				),
				array(
					'field'			=> 'prof_offc_addr',
					'label'			=> 'Alamat Instansi',
					'rules'			=> 'required',
					'errors'		=> array(
						'required' 	=> 'Mohon isikan alamat instansi anda.'
					),
				),
				array(
					'field'			=> 'prof_offc_city',
					'label'			=> 'Kab./Kota',
					'rules'			=> 'required',
					'errors'		=> array(
						'required' 	=> 'Mohon isikan kab./kota instansi anda.'
					),
				),
			);
			
			$this->form_validation->set_rules($validasi);
			$data['custom_error']	= '';
			$data['success']		= '';
			
			if(isset($_POST['submit'])) {
			
				if ($this->form_validation->run() == FALSE)
				{
					// If Failed
					$data['custom_error'] 	= validation_errors('<li><i class="icon-line-square-check"></i> ', '</li>');

				} else {
					
					// If Success
					$clean = $this->security->xss_clean($this->input->post(NULL, TRUE));
					$this->instansi_model->addOff($clean);
					$data['success'] = '<li><i class="icon-line-square-check"></i>Data anda berhasil disimpan.</li>';
					redirect(base_url('dashboard/profile'));
				}
			}
			
			$view['content'] 		= $this->load->view('instansi', $data, TRUE);
			$view['footer_gallery']	= $this->media->images_list(4, 6);
			$view['blog']			= 'Members Private Page';
			$this->load->view('master/full', $view);
		}
	}
	
	public function logout()
	{
		$this->load->library('session');
		$this->session->sess_destroy();
		redirect(base_url().'member/');
	}
}
