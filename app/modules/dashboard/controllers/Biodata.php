<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Biodata extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('crudMod');
		$this->load->library(array('form_validation','session'));
		$this->load->helper(array('text','date','form','security','string','url'));
	}
	
	public function index()
	{
		if(empty($this->session->userdata['email']))
		{
			redirect(base_url('login'));
			
		} else {
			
			$data				= $this->session->userdata;
			$profID				= $this->uri->segment(3);
			$data['Avatar']		= $this->crudMod->getAvatar($data);
			$data['getProf']	= $this->crudMod->getProf($data);
			
			$validasi = array(
				array(
					'field'			=> 'prof_birth_city',
					'label'			=> 'Tempat Lahir',
					'rules'			=> 'required',
					'errors'		=> array(
						'required' 	=> 'Mohon isikan tempat lahir anda.'
					),
				),
				array(
					'field'			=> 'prof_birth_date',
					'label'			=> 'Tanggal Lahir',
					'rules'			=> 'required',
					'errors'		=> array(
						'required' 	=> 'Mohon isikan tanggal lahir anda.'
					),
				),
				array(
					'field'			=> 'prof_gender',
					'label'			=> 'Jenis Kelamin',
					'rules'			=> 'required',
					'errors'		=> array(
						'required' 	=> 'Mohon pilih salah satu jenis kelamin anda.'
					),
				),
				array(
					'field'			=> 'prof_phone',
					'label'			=> 'Nomor Ponsel',
					'rules'			=> 'required|numeric|min_length[8]',
					'errors'		=> array(
						'required' 	=> 'Mohon isikan nomor ponsel anda yang masih aktif.',
						'numeric'	=> 'Mohon isikan nomor ponsel hanya dengan angka.',
						'min_length'=> 'Nomor ponsel minimal 8 angka.'
					),
				),
			);
			
			$this->form_validation->set_rules($validasi);
			$data['custom_error']	= '';
			$data['success']		= '';
			
			if(isset($_POST['submit'])) {
			
				if ($this->form_validation->run() == FALSE)
				{
					// If Failed
					$data['custom_error'] 	= validation_errors('<li><i class="icon-line-square-check"></i> ', '</li>');

				} else {
					
					// If Success
					$clean = $this->security->xss_clean($this->input->post(NULL, TRUE));
					$this->crudMod->addBio($clean);
					$data['success'] = '<li><i class="icon-line-square-check"></i>Data anda berhasil disimpan.</li>';
					redirect(base_url('dashboard/profile'));
				}
			}
			
			$view['content'] 		= $this->load->view('biodata', $data, TRUE);
			$view['footer_gallery']	= $this->media->images_list(4, 6);
			$view['blog']			= 'Members Private Page';
			$this->load->view('master/full', $view);
		}
	}
	
	public function logout()
	{
		$this->load->library('session');
		$this->session->sess_destroy();
		redirect(base_url().'member/');
	}
}