<section class="container clearfix">
	<div class="row">
		<div class="panel panel-default noshadow">
			<div class="panel-heading nobottompadding mbrMenu">
				<ul class="nav nav-tabs">
					<li><a href="<?php echo base_url(); ?>dashboard">Dashboard</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/profile">Profile</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/biodata">Biodata</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/instansi">Instansi</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/pendidikan">Pendidikan</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/jabatan">Jabatan</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/publikasi">Publikasi</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/penelitian">Riset</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/pengabdian">Pengabdian</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/logout">Logout</a></li>
				</ul>
			</div>
			<div class="panel-body norightpadding noleftpadding">
				<div class="tab-content">
					<div class="col-md-4 panel nobottommargin">
<?php $this->load->view('avatar'); ?>
					</div>
					<div class="col-md-8 panel nobottommargin">
<?php foreach ($getJabID as $jab):

$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
if (strpos($url,'update') == false): ?>
						<!-- FORM JABATAN BEGIN -->
						<?php echo form_open(base_url('dashboard/jabatan/add'), 'class="form-horizontal nobottommargin" id="biodata-form" name="biodata-form"') ?>
<?php if($custom_error == ''): ?>
							<h4 class="nomargin">Data Jabatan</h4>
							Mohon lengkapi formulir riwayat jabatan anggota berikut ini.
							<hr />
<?php else: ?>
<?php $this->load->view('submit_error') ?>
<?php endif; ?>
<?php if($success == ''): ?>
<?php else: ?>
<?php $this->load->view('submit_success') ?>
<?php endif; ?> 
							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_jbt_type">Jenis Jabatan</label>
								<div class="col-md-8">
									<input id="prof_jbt_type" name="prof_jbt_type" class="form-control input-md" type="text">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_jbt_offc">Institusi</label>
								<div class="col-md-8">
									<input id="prof_jbt_offc" name="prof_jbt_offc" class="form-control input-md" type="text">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_jbt_year">Periode Jabatan</label>
								<div class="col-md-8">
									<input id="prof_jbt_year" name="prof_jbt_year" class="form-control input-md" type="text">
								</div>
							</div>
							
							<div class="form-group nobottommargin">
								<?php echo form_hidden('member_id', $member_id);?>
								<div class="text-right col-md-12">
									<div id="button1idGroup" class="btn-group" role="group" aria-label="Button Group">
										<button type="submit" class="btn btn-primary nomargin" id="form-submit" name="submit" value="submit">Simpan</button>
									</div>
								</div>
							</div>
						</form>
						<!-- FORM JABATAN END -->
<?php else:

foreach ($getProf as $prof):
endforeach;

if( $prof->member_id === $jab->member_id ): ?>
						<!-- FORM JABATAN BEGIN -->
						<?php echo form_open(base_url('dashboard/jabatan/update/'.$jab->jabatan_id), 'class="form-horizontal nobottommargin" id="biodata-form" name="biodata-form"') ?>
<?php if($custom_error == ''): ?>
							<h4 class="nomargin">Data Jabatan</h4>
							Mohon lengkapi formulir riwayat jabatan anggota berikut ini.
							<hr />
<?php else:
	$this->load->view('submit_error');
endif;
if($success == ''):
else:
	$this->load->view('submit_success');
endif; ?>
							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_jbt_type">Jenis Jabatan</label>
								<div class="col-md-8">
									<input id="prof_jbt_type" name="prof_jbt_type" value="<?php echo $jab->jbt_type; ?>" class="form-control input-md" type="text">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_jbt_offc">Institusi</label>
								<div class="col-md-8">
									<input id="prof_jbt_offc" name="prof_jbt_offc" value="<?php echo $jab->jbt_office; ?>" class="form-control input-md" type="text">
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="prof_jbt_year">Periode Jabatan</label>
								<div class="col-md-8">
									<input id="prof_jbt_year" name="prof_jbt_year" value="<?php echo $jab->jbt_periode; ?>" class="form-control input-md" type="text">
								</div>
							</div>
							 
							<div class="form-group nobottommargin">
								<?php echo form_hidden('member_id', $member_id);?>
								<div class="text-right col-md-12">
									<div id="button1idGroup" class="btn-group" role="group" aria-label="Button Group">
										<button type="submit" class="btn btn-primary nomargin" id="form-submit" name="update" value="update">Simpan</button>
									</div>
								</div>
							</div>
						</form>
						<!-- FORM JABATAN END -->
<?php else: ?>
						<div class="panel panel-danger">
							<div class="panel-heading">
								<h4 class="nomargin" style="color:red">Galat</h4>
							</div>
							<div class="panel-body">
								<ul class="iconlist nobottommargin" style="line-height: 2;">
									<h4 class="nobottommargin">Maaf, anda mencoba untuk memasuki halaman yang belum tersedia.</h4>
								</ul>
							</div>
						</div>
<?php
endif;
endif;
endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>