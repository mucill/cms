<section class="container clearfix">
	<div class="row">
		<div class="panel panel-default noshadow">
			<div class="panel-heading nobottompadding mbrMenu">
				<ul class="nav nav-tabs">
					<li><a href="<?php echo base_url(); ?>dashboard">Dashboard</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/profile">Profile</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/biodata">Biodata</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/instansi">Instansi</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/pendidikan">Pendidikan</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/jabatan">Jabatan</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/publikasi">Publikasi</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/penelitian">Riset</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/pengabdian">Pengabdian</a></li>
					<li><a href="<?php echo base_url(); ?>dashboard/logout">Logout</a></li>
				</ul>
			</div>
			<div class="panel-body norightpadding noleftpadding">
				<div class="tab-content">
					<div class="col-md-4 panel nobottommargin">
<?php $this->load->view('avatar'); ?>
					</div>

<?php foreach ($getData as $dat): ?>
					<div class="col-md-8 panel nobottommargin">
						<p>
							Selamat Datang <?php echo $dat->realname; ?><br>
						</p>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>