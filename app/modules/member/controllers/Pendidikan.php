<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendidikan extends MX_Controller {

    public $data            = array();
    private $url            = 'member/pendidikan';
    private $perpage        = 6;
    private $member_mail    = '';


    public function __construct()
    {
        parent::__construct();

        // load library and model
        $this->load->model(array('auth_model','CrudMod'));

        // set default language
        $this->media->set_language();

        // set default value
        $this->data['title']    = 'Data Anggota';
        $this->data['propinsi']       = array(
                                    '11' => 'Aceh',
                                    '12' => 'Sumatera Utara',
                                    '13' => 'Sumatera Barat',
                                    '14' => 'Riau',
                                    '15' => 'Jambi',
                                    '16' => 'Sumatera Selatan',
                                    '17' => 'Bengkulu',
                                    '18' => 'Lampung',
                                    '19' => 'Kepulauan Bangka Belitung',
                                    '21' => 'Kepulauan Riau',
                                    '31' => 'Dki Jakarta',
                                    '32' => 'Jawa Barat',
                                    '33' => 'Jawa Tengah',
                                    '34' => 'Di Yogyakarta',
                                    '35' => 'Jawa Timur',
                                    '36' => 'Banten',
                                    '51' => 'Bali',
                                    '52' => 'Nusa Tenggara Barat',
                                    '53' => 'Nusa Tenggara Timur',
                                    '61' => 'Kalimantan Barat',
                                    '62' => 'Kalimantan Tengah',
                                    '63' => 'Kalimantan Selatan',
                                    '64' => 'Kalimantan Timur',
                                    '65' => 'Kalimantan Utara',
                                    '71' => 'Sulawesi Utara',
                                    '72' => 'Sulawesi Tengah',
                                    '73' => 'Sulawesi Selatan',
                                    '74' => 'Sulawesi Tenggara',
                                    '75' => 'Gorontalo',
                                    '76' => 'Sulawesi Barat',
                                    '81' => 'Maluku',
                                    '82' => 'Maluku Utara',
                                    '91' => 'Papua Barat',
                                    '94' => 'Papua');

        // for debug purpose only
        $this->output->enable_profiler(false);

    }

    public function index($action = '', $id = '')
    {
        // security check
        $this->is_logged();

        // get current data
        $this->db->select('avatar');
        $query = $this->db->get_where('member', array('member_id'=> $this->session->userdata['member_id']));
        $this->data['prof']     = $query->row();

        // show list output
        $query = $this->db->get_where('member_edu', array('member_id'=> $this->session->userdata['member_id']));
        $this->data['education']    = $query->result();
        $this->data['edu']          = $query->row();


            // validation rules
            $this->form_validation->set_rules('prof_nama', 'Nama Lengkap', 'required');

            // if break the rules
            if ($this->form_validation->run() == FALSE) {
                $this->data['member_content']   = $this->load->view('pendidikan/list', $this->data, true);
                $this->data['content']          = $this->load->view('member/dashboard', $this->data, true);
            } else {
                // if not break the rules
                $record = array(
                    'member_id'     => $this->input->post('member_id'),
                    'realname'      => $this->input->post('prof_nama'),
                    'birth_city'    => $this->input->post('prof_birth_city'),
                    'birth_date'    => $this->input->post('prof_birth_date'),
                    'gender'        => $this->input->post('prof_gender'),
                    'home_addr'     => $this->input->post('prof_home_addr'),
                    'home_city'     => $this->input->post('prof_home_city'),
                    'home_prov'     => $this->input->post('prof_home_prov'),
                    'phone'         => $this->input->post('prof_phone'),
                    'ahli_bid'      => $this->input->post('prof_ahli'),
                    'website'       => $this->input->post('prof_web'),
                    'socmed_fb'     => $this->input->post('prof_fb'),
                    'socmed_tw'     => $this->input->post('prof_tw'),
                    'last_update'   => date('Y-m-d H:i:s')
                );

                // start transaction
                $this->db->where('member_id', $this->input->post('member_id'));
                $update  = $this->db->update('member', $record);

                // check for status
                if ($update) {
                    $this->data['message']          = 'Your record has been updated.';
                    $this->data['button_link']      = 'profile';
                    $this->data['content']          = $this->load->view('master/redirect', $this->data, TRUE);
                } else {
                    $this->data['member_content']   = $this->load->view('pendidkan/list', $this->data, true);
                    $this->data['content']          = $this->load->view('member/dasboard', $this->data, true);
                }
            }

        // make an output
        $this->data['footer_gallery'] = $this->media->images_list(4, 6);
        $this->load->view('master/content', $this->data);

    }

    public function add()
    {

        // security check
        $this->is_logged();

        // get current data
        $this->db->select('avatar');
        $query = $this->db->get_where('member', array('member_id'=> $this->session->userdata['member_id']));
        $this->data['prof']     = $query->row();

        $this->data['edu']              = new stdClass();
        $this->data['edu']->sarjana     = NULL;
        $this->data['edu']->jurusan     = NULL;
        $this->data['edu']->kampus      = NULL;
        $this->data['edu']->lulus       = NULL;
        $this->data['edu']->skripsi     = NULL;
        $this->data['edu']->edu_id      = NULL;
        $this->data['edu']->member_id   = $this->session->userdata('member_id');

        // for any submit
        if(isset($_POST['submit'])) {
            $record = array(
                'sarjana'   => $this->input->post('prof_edu_level'),
                'jurusan'   => $this->input->post('prof_edu_jur'),
                'kampus'    => $this->input->post('prof_edu_camp'),
                'lulus'     => $this->input->post('prof_edu_year'),
                'skripsi'   => $this->input->post('prof_edu_title'),
                'member_id' => $this->input->post('member_id')
            );

            $this->db->insert('member_edu', $record);
            redirect(site_url('member/pendidikan'),'refresh');
        }


        // make an output
        $this->data['custom_error']     = '';
        $this->data['success']          = '';
        $this->data['member_content']   = $this->load->view('pendidikan/add', $this->data, true);
        $this->data['content']          = $this->load->view('dashboard', $this->data, true);
        $this->data['footer_gallery'] = $this->media->images_list(4, 6);
        $this->load->view('master/content', $this->data);

    }


    public function edit($id)
    {

        // security check
        $this->is_logged();

        // get current data
        $this->db->select('avatar');
        $query = $this->db->get_where('member', array('member_id'=> $this->session->userdata['member_id']));
        $this->data['prof']     = $query->row();

        // show list output
        $this->db
        ->where('member_id', $this->session->userdata['member_id'])
        ->where('edu_id', $id);
        $query = $this->db->get('member_edu');
        $this->data['education']    = $query->result();
        $this->data['edu']          = $query->row();

        // for any submit
        if(isset($_POST['submit'])) {
            $record = array(
                'sarjana'   => $this->input->post('prof_edu_level'),
                'jurusan'   => $this->input->post('prof_edu_jur'),
                'kampus'    => $this->input->post('prof_edu_camp'),
                'lulus'     => $this->input->post('prof_edu_year'),
                'skripsi'   => $this->input->post('prof_edu_title'),
                'member_id' => $this->session->userdata['member_id']
            );

            $this->db
            ->where('member_id', $this->session->userdata['member_id'])
            ->where('edu_id', $this->input->post('edu_id'));
            $this->db->update('member_edu', $record);
            redirect(site_url('member/pendidikan'),'refresh');
        }


        // make an output
        $this->data['custom_error']     = '';
        $this->data['success']          = '';
        $this->data['member_content']   = $this->load->view('pendidikan/add', $this->data, true);
        $this->data['content']          = $this->load->view('dashboard', $this->data, true);
        $this->data['footer_gallery'] = $this->media->images_list(4, 6);
        $this->load->view('master/content', $this->data);

    }
    public function remove($id)
    {
        $this->db
        ->where('member_id', $this->session->userdata['member_id'])
        ->where('edu_id', $id);
        $this->db->delete('member_edu');
        redirect(site_url('member/pendidikan'),'refresh');
    }

    public function is_logged()
    {
        if(empty($this->session->userdata['email'])) {
            redirect(base_url('member/login'));
            exit;
        }
    }
}
