<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publikasi extends CI_Controller {

    public $data            = array();
    private $url            = 'member/pendidikan';
    private $perpage        = 2;
    private $member_mail    = '';


    public function __construct()
    {
        parent::__construct();

        // load library and model
        $this->load->model(array('auth_model','CrudMod'));
        $this->load->library('upload');
        $this->load->helper('download');

        // set default language
        $this->media->set_language();

        // set default value
        $this->data['title']    = 'Data Anggota';
        $this->data['propinsi'] = array(
                                    '11' => 'Aceh',
                                    '12' => 'Sumatera Utara',
                                    '13' => 'Sumatera Barat',
                                    '14' => 'Riau',
                                    '15' => 'Jambi',
                                    '16' => 'Sumatera Selatan',
                                    '17' => 'Bengkulu',
                                    '18' => 'Lampung',
                                    '19' => 'Kepulauan Bangka Belitung',
                                    '21' => 'Kepulauan Riau',
                                    '31' => 'Dki Jakarta',
                                    '32' => 'Jawa Barat',
                                    '33' => 'Jawa Tengah',
                                    '34' => 'Di Yogyakarta',
                                    '35' => 'Jawa Timur',
                                    '36' => 'Banten',
                                    '51' => 'Bali',
                                    '52' => 'Nusa Tenggara Barat',
                                    '53' => 'Nusa Tenggara Timur',
                                    '61' => 'Kalimantan Barat',
                                    '62' => 'Kalimantan Tengah',
                                    '63' => 'Kalimantan Selatan',
                                    '64' => 'Kalimantan Timur',
                                    '65' => 'Kalimantan Utara',
                                    '71' => 'Sulawesi Utara',
                                    '72' => 'Sulawesi Tengah',
                                    '73' => 'Sulawesi Selatan',
                                    '74' => 'Sulawesi Tenggara',
                                    '75' => 'Gorontalo',
                                    '76' => 'Sulawesi Barat',
                                    '81' => 'Maluku',
                                    '82' => 'Maluku Utara',
                                    '91' => 'Papua Barat',
                                    '94' => 'Papua');

        // for debug purpose only
        $this->output->enable_profiler(false);

    }

    public function index($action = '', $id = '')
    {
        // security check
        $this->is_logged();

        // get current data
        $this->db
        ->select('avatar');

        $query = $this->db->get_where('member', array('member_id'=> $this->session->userdata['member_id']));
        $this->data['prof']     = $query->row();

        // show list output
        $this->db
        ->where(array('member_id'=> $this->session->userdata['member_id']))
        ->order_by('pub_id','desc');
        $query = $this->db->get('member_publikasi');
        $this->data['publication']  = $query->result();
        $this->data['pub']          = $query->row();

        $this->data['member_content']   = $this->load->view('publication/list', $this->data, true);
        $this->data['content']          = $this->load->view('member/dashboard', $this->data, true);

        // make an output
        $this->data['footer_gallery'] = $this->media->images_list(4, 6);
        $this->load->view('master/content', $this->data);

    }

    public function add()
    {

        // security check
        $this->is_logged();

        // get current data
        $this->db->select('avatar');
        $query = $this->db->get_where('member', array('member_id'=> $this->session->userdata['member_id']));
        $this->data['prof']     = $query->row();

        $this->data['pub']                  = new stdClass();
        $this->data['pub']->pub_id          = NULL;
        $this->data['pub']->pub_title       = NULL;
        $this->data['pub']->pub_partner     = NULL;
        $this->data['pub']->pub_type        = NULL;
        $this->data['pub']->pub_isbn        = NULL;
        $this->data['pub']->pub_year        = NULL;
        $this->data['pub']->pub_abstract    = NULL;
        $this->data['pub']->pub_file        = NULL;
        $this->data['pub']->pub_link        = NULL;
        $this->data['pub']->member_id       = $this->session->userdata('member_id');
        $this->data['max_size']             = ini_get('post_max_size');
        $filename                           = '';

        // for any submit
        if(isset($_POST['submit'])) {
            if(isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
                $config['upload_path']          = REPOSITORY;
                $config['allowed_types']        = 'gif|jpg|png|pdf|doc|docx|odt';
                $config['max_size']             = 10000;

                $this->upload->initialize($config);
                $this->upload->do_upload();
            }

            $record = array(
                'member_id'         => $this->session->userdata['member_id'],
                'pub_title'         => $this->input->post('prof_pub_title'),
                'pub_partner'       => $this->input->post('prof_pub_author'),
                'pub_type'          => $this->input->post('prof_pub_type'),
                'pub_isbn'          => $this->input->post('prof_pub_isbn'),
                'pub_year'          => $this->input->post('prof_pub_year'),
                'pub_abstract'      => $this->input->post('prof_pub_abstract'),
                'pub_file'          => $_FILES['userfile']['name'],
                'pub_link'          => $this->input->post('prof_pub_link'),
                'last_update'       => date('Y-m-d h:i:s')
            );

            $this->db->insert('member_publikasi', $record);
            redirect(site_url('member/publikasi'),'refresh');
        }


        // make an output
        $this->data['custom_error']     = '';
        $this->data['success']          = '';
        $this->data['member_content']   = $this->load->view('publication/add', $this->data, true);
        $this->data['content']          = $this->load->view('dashboard', $this->data, true);
        $this->data['footer_gallery'] = $this->media->images_list(4, 6);
        $this->load->view('master/content', $this->data);

    }

    public function edit($id)
    {
        // security check
        $this->is_logged();

        // get current data
        $this->db->select('avatar');
        $query = $this->db->get_where('member', array('member_id'=> $this->session->userdata['member_id']));
        $this->data['prof']     = $query->row();

        // show list output
        $this->db
        ->where('member_id', $this->session->userdata['member_id'])
        ->where('pub_id', $id);
        $query = $this->db->get('member_publikasi');
        $this->data['publication']    = $query->result();
        $this->data['pub']          = $query->row();
        $this->data['max_size']             = ini_get('post_max_size');

        // for any submit
        if(isset($_POST['submit'])) {
            if(isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
                $config['upload_path']          = REPOSITORY;
                $config['allowed_types']        = 'gif|jpg|png|pdf|doc|docx|odt';
                $config['max_size']             = 10000;
                $config['remove_spaces']        = FALSE;

                $this->upload->initialize($config);
                $this->upload->do_upload();
            }

            $record = array(
                'member_id'         => $this->session->userdata['member_id'],
                'pub_title'         => $this->input->post('prof_pub_title'),
                'pub_partner'       => $this->input->post('prof_pub_author'),
                'pub_type'          => $this->input->post('prof_pub_type'),
                'pub_isbn'          => $this->input->post('prof_pub_isbn'),
                'pub_year'          => $this->input->post('prof_pub_year'),
                'pub_abstract'      => $this->input->post('prof_pub_abstract'),
                'pub_file'          => $_FILES['userfile']['name'],
                'pub_link'          => $this->input->post('prof_pub_link'),
                'last_update'       => date('Y-m-d h:i:s')
            );

            $this->db
            ->where('member_id', $this->session->userdata['member_id'])
            ->where('pub_id', $this->input->post('pub_id'));
            $this->db->update('member_publikasi', $record);
            redirect(site_url('member/publikasi'),'refresh');
        }


        // make an output
        $this->data['custom_error']     = '';
        $this->data['success']          = '';
        $this->data['member_content']   = $this->load->view('publication/add', $this->data, true);
        $this->data['content']          = $this->load->view('dashboard', $this->data, true);
        $this->data['footer_gallery'] = $this->media->images_list(4, 6);
        $this->load->view('master/content', $this->data);

    }
    public function remove($id)
    {
        // remove file
        $this->db
        ->select('pub_file')
        ->from('member_publikasi')
        ->where('pub_id', $id);
        $query      = $this->db->get();
        $file       = $query->row();
        $filename   = $file->pub_file;

        $this->db
        ->where('member_id', $this->session->userdata['member_id'])
        ->where('pub_id', $id);
        unlink(REPOSITORY . '/'. $filename);
        $this->db->delete('member_publikasi');
        redirect(site_url('member/publikasi'),'refresh');
    }

    public function downld($id)
    {
        $this->db
        ->select('pub_file')
        ->from('member_publikasi')
        ->where('pub_id', $id);
        $query      = $this->db->get();
        $file       = $query->row();
        $filename   = $file->pub_file;
        if($filename != '') {
            force_download(REPOSITORY . '/'. $filename, NULL);
        } else {
            echo '<script>alert("Failed to download.")</script>';
        }
    }

    public function is_logged()
    {
        if(empty($this->session->userdata['email'])) {
            redirect(base_url('member/login'));
            exit;
        }
    }
}
