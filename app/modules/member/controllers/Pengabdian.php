<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengabdian extends MX_Controller {

    public $data            = array();
    private $url            = 'member/pengabdian';
    private $perpage        = 6;
    private $member_mail    = '';


    public function __construct()
    {
        parent::__construct();

        // load library and model
        $this->load->model(array('auth_model','CrudMod'));

        // set default value
        $this->data['title'] = 'Data Anggota';

        // set default language
        $this->media->set_language();

        // for debug purpose only
        $this->output->enable_profiler(false);

    }

    public function index($action = '', $id = '')
    {
        // security check
        $this->is_logged();

        // get current data
        $this->db->select('avatar');
        $query = $this->db->get_where('member', array('member_id'=> $this->session->userdata['member_id']));
        $this->data['prof']     = $query->row();

        // show list output
        $query = $this->db->get_where('member_abdi  ', array('member_id'=> $this->session->userdata['member_id']));
        $this->data['pengabdian']    = $query->result();
        $this->data['abdi']          = $query->row();

        $this->data['member_content']   = $this->load->view('pengabdian/list', $this->data, true);
        $this->data['content']          = $this->load->view('member/dashboard', $this->data, true);

        // make an output
        $this->data['footer_gallery'] = $this->media->images_list(4, 6);
        $this->load->view('master/content', $this->data);

    }

    public function add()
    {

        // security check
        $this->is_logged();

        // get current data
        $this->db->select('avatar');
        $query = $this->db->get_where('member', array('member_id'=> $this->session->userdata['member_id']));
        $this->data['prof']     = $query->row();

        $this->data['abdi']              = new stdClass();
        $this->data['abdi']->abdi_title     = NULL;
        $this->data['abdi']->abdi_partner     = NULL;
        $this->data['abdi']->abdi_year      = NULL;
        $this->data['abdi']->abdi_fund       = NULL;
        $this->data['abdi']->abdi_stake     = NULL;
        $this->data['abdi']->abdi_id      = NULL;
        $this->data['abdi']->member_id   = $this->session->userdata('member_id');

        // for any submit
        if(isset($_POST['submit'])) {
            $record = array(
                'abdi_title'        => $this->input->post('prof_abdi_title'),
                'abdi_partner'      => $this->input->post('prof_abdi_partner'),
                'abdi_year'         => $this->input->post('prof_abdi_year'),
                'abdi_stake'        => $this->input->post('prof_abdi_stake'),
                'member_id'         => $this->session->userdata['member_id']
            );

            $this->db->insert('member_abdi  ', $record);
            redirect(site_url('member/pengabdian'),'refresh');
        }


        // make an output
        $this->data['custom_error']     = '';
        $this->data['success']          = '';
        $this->data['member_content']   = $this->load->view('pengabdian/add', $this->data, true);
        $this->data['content']          = $this->load->view('dashboard', $this->data, true);
        $this->data['footer_gallery'] = $this->media->images_list(4, 6);
        $this->load->view('master/content', $this->data);

    }


    public function edit($id)
    {

        // security check
        $this->is_logged();

        // get current data
        $this->db->select('avatar');
        $query = $this->db->get_where('member', array('member_id'=> $this->session->userdata['member_id']));
        $this->data['prof']     = $query->row();

        // show list output
        $this->db
        ->where('member_id', $this->session->userdata['member_id'])
        ->where('abdi_id', $id);
        $query = $this->db->get('member_abdi    ');
        $this->data['education']    = $query->result();
        $this->data['abdi']          = $query->row();

        // for any submit
        if(isset($_POST['submit'])) {
            $record = array(
                'abdi_title'        => $this->input->post('prof_abdi_title'),
                'abdi_partner'      => $this->input->post('prof_abdi_partner'),
                'abdi_year'         => $this->input->post('prof_abdi_year'),
                'abdi_stake'        => $this->input->post('prof_abdi_stake'),
                'member_id'         => $this->session->userdata['member_id']
            );

            $this->db
            ->where('member_id', $this->session->userdata['member_id'])
            ->where('abdi_id', $this->input->post('abdi_id'));
            $this->db->update('member_abdi  ', $record);
            redirect(site_url('member/pengabdian'),'refresh');
        }


        // make an output
        $this->data['custom_error']     = '';
        $this->data['success']          = '';
        $this->data['member_content']   = $this->load->view('pengabdian/add', $this->data, true);
        $this->data['content']          = $this->load->view('dashboard', $this->data, true);
        $this->data['footer_gallery'] = $this->media->images_list(4, 6);
        $this->load->view('master/content', $this->data);

    }
    public function remove($id)
    {
        $this->db
        ->where('member_id', $this->session->userdata['member_id'])
        ->where('abdi_id', $id);
        $this->db->delete('member_abdi  ');
        redirect(site_url('member/pengabdian'),'refresh');
    }

    public function is_logged()
    {
        if(empty($this->session->userdata['email'])) {
            redirect(base_url('member/login'));
            exit;
        }
    }
}
