<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penelitian extends MX_Controller {

    public $data            = array();
    private $url            = 'member/penelitian';
    private $perpage        = 6;
    private $member_mail    = '';


    public function __construct()
    {
        parent::__construct();

        // load library and model
        $this->load->model(array('auth_model','CrudMod'));

        // set default value
        $this->data['title'] = 'Data Anggota';

        // set default language
        $this->media->set_language();

        // for debug purpose only
        $this->output->enable_profiler(false);

    }

    public function index($action = '', $id = '')
    {
        // security check
        $this->is_logged();

        // get current data
        $this->db->select('avatar');
        $query = $this->db->get_where('member', array('member_id'=> $this->session->userdata['member_id']));
        $this->data['prof']     = $query->row();

        // show list output
        $query = $this->db->get_where('member_riset', array('member_id'=> $this->session->userdata['member_id']));
        $this->data['riset']    = $query->result();
        $this->data['ris']          = $query->row();


            // validation rules
            $this->form_validation->set_rules('prof_nama', 'Nama Lengkap', 'required');

            // if break the rules
            if ($this->form_validation->run() == FALSE) {
                $this->data['member_content']   = $this->load->view('penelitian/list', $this->data, true);
                $this->data['content']          = $this->load->view('member/dashboard', $this->data, true);
            } else {
                // if not break the rules
                $record = array(
                    'member_id'     => $this->input->post('member_id'),
                    'realname'      => $this->input->post('prof_nama'),
                    'birth_city'    => $this->input->post('prof_birth_city'),
                    'birth_date'    => $this->input->post('prof_birth_date'),
                    'gender'        => $this->input->post('prof_gender'),
                    'home_addr'     => $this->input->post('prof_home_addr'),
                    'home_city'     => $this->input->post('prof_home_city'),
                    'home_prov'     => $this->input->post('prof_home_prov'),
                    'phone'         => $this->input->post('prof_phone'),
                    'ahli_bid'      => $this->input->post('prof_ahli'),
                    'website'       => $this->input->post('prof_web'),
                    'socmed_fb'     => $this->input->post('prof_fb'),
                    'socmed_tw'     => $this->input->post('prof_tw'),
                    'last_update'   => date('Y-m-d H:i:s')
                );

                // start transaction
                $this->db->where('member_id', $this->input->post('member_id'));
                $update  = $this->db->update('member', $record);

                // check for status
                if ($update) {
                    $this->data['message']          = 'Your record has been updated.';
                    $this->data['button_link']      = 'profile';
                    $this->data['content']          = $this->load->view('master/redirect', $this->data, TRUE);
                } else {
                    $this->data['member_content']   = $this->load->view('pendidkan/list', $this->data, true);
                    $this->data['content']          = $this->load->view('member/dasboard', $this->data, true);
                }
            }

        // make an output
        $this->data['footer_gallery'] = $this->media->images_list(4, 6);
        $this->load->view('master/content', $this->data);

    }

    public function add()
    {

        // security check
        $this->is_logged();

        // get current data
        $this->db->select('avatar');
        $query = $this->db->get_where('member', array('member_id'=> $this->session->userdata['member_id']));
        $this->data['prof']     = $query->row();

        $this->data['ris']              = new stdClass();
        $this->data['ris']->riset_title     = NULL;
        $this->data['ris']->riset_partner     = NULL;
        $this->data['ris']->riset_year      = NULL;
        $this->data['ris']->riset_fund       = NULL;
        $this->data['ris']->riset_stake     = NULL;
        $this->data['ris']->riset_id      = NULL;
        $this->data['ris']->member_id   = $this->session->userdata('member_id');

        // for any submit
        if(isset($_POST['submit'])) {
            $record = array(
                'riset_title'       => $this->input->post('prof_riset_title'),
                'riset_partner'     => $this->input->post('prof_riset_part'),
                'riset_year'        => $this->input->post('prof_riset_year'),
                'riset_fund'        => $this->input->post('prof_riset_fund'),
                'riset_stake'       => $this->input->post('prof_riset_stake'),
                'member_id'         => $this->session->userdata['member_id']
            );

            $this->db->insert('member_riset', $record);
            redirect(site_url('member/penelitian'),'refresh');
        }


        // make an output
        $this->data['custom_error']     = '';
        $this->data['success']          = '';
        $this->data['member_content']   = $this->load->view('penelitian/add', $this->data, true);
        $this->data['content']          = $this->load->view('dashboard', $this->data, true);
        $this->data['footer_gallery'] = $this->media->images_list(4, 6);
        $this->load->view('master/content', $this->data);

    }


    public function edit($id)
    {

        // security check
        $this->is_logged();

        // get current data
        $this->db->select('avatar');
        $query = $this->db->get_where('member', array('member_id'=> $this->session->userdata['member_id']));
        $this->data['prof']     = $query->row();

        // show list output
        $this->db
        ->where('member_id', $this->session->userdata['member_id'])
        ->where('riset_id', $id);
        $query = $this->db->get('member_riset');
        $this->data['education']    = $query->result();
        $this->data['ris']          = $query->row();

        // for any submit
        if(isset($_POST['submit'])) {
            $record = array(
                'riset_title'       => $this->input->post('prof_riset_title'),
                'riset_partner'     => $this->input->post('prof_riset_part'),
                'riset_year'        => $this->input->post('prof_riset_year'),
                'riset_fund'        => $this->input->post('prof_riset_fund'),
                'riset_stake'       => $this->input->post('prof_riset_stake'),
                'member_id'         => $this->session->userdata['member_id']
            );

            $this->db
            ->where('member_id', $this->session->userdata['member_id'])
            ->where('riset_id', $this->input->post('riset_id'));
            $this->db->update('member_riset', $record);
            redirect(site_url('member/penelitian'),'refresh');
        }


        // make an output
        $this->data['custom_error']     = '';
        $this->data['success']          = '';
        $this->data['member_content']   = $this->load->view('penelitian/add', $this->data, true);
        $this->data['content']          = $this->load->view('dashboard', $this->data, true);
        $this->data['footer_gallery'] = $this->media->images_list(4, 6);
        $this->load->view('master/content', $this->data);

    }
    public function remove($id)
    {
        $this->db
        ->where('member_id', $this->session->userdata['member_id'])
        ->where('riset_id', $id);
        $this->db->delete('member_riset');
        redirect(site_url('member/penelitian'),'refresh');
    }

    public function is_logged()
    {
        if(empty($this->session->userdata['email'])) {
            redirect(base_url('member/login'));
            exit;
        }
    }
}
