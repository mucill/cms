<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends MX_Controller {

    public $data                = array();
    private $url                = 'member';
    private $perpage            = 15;
    private $member_mail        = '';
    private $propinsi           = array();
    private $kabupaten          = array();
    private $prefix_member_code = 'APT';

    public function __construct()
    {
        parent::__construct();

        // load model
        $this->load->model(array('auth_model','CrudMod'));
        $this->load->library('upload');

        // set default language
        $this->media->set_language();

        // set default value
        $this->data['title']    = __('membership', false);

        // get propinsi
        $query  = $this->db->get('cms_provinces');
        $prov   = $query->result();
        foreach ($prov as $provinces) {
            $this->data['propinsi'][$provinces->id] = $provinces->name;
        }

        // get kabupaten
        $this->db->where('province_id', 11);
        $query      = $this->db->get('cms_regencies');
        $kabupaten  = $query->result();
        foreach ($kabupaten as $kab) {
            $this->data['kab'][$kab->id] = str_replace('KABUPATEN','KAB.',$kab->name);
        }

        // for debug purpose only
        $this->output->enable_profiler(false);

    }

    public function index($offset = 0)
    {
        // pagination
        $this->db
        ->select('member_id')
        ->from('member')
        ->order_by('realname');

        if(isset($_POST['q']) && ($_POST['q'] != '')) {
            $this->db
            ->like('realname', $_POST['q'])
            ->or_like('ahli_bid', $_POST['q']);
        }

        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->from('member')
        ->limit($this->perpage, $offset )
        ->order_by('realname');

        if(isset($_POST['q']) && ($_POST['q'] != '')) {
            $this->db
            ->like('realname', $_POST['q'])
            ->or_like('ahli_bid', $_POST['q']);
        }

        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {

            // most view
            $this->data['view_most']        = $this->media->blog_view_most();
            $this->data['view_latest']      = $this->media->blog_view_latest();

            // galeri
            $this->data['gallery']          = $this->media->images_list(4, 5); // $cat_id, $limit
            $this->data['footer_gallery']   = $this->media->images_list(4, 6); // $cat_id, $limit

            // generate output
            $this->data['category']     = $this->media->category_list('blogs');
            $this->data['member']           = $query->result();
            $this->data['content']          = $this->load->view('list', $this->data, true);

        } else {

            // if no data
            $this->data['footer_gallery']   = $this->media->images_list(4, 6);
            $this->data['message']          = __('blank', false);
            $this->data['content']          = $this->load->view('master/blank', $this->data, true);

        }


        // Generate output
        $this->load->view('master/content', $this->data);
    }

    // Add a new item
    public function register()
    {
        //Validation config
        $config = array(
            array(
                'field'     => 'reg_name',
                'label'     => 'Nama Lengkap',
                'rules'     => 'required',
                'errors'    => array(
                    'required'      => 'Mohon isikan dengan nama lengkap anda.'
                ),
            ),
            array(
                'field'     => 'reg_premail',
                'label'     => 'Email',
                'rules'     => 'required|valid_email',
                'errors'    => array(
                    'required'      => 'Mohon isikan alamat email anda.',
                    'valid_email'   => 'Format email belum benar.'
                ),
            ),
            array(
                'field'     => 'reg_mail',
                'label'     => 'Konfirmasi Email',
                'rules'     => 'required|valid_email|matches[reg_premail]|is_unique[member.email]',
                'errors'    => array(
                    'required'      => 'Mohon isikan alamat email anda.',
                    'valid_email'   => 'Format email belum benar.',
                    'matches'       => '<strong>Konfirmasi Email</strong> berbeda dengan alamat <strong>Email</strong>.',
                    'is_unique'     => '<strong style="color:red">Alamat email anda sudah pernah terdaftar.</strong>'
                ),
            )
        );

        $this->form_validation->set_rules($config);
        $data['custom_error']   = '';
        $view['title']              = 'Pendaftaran';

            if ($this->form_validation->run() == FALSE)
            {
                // If Failed
                $view['title']          = 'Pendaftaran';
                $data['custom_error']   = validation_errors('<li><i class="icon-line-square-check"></i> ', '</li>');
                $view['content']        = $this->load->view('register', '', TRUE);

            } else {

                // If Success
                $r_name                 = $this->input->post('reg_name');
                $r_mail                 = $this->input->post('reg_mail');

                $clean                  = $this->security->xss_clean($this->input->post(NULL, TRUE));
                $id                     = $this->auth_model->insertMember($clean);
                $token                  = $this->auth_model->insertToken($id);

                if($id) {

                    // $config = array(
                    //   'protocol'    => 'smtp',
                    //   'smtp_host'   => 'mailtrap.io',
                    //   'smtp_port'   => 2525,
                    //   'smtp_user'   => '31a7f7f4e5c2ce',
                    //   'smtp_pass'   => 'b792162450272e',
                    //   'mailtype'    => 'html',
                    //   'crlf'        => "\r\n",
                    //   'newline'     => "\r\n"
                    // );

                    $config = array(
                      'protocol'    => 'smtp',
                      'smtp_host'   => 'ssl://mail.aptipi.or.id',
                      'smtp_port'   => 465,
                      'smtp_user'   => 'noreply@aptipi.or.id',
                      'smtp_pass'   => '1q2w3e4r',
                      'crlf'        => "\r\n",
                      'newline'     => "\r\n",
                      'mailtype'    => "html"
                    );

                    $this->load->library('email', $config);

                    $qstring    = base64_encode($token);
                    $url        = site_url() . '/member/complete/token/' . $qstring;
                    $link       = '<a href="' . $url . '">' . $url . '</a>';

                    $subject    = 'Konfirmasi Pendaftaran';
                    $message    = '';
                    $message   .= "Hai  ".$r_name.",<br /><br />Silahkan klik pada tautan berikut untuk proses verifikasi data anda.<br /> " .$link;
                    $message   .= "<br><br><br>Hormat kami,<br>Pengurus APTIPI";
                    $this->email->from('noreply@aptipi.or.id', 'APTIPI Indonesia');
                    $this->email->to($r_mail);
                    $this->email->subject($subject);
                    $this->email->message($message);

                    if($this->email->send()) {
                        $view['message']            = '<p>Kami telah mengirimkan email konfirmasi ke alamat email anda.</p>';
                    } else {
                        $view['message']            = '<p>Gagal mengirimkan email konfirmasi ke alamat email anda. Silahkan mencoba kembali</p>';
                    }

                    $view['footer_gallery']     = $this->media->images_list(4, 6);
                    $view['title']              = 'Konfirmasi Pendaftaran';
                    $view['content']            = $this->load->view('submit_success', $view, TRUE);
                }
        }
        $view['footer_gallery'] = $this->media->images_list(4, 6);
        $this->load->view('master/content', $view);
    }

    public function complete()
    {
        $token      = base64_decode($this->uri->segment(4));
        $cleanToken = $this->security->xss_clean($token);
        $user_info  = $this->auth_model->isTokenValid($cleanToken);
        if($user_info) {
            if($user_info->status == 'approved'){
                $data['custom_error']   = '<li><i class="icon-line-square-check"></i> Token is invalid or expired</li>';
                $view['title']          = 'Token Kadaluarsa';
                $view['message']        = 'Akun anda mungkin telah diaktifkan atau token telah kadaluarsa.';
                $view['content']        = $this->load->view('master/blank', $view, true);
            } else {
                $data = array(
                    'realName'  => $user_info->realname,
                    'email'     => $user_info->email,
                    'member_id' => $user_info->member_id,
                    'token'     => base64_encode($token)
                );

                //Validation config
                $config = array(
                    array(
                        'field'     => 'reg_pass',
                        'label'     => 'Password',
                        'rules'     => 'required|min_length[6]',
                        'errors'    => array(
                            'required'      => 'Mohon isikan password anda.',
                            'min_length'    => 'Panjang password minimal 6 karakter.'
                        ),
                    ),
                    array(
                        'field'     => 'reg_passed',
                        'label'     => 'Konfirmasi Password',
                        'rules'     => 'required|min_length[6]|matches[reg_pass]',
                        'errors'    => array(
                            'required'      => 'Mohon isikan password anda.',
                            'min_length'    => 'Panjang password minimal 6 karakter.',
                            'matches'       => '<strong>Konfirmasi Password</strong> harus sama dengan Password.'
                        ),
                    )
                );

                $this->form_validation->set_rules($config);
                $data['custom_error']   = '';

                if ($this->form_validation->run() == FALSE) {
                    $data['custom_error']   = validation_errors('<li><i class="icon-line-square-check"></i> ', '</li>');
                    $view['title']          = 'Konfirmasi Pendaftaran';
                    $view['content']        = $this->load->view('complete', $data, TRUE);
                } else {

                    $this->load->library('password');

                    $post                   = $this->input->post(NULL, TRUE);
                    $cleanPost              = $this->security->xss_clean($post);
                    $hashed                 = $this->password->create_hash($cleanPost['reg_pass']);
                    $cleanPost['reg_pass']  = $hashed;
                    unset($cleanPost['reg_passed']);

                    $userInfo = $this->auth_model->updateUserInfo($cleanPost);
                    if(!$userInfo){
                        $data['custom_error'] = '<li><i class="icon-line-square-check"></i> Terjadi kesalahan. Silahkan ulangi beberapa saat lagi.</li>';
                    }
                    unset($userInfo->password);
                    // foreach($userInfo as $key=>$val){
                    //     $this->session->set_userdata($key, $val);
                    // }
                    // redirect(base_url().'member/login');

                    // read custom message from Widget module
                    $content            = '';
                    $this->db
                    ->where('slug', 'widget-2') // widget-2 is a alias name - taken from table cms_widget for successfully registration message
                    ->limit(1);
                    $query = $this->db->get('cms_widget');
                    if($query->num_rows() > 0 ) {
                        $widget = $query->row();
                        $content = $widget->content;
                    }
                    $view['message']    = $content;
                    $view['content']    = $this->load->view('submit_success', $view, true);
                    $view['title']      = 'Akun Telah Aktif';
                }
            }
        } else {
            $data['custom_error']   = '<li><i class="icon-line-square-check"></i> Token is invalid or expired</li>';
            $view['title']          = 'Token Kadaluarsa';
            $view['message']        = 'Akun anda mungkin telah diaktifkan atau token telah kadaluarsa.';
            $view['content']        = $this->load->view('master/blank', $view, true);
        }
        $view['footer_gallery']     = $this->media->images_list(4, 6);
        $this->load->view('master/content', $view);
    }

    public function login()
    {
        //Validation config
        $config = array(
            array(
                'field'     => 'reg_mail',
                'label'     => 'Email',
                'rules'     => 'required|valid_email',
                'errors'    => array(
                    'required'      => 'Mohon isikan alamat email anda.',
                    'valid_email'   => 'Format email belum benar.'
                ),
            ),
            array(
                'field'     => 'reg_pass',
                'label'     => 'Password',
                'rules'     => 'required',
                'errors'    => array(
                    'required'      => 'Mohon isikan password anda.'
                ),
            )
        );

        $this->form_validation->set_rules($config);
        $data['custom_error']   = '';

        if($this->form_validation->run() == FALSE) {
            $data['custom_error']   = validation_errors('<li><i class="icon-line-square-check"></i> ', '</li>');
        } else {
            // debug($this->input->post());
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);
            $userInfo = $this->auth_model->checkLogin($clean);
            if(!$userInfo){
                $data['custom_error']   = '<li><i class="icon-line-square-check"></i>Upaya masuk anda gagal, silahkan ulangi lagi.</li>';
            } else {
                foreach($userInfo as $key=>$val){
                    $this->session->set_userdata($key, $val);
                }
                redirect(site_url().'/member/profile');
                exit;
            }
        }

        $view['content']        = $this->load->view('login', $data, TRUE);
        $view['footer_gallery'] = $this->media->images_list(4, 6);
        $view['title']           = 'Halaman Login';
        $this->load->view('master/content', $view);
    }

    public function logout()
    {
        $this->load->library('session');
        $this->session->sess_destroy();
        redirect(site_url('member/login'));
    }

    public function profile()
    {
        // security check
        $this->is_logged();

        // get current data
        $query = $this->db->get_where('member', array('member_id'=> $this->session->userdata['member_id']));
        $this->data['prof']     = $query->row();

        // get kabupaten
        if($this->data['prof']->home_city != '') {
            $this->db->where('province_id', $this->data['prof']->home_prov);
        }

        $query      = $this->db->get('cms_regencies');
        $kabupaten  = $query->result();
        foreach ($kabupaten as $kab) {
            $this->kabupaten[$kab->id] = str_replace('KABUPATEN','KAB.',$kab->name);
        }

        $this->data['kab'] = $this->kabupaten;

        // validation rules
        $this->form_validation->set_rules('prof_nama', 'Nama Lengkap', 'required');

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['member_content']   = $this->load->view('biodata', $this->data, true);
            $this->data['content']          = $this->load->view('dashboard', $this->data, true);
        } else {
            // if not break the rules
            if(isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
                // remove id current file is exist
                if(file_exists(PERSON . DIRECTORY_SEPARATOR . $this->data['prof']->avatar)) {
                    @unlink(PERSON . DIRECTORY_SEPARATOR . $this->data['prof']->avatar);
                }

                $path_parts                     = pathinfo($_FILES["userfile"]["name"]);
                $extension                      = $path_parts['extension'];
                $config['upload_path']          = PERSON;
                $config['allowed_types']        = 'gif|jpg|png';
                $config['file_name']            = md5($_FILES['userfile']['name']).'.'.$extension;

                $this->upload->initialize($config);
                $this->upload->do_upload('userfile');

                $metafile                       = $this->upload->data();
                $file_name                      = md5($_FILES["userfile"]["name"]).'.'.$extension;
                $file_type                      = $metafile['file_type'];
                $file_size                      = $metafile['file_size'];
            }


            $record = array(
                'member_id'     => $this->input->post('member_id'),
                'realname'      => $this->input->post('prof_nama'),
                'birth_city'    => $this->input->post('prof_birth_city'),
                'birth_date'    => $this->input->post('prof_birth_date'),
                'gender'        => $this->input->post('prof_gender'),
                'home_addr'     => $this->input->post('prof_home_addr'),
                'home_city'     => $this->input->post('prof_home_city'),
                'home_prov'     => $this->input->post('prof_home_prov'),
                'phone'         => $this->input->post('prof_phone'),
                'ahli_bid'      => $this->input->post('prof_ahli'),
                'avatar'        => $file_name,
                'website'       => $this->input->post('prof_web'),
                'socmed_fb'     => $this->input->post('prof_fb'),
                'socmed_tw'     => $this->input->post('prof_tw'),
                'last_update'   => date('Y-m-d H:i:s')
            );

            // start transaction
            $this->db->where('member_id', $this->input->post('member_id'));
            $update  = $this->db->update('member', $record);

            if($this->data['prof']->member_code == '') {
                $record = array(
                    'member_code'   => $this->prefix_member_code . '-' . $this->input->post('prof_home_city') .'-'.$this->input->post('member_id')
                );
                $this->db->where('member_id', $this->input->post('member_id'));
                $update  = $this->db->update('member', $record);
            }

            // check for status
            if ($update) {
                $this->data['message']          = 'Your record has been updated.';
                $this->data['button_link']      = 'profile';
                $this->data['content']          = $this->load->view('master/redirect', $this->data, TRUE);
            } else {
                $this->data['member_content']   = $this->load->view('biodata', $this->data, true);
                $this->data['content']          = $this->load->view('dasboard', $this->data, true);
            }
        }

        // make an output
        $this->data['footer_gallery'] = $this->media->images_list(4, 6);
        $this->load->view('master/content', $this->data);

    }

    public function instansi()
    {
        // security check
        $this->is_logged();

        // get current data
        $query = $this->db->get_where('member', array('member_id'=> $this->session->userdata['member_id']));
        $this->data['prof']     = $query->row();

        // show list output
        $this->db
        ->where('member_id', $this->session->userdata['member_id']);
        $query = $this->db->get('member_office');
        if($query->num_rows() > 0) {
            $this->data['inst']                 = $query->row();
        } else {
            $this->data['inst']                 = new stdClass();
            $this->data['inst']->office_name    = NULL;
            $this->data['inst']->office_addr    = NULL;
            $this->data['inst']->office_city    = NULL;
            $this->data['inst']->office_prov    = NULL;
        }

        // validation rules
        $this->form_validation->set_rules('prof_offc_name', 'Nama Instansi', 'required');

        // if break the rules
        if ($this->form_validation->run() == FALSE) {
            $this->data['member_content']   = $this->load->view('instansi', $this->data, true);
            $this->data['content']          = $this->load->view('dashboard', $this->data, true);
        } else {
            // if not break the rules
            $record = array(
                'member_id'     => $this->input->post('member_id'),
                'office_name'      => $this->input->post('prof_offc_name'),
                'office_addr'    => $this->input->post('prof_offc_addr'),
                'office_city'    => $this->input->post('prof_offc_city'),
                'office_prov'    => $this->input->post('prof_offc_prov'),
                'last_update'   => date('Y-m-d H:i:s')
            );
            // start transaction
            if($query->num_rows() > 0 ) {
                $this->db->where('member_id', $this->input->post('member_id'));
                $update  = $this->db->update('member_office', $record);
            } else {
                // debug($record);
                $update  = $this->db->insert('member_office', $record);
            }

            // check for status
            if ($update) {
                $this->data['message']          = 'Your record has been updated.';
                $this->data['button_link']      = 'instansi';
                $this->data['content']          = $this->load->view('master/redirect', $this->data, TRUE);
            } else {
                $this->data['member_content']   = $this->load->view('instansi', $this->data, true);
                $this->data['content']          = $this->load->view('dasboard', $this->data, true);
            }
        }

        // make an output
        $this->data['footer_gallery'] = $this->media->images_list(4, 6);
        $this->load->view('master/content', $this->data);

    }

    public function is_logged()
    {
        if(empty($this->session->userdata['email'])) {
            redirect(base_url('member/login'));
            exit;
        }
    }

    // Add a new item
    public function forgot()
    {
        $this->load->helper('string');
        $this->form_validation->set_rules('reg_premail', 'Email', 'required');
        $data['custom_error']   = '';
        $view['title']          = 'Pendaftaran';

            if ($this->form_validation->run() == FALSE) {

                // If Failed
                $view['title']          = 'Galat';
                $data['custom_error']   = validation_errors('<li><i class="icon-line-square-check"></i> ', '</li>');
                $view['content']        = $this->load->view('forgot', '', TRUE);

            } else {

                // If Success
                $r_mail = $this->input->post('reg_premail');
                $query  = $this->db->get_where('cms_member', array('email' => $r_mail));

                if($query->num_rows() > 0 ) {
                    $user       = $query->row();
                    $member_id  = $user->member_id;
                    // $config = Array(
                    //     'protocol' => 'smtp',
                    //     'smtp_host' => 'mailtrap.io',
                    //     'smtp_port' => 2525,
                    //     'smtp_user' => '31a7f7f4e5c2ce',
                    //     'smtp_pass' => 'b792162450272e',
                    //     'mailtype'    => 'html',
                    //     'crlf' => "\r\n",
                    //     'newline' => "\r\n"
                    // );

                    $config = array(
                      'protocol'    => 'smtp',
                      'smtp_host'   => 'ssl://mail.aptipi.or.id',
                      'smtp_port'   => 465,
                      'smtp_user'   => 'noreply@aptipi.or.id',
                      'smtp_pass'   => '1q2w3e4r',
                      'crlf'        => "\r\n",
                      'newline'     => "\r\n",
                      'mailtype'    => "html"
                    );

                    $this->load->library('email', $config);
                    $this->load->library('password');
                    $new_pass   = strtoupper(random_string('alpha',3)) . random_string('numeric',3);
                    $subject    = 'Lupa Password';
                    $message    = '';
                    $message   .= "Hai, <br /><br />Berikut ini adalah Password anda yang baru : <br /><br /> ";
                    $message   .= "Email : <strong>". $r_mail . "</strong><br>";
                    $message   .= "Password : <strong>" . $new_pass . "</strong>";
                    $message   .= "<br /><br />Silahkan gunakan pada saat login dan ubahlah dengan segera demi keamanan data anda.";
                    $message   .= "<br><br><br>Hormat kami,<br>Pengurus APTIPI Indonesia";

                    $this->email->clear();
                    $this->email->from('conigter@gmail.com', 'APTIPI Indonesia');
                    $this->email->to($r_mail);
                    $this->email->subject($subject);
                    $this->email->message($message);

                    if($this->email->send()) {

                        $update = array(
                            'reg_pass'      => $this->password->create_hash($new_pass),
                            'member_id'     => $member_id
                        );
                        $userInfo           = $this->auth_model->updateUserInfo($update);
                        $view['title']      = 'Konfirmasi Email';
                        $view['message']    = '<p>Kami telah mengirimkan email konfirmasi ke alamat email anda.</p>';
                    } else {
                        $view['title']      = 'Galat';
                        $view['message']    = '<p>Tidak dapat mengirimkan email. Mohon periksa koneksi internet anda.</p>';
                    }

                } else {
                    $view['title']      = 'Galat';
                    $view['message']    = '<p>Akun tidak ditemukan. <a href="forgot">Silahkan dicoba kembali.</a> </p>';
                }
                $view['content']        = $this->load->view('submit_success', $view, TRUE);
            }
        $view['footer_gallery'] = $this->media->images_list(4, 6);
        $this->load->view('master/content', $view);
    }

    // get propinsi
    public function district($province = null)
    {
        $this->db
        ->where('province_id', $province)
        ->order_by('name') ;
        $query  = $this->db->get('cms_regencies');
        $reg   = $query->result();
        foreach ($reg as $regional) {
            $this->kabupaten['kab'][$regional->id] = str_replace('KABUPATEN ','KAB. ',$regional->name);
        }
        header('Content-Type: application/json');
        echo json_encode($this->kabupaten, JSON_PRETTY_PRINT);
    }

    public function detail($id = null)
    {

        // get all data
        $this->db
        ->from('member')
        ->where('member_id', $id);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {

            // most view
            $this->data['view_most']        = $this->media->blog_view_most();
            $this->data['view_latest']      = $this->media->blog_view_latest();

            // galeri
            $this->data['gallery']          = $this->media->images_list(4, 5); // $cat_id, $limit
            $this->data['footer_gallery']   = $this->media->images_list(4, 6); // $cat_id, $limit

            // generate output
            $this->data['form']             = $query->row();
            $this->data['content']          = $this->load->view('summary', $this->data, true);

        } else {

            // if no data
            $this->data['footer_gallery']   = $this->media->images_list(4, 6);
            $this->data['message']          = __('blank', false);
            $this->data['content']          = $this->load->view('master/blank', $this->data, true);

        }
        // Generate output
        $this->load->view('master/content', $this->data);
    }

    public function remove_photo($id = null)
    {
        $this->db
        ->select('avatar')
        ->from('member')
        ->where('member_id', $id)
        ->limit(1);
        $query = $this->db->get();
        if($query->num_rows() > 0 ) {
            $form = $query->row();
            if(file_exists(PERSON . DIRECTORY_SEPARATOR . $form->avatar)) {
                @unlink(PERSON . DIRECTORY_SEPARATOR . $form->avatar);
                $record = array('avatar' => '');
                $this->db->where('member_id', $id);
                $this->db->update('member', $record);
            }
        }

        $this->data['message']          = 'Photo has been removed.';
        $this->data['button_link']      = site_url('member/profile');
        $this->data['content']          = $this->load->view('master/redirect', $this->data, true);

        $view['footer_gallery']         = $this->media->images_list(4, 6);
        $this->load->view('master/content', $this->data);

    }

}
