<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan extends MX_Controller {

    public $data            = array();
    private $url            = 'member/pendidikan';
    private $perpage        = 6;
    private $member_mail    = '';


    public function __construct()
    {
        parent::__construct();

        // load library and model
        $this->load->model(array('auth_model','CrudMod'));

        // set default language
        $this->media->set_language();

        // set default value
        $this->data['title']    = 'Data Anggota';
        $this->data['propinsi']       = array(
                                    '11' => 'Aceh',
                                    '12' => 'Sumatera Utara',
                                    '13' => 'Sumatera Barat',
                                    '14' => 'Riau',
                                    '15' => 'Jambi',
                                    '16' => 'Sumatera Selatan',
                                    '17' => 'Bengkulu',
                                    '18' => 'Lampung',
                                    '19' => 'Kepulauan Bangka Belitung',
                                    '21' => 'Kepulauan Riau',
                                    '31' => 'Dki Jakarta',
                                    '32' => 'Jawa Barat',
                                    '33' => 'Jawa Tengah',
                                    '34' => 'Di Yogyakarta',
                                    '35' => 'Jawa Timur',
                                    '36' => 'Banten',
                                    '51' => 'Bali',
                                    '52' => 'Nusa Tenggara Barat',
                                    '53' => 'Nusa Tenggara Timur',
                                    '61' => 'Kalimantan Barat',
                                    '62' => 'Kalimantan Tengah',
                                    '63' => 'Kalimantan Selatan',
                                    '64' => 'Kalimantan Timur',
                                    '65' => 'Kalimantan Utara',
                                    '71' => 'Sulawesi Utara',
                                    '72' => 'Sulawesi Tengah',
                                    '73' => 'Sulawesi Selatan',
                                    '74' => 'Sulawesi Tenggara',
                                    '75' => 'Gorontalo',
                                    '76' => 'Sulawesi Barat',
                                    '81' => 'Maluku',
                                    '82' => 'Maluku Utara',
                                    '91' => 'Papua Barat',
                                    '94' => 'Papua');

        // for debug purpose only
        $this->output->enable_profiler(false);

    }

    public function index($action = '', $id = '')
    {
        // security check
        $this->is_logged();

        // get current data
        $this->db
        ->select('avatar');

        $query = $this->db->get_where('member', array('member_id'=> $this->session->userdata['member_id']));
        $this->data['prof']     = $query->row();

        // show list output
        $this->db
        ->where(array('member_id'=> $this->session->userdata['member_id']))
        ->order_by('jabatan_id','desc');
        $query = $this->db->get('member_jabatan');
        $this->data['jabatan']    = $query->result();
        $this->data['jab']          = $query->row();


            // validation rules
            $this->form_validation->set_rules('prof_nama', 'Nama Lengkap', 'required');

            // if break the rules
            if ($this->form_validation->run() == FALSE) {
                $this->data['member_content']   = $this->load->view('jabatan/list', $this->data, true);
                $this->data['content']          = $this->load->view('member/dashboard', $this->data, true);
            } else {
                // if not break the rules
                $record = array(
                    'member_id'     => $this->input->post('member_id'),
                    'realname'      => $this->input->post('prof_nama'),
                    'birth_city'    => $this->input->post('prof_birth_city'),
                    'birth_date'    => $this->input->post('prof_birth_date'),
                    'gender'        => $this->input->post('prof_gender'),
                    'home_addr'     => $this->input->post('prof_home_addr'),
                    'home_city'     => $this->input->post('prof_home_city'),
                    'home_prov'     => $this->input->post('prof_home_prov'),
                    'phone'         => $this->input->post('prof_phone'),
                    'ahli_bid'      => $this->input->post('prof_ahli'),
                    'website'       => $this->input->post('prof_web'),
                    'socmed_fb'     => $this->input->post('prof_fb'),
                    'socmed_tw'     => $this->input->post('prof_tw'),
                    'last_update'   => date('Y-m-d H:i:s')
                );

                // start transaction
                $this->db->where('member_id', $this->input->post('member_id'));
                $update  = $this->db->update('member', $record);

                // check for status
                if ($update) {
                    $this->data['message']          = 'Your record has been updated.';
                    $this->data['button_link']      = 'profile';
                    $this->data['content']          = $this->load->view('master/redirect', $this->data, TRUE);
                } else {
                    $this->data['member_content']   = $this->load->view('jabatan/list', $this->data, true);
                    $this->data['content']          = $this->load->view('member/dasboard', $this->data, true);
                }
            }

        // make an output
        $this->data['footer_gallery'] = $this->media->images_list(4, 6);
        $this->load->view('master/content', $this->data);

    }

    public function add()
    {

        // security check
        $this->is_logged();

        // get current data
        $this->db->select('avatar');
        $query = $this->db->get_where('member', array('member_id'=> $this->session->userdata['member_id']));
        $this->data['prof']     = $query->row();

        $this->data['jab']              = new stdClass();
        $this->data['jab']->jabatan_id  = NULL;
        $this->data['jab']->jbt_type    = NULL;
        $this->data['jab']->jbt_office  = NULL;
        $this->data['jab']->jbt_periode = NULL;
        $this->data['jab']->member_id   = $this->session->userdata('member_id');

        // for any submit
        if(isset($_POST['submit'])) {
            $record = array(
                'member_id'     => $this->session->userdata['member_id'],
                'jbt_type'      => $this->input->post('prof_jab_nama'),
                'jbt_office'    => $this->input->post('prof_jab_office'),
                'jbt_periode'   => $this->input->post('prof_jab_periode'),
                'last_update'   => date('Y-m-d h:i:s')
            );

            $this->db->insert('member_jabatan', $record);
            redirect(site_url('member/jabatan'),'refresh');
        }


        // make an output
        $this->data['custom_error']     = '';
        $this->data['success']          = '';
        $this->data['member_content']   = $this->load->view('jabatan/add', $this->data, true);
        $this->data['content']          = $this->load->view('dashboard', $this->data, true);
        $this->data['footer_gallery'] = $this->media->images_list(4, 6);
        $this->load->view('master/content', $this->data);

    }


    public function edit($id)
    {

        // security check
        $this->is_logged();

        // get current data
        $this->db->select('avatar');
        $query = $this->db->get_where('member', array('member_id'=> $this->session->userdata['member_id']));
        $this->data['prof']     = $query->row();

        // show list output
        $this->db
        ->where('member_id', $this->session->userdata['member_id'])
        ->where('jabatan_id', $id);
        $query = $this->db->get('member_jabatan');
        $this->data['jabatan']    = $query->result();
        $this->data['jab']          = $query->row();

        // for any submit
        if(isset($_POST['submit'])) {
            $record = array(
                'jbt_type'      => $this->input->post('prof_jab_nama'),
                'jbt_office'    => $this->input->post('prof_jab_office'),
                'jbt_periode'   => $this->input->post('prof_jab_periode')
            );

            $this->db
            ->where('member_id', $this->session->userdata['member_id'])
            ->where('jabatan_id', $this->input->post('jabatan_id'));
            $this->db->update('member_jabatan', $record);
            redirect(site_url('member/jabatan'),'refresh');
        }


        // make an output
        $this->data['custom_error']     = '';
        $this->data['success']          = '';
        $this->data['member_content']   = $this->load->view('jabatan/add', $this->data, true);
        $this->data['content']          = $this->load->view('dashboard', $this->data, true);
        $this->data['footer_gallery'] = $this->media->images_list(4, 6);
        $this->load->view('master/content', $this->data);

    }
    public function remove($id)
    {
        $this->db
        ->where('member_id', $this->session->userdata['member_id'])
        ->where('jabatan_id', $id);
        $this->db->delete('member_jabatan');
        redirect(site_url('member/jabatan'),'refresh');
    }

    public function is_logged()
    {
        if(empty($this->session->userdata['email'])) {
            redirect(base_url('member/login'));
            exit;
        }
    }
}
