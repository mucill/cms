		<div class="panel panel-default noshadow">
			<div class="panel-body">
				<?php echo form_open_multipart(current_url(), 'class="form-horizontal nobottommargin" id="biodata-form" name="biodata-form"') ?>
				<?php if($custom_error == '') : ?>
				<h4 style="text-transform: uppercase">Publikasi Ilmiah</h4>
				Mohon lengkapi formulir publikasi ilmiah berikut ini.
				<hr />
				<?php else: ?>
					<?php $this->load->view('submit_error') ?>
				<?php endif; ?>
				<?php if($success == '') : ?>
				<?php else: ?>
					<?php $this->load->view('submit_success') ?>
				<?php endif; ?>

				<div class="form-group">
					<label class="col-md-4 control-label" for="prof_pub_title">Judul Publikasi</label>
					<div class="col-md-8">
						<input name="prof_pub_title" value="<?php echo set_value('prof_pub_title', $pub->pub_title) ?>" class="sm-form-control" type="text">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="prof_pub_author">Penulis Tambahan (jika ada)</label>
					<div class="col-md-8">
						<input name="prof_pub_author" value="<?php echo set_value('prof_pub_author',$pub->pub_partner) ?>" class="sm-form-control" type="text">
						<em class="text-muted">Penulis selain anda, pisahkan dengan koma</em>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="prof_pub_type">Jenis</label>
					<div class="col-md-8">
						<?php
						$type = array('Jurnal', 'Proceding', 'Buku', 'Lainnya');
						echo form_dropdown('prof_pub_type', $type, set_value('prof_pub_type',$pub->pub_type), 'class="sm-form-control"');
						?>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="prof_pub_isbn">ISBN / ISSN</label>
					<div class="col-md-8">
						<input name="prof_pub_isbn" value="<?php echo set_value('prof_pub_isbn',$pub->pub_isbn) ?>" class="sm-form-control" type="text">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="prof_pub_year">Tahun Terbit</label>
					<div class="col-md-8">
						<input name="prof_pub_year" value="<?php echo set_value('prof_pub_year',$pub->pub_year) ?>" class="sm-form-control" type="text">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="prof_pub_abstract">Abstrak</label>
					<div class="col-md-8">
						<?php echo form_textarea('prof_pub_abstract', set_value('prof_pub_abstract',$pub->pub_abstract), 'class="sm-form-control"' ) ?>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="userfile">Upload Berkas ( maks. <?php echo $max_size?> )</label>
					<div class="col-md-8">
						<?php echo form_upload('userfile', set_value('userfile'), 'class="sm-form-control"' ) ?>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="prof_pub_link">Tautan ( jika ada )</label>
					<div class="col-md-8">
						<input name="prof_pub_link" value="<?php echo set_value('prof_pub_link',$pub->pub_link) ?>" class="sm-form-control" type="text">
					</div>
				</div>

				<div class="form-group nobottommargin">
					<?php echo form_hidden('member_id', $pub->member_id);?>
					<?php echo form_hidden('pub_id', $pub->pub_id);?>
					<div class="text-right col-md-12">
						<div id="button1idGroup" class="btn-group" role="group" aria-label="Button Group">
							<button type="submit" class="button button-rounded button-black" id="form-submit" name="submit" value="submit">Simpan</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
