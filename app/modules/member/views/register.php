<section class="container clearfix topmargin">
    <div class="col_two_third clearfix">
        <?php if(!validation_errors()) : ?>
        <div class="panel panel-default noshadow">
            <div class="panel-heading">
                <h3 class="nobottommargin" style="text-transform: uppercase">Formulir Pendaftaran</h3>
            </div>
            <div class="panel-body">
                <p class="nobottommargin">
                    Mohon lengkapi formulir pendaftaran berikut ini.<br/>
                    Pastikan alamat email masih dapat dihubungi oleh panitia.<br/>
                    <strong style="color:red">Seluruh kolom wajib terisi.</strong>
                </p>
            </div>
        </div>
        <?php else: ?>
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Galat</h3>
            </div>
            <div class="panel-body">
                <ul class="iconlist" style="line-height: 2;">
                    <?php echo validation_errors(); ?>
                </ul>
            </div>
        </div>
        <?php endif; ?>
        <!-- FORM PENDAFTARAN BEGIN -->
        <?php echo form_open(current_url(), 'class="nobottommargin" id="register-form" name="register-form"') ?>
            <div class="panel panel-default noshadow">
                <div class="panel-body">
                    <div class="form-group">
                        <label for="register-form-name">Nama Lengkap <small>( Lengkapi gelar jika ada )</small></label>
                        <input type="text" name="reg_name" value="<?php echo set_value('reg_name') ?>" class="required sm-form-control" />
                    </div>
                    <div class="form-group">
                        <label for="register-form-premail">Email</label>
                        <input type="text" name="reg_premail" value="<?php echo set_value('reg_premail') ?>" class="required sm-form-control" />
                    </div>
                    <div class="form-group">
                        <label for="register-form-mail">Konfirmasi Email</label>
                        <input type="text" name="reg_mail" value="<?php echo set_value('reg_mail') ?>" class="required sm-form-control" />
                    </div>
                </div>
                
                <div class="text-center panel-footer">
                    Dengan menekan tombol <strong>DAFTAR</strong> dibawah ini,<br />
                    maka saya menyetujui segala <a href="<?php echo site_url('read/syarat-anggota') ?>">Persyaratan &amp; Ketentuan</a> yang telah berlaku.<br />
                    <br />
                    <button type="button" class="button button-3d button-rounded button-white button-light" onclick="javascript:window.history.back()" value="batal">Batal</button>
                    <button type="submit" class="button button-3d button-rounded button-black" id="register-form-submit" name="submit" value="submit">Daftar</button>
                </div>
            </div>
        </form>
        <!-- FORM PENDAFTARAN END -->
    </div>
    <?php $this->load->view('sidebar') ?>
</section>