    <div class="col_one_third col_last">
        <h3>Informasi</h3>
        <p>Bagi anggota APTIPI diharapkan dapat melengkapi data-data keanggotaan pada laman profil.</p>
        <ul class="iconlist" style="line-height:1.8;">
            <li><i class="icon-line-circle-check"></i> Data Pribadi</li>
            <li><i class="icon-line-circle-check"></i> Riwayat Akademik</li>
            <li><i class="icon-line-circle-check"></i> Riwayat Pekerjaan</li>
            <li><i class="icon-line-circle-check"></i> Hasil Penelitian</li>
            <li><i class="icon-line-circle-check"></i> Informasi Lain</li>
        </ul>
        <p>
            Seluruh data akan dijamin kerahasiaannya.<br>Data yang ditampilkan pada laman website APTIPI hanya sebagian informasi dan profil singkat anggota member APTIPI, yang dianggap perlu dan layak untuk ditampilkan.
        </p>
    </div>
