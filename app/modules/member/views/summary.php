<?php $type = array('Jurnal', 'Proceding', 'Buku', 'Lainnya'); if(count($form) > 0): ?>
<section class="container clearfix topmargin">
    <div class="postcontent nobottommargin clearfix">
        <div id="posts" class="small-thumbs">
            <div class="row">
                <div class="col-md-12">
                    <h2>Detail Anggota</h2>
                </div>
                <div class="col-md-4">
                <div class="panel panel-default noshadow">
                    <div class="panel-body">
                        <img src="<?php if($form->avatar == NULL){ echo base_url(). PERSON . '/person.png'; } else { echo base_url(). PERSON .'/'.$form->avatar; } ?>" class="img-thumb" />                                    
                    </div>
                </div>
                </div>
                <div class="col-md-8">
                    <table class="table" cellspacing="35">
                        <tbody>
                            <tr>
                                <th width="25%">Nama Lengkap</th>
                                <td width="75%"><?php echo $form->realname ?> </td>
                            </tr>
                            <tr>
                                <th>Ahli Bidang</th>
                                <td><?php echo $form->ahli_bid ?> </td>
                            </tr>
                            <tr>
                                <th>Website</th>
                                <td><?php echo $form->website ?></td>
                            </tr>
                            <tr>
                                <th>Facebook</th>
                                <td><a href="https://fb.me/<?php echo $form->socmed_fb ?>"><?php echo $form->socmed_fb ?></a></td>
                            </tr>
                            <tr>
                                <th>Twitter</th>
                                <td><a href="https://twitter.com/<?php echo $form->socmed_fb ?>"><?php echo $form->socmed_fb ?></a></td>
                            </tr>
                        </tbody>
                    </table>
                    <p><a href="<?php echo site_url('member') ?>" class="button button-3d button-black nomargin">Back</a></p>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('archives/sidebar') ?>
</section>
<?php else: ?>
<?php //show_404() ?>
<?php endif ?>