		<div class="panel panel-default noshadow">
			<div class="panel-body">
				<?php echo form_open_multipart(current_url(), 'class="form-horizontal nobottommargin" id="biodata-form" name="biodata-form"') ?>
				<?php if($custom_error == '') : ?>
				<h4 style="text-transform: uppercase">Riwayat Pengabdian</h4>
				Mohon lengkapi formulir riwayat pengabdian berikut ini.
				<hr />
				<?php else: ?>
					<?php $this->load->view('submit_error') ?>
				<?php endif; ?>
				<?php if($success == '') : ?>
				<?php else: ?>
					<?php $this->load->view('submit_success') ?>
				<?php endif; ?>

				<div class="form-group">
					<label class="col-md-4 control-label" for="prof_abdi_title">Nama Kegiatan</label>
					<div class="col-md-8">
						<input name="prof_abdi_title" value="<?php echo set_value('prof_abdi_title', $abdi->abdi_title) ?>" class="sm-form-control" type="text">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="prof_abdi_partner">Rekan Kerja ( jika ada )</label>
					<div class="col-md-8">
						<input name="prof_abdi_partner" value="<?php echo set_value('prof_abdi_partner',$abdi->abdi_partner) ?>" class="sm-form-control" type="text">
						<em class="text-muted">Pisahkan dengan koma</em>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="prof_abdi_year">Tahun Kegiatan</label>
					<div class="col-md-8">
						<input name="prof_abdi_year" value="<?php echo set_value('prof_abdi_year',$abdi->abdi_year) ?>" class="sm-form-control" type="text">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="prof_abdi_stake">Stakeholder</label>
					<div class="col-md-8">
						<input name="prof_abdi_stake" value="<?php echo set_value('prof_abdi_stake',$abdi->abdi_stake) ?>" class="sm-form-control" type="text">
					</div>
				</div>

				<div class="form-group nobottommargin">
					<?php echo form_hidden('member_id', $abdi->member_id);?>
					<?php echo form_hidden('abdi_id', $abdi->abdi_id);?>
					<div class="text-right col-md-12">
						<div id="button1idGroup" class="btn-group" role="group" aria-label="Button Group">
							<button type="submit" class="button button-rounded button-black" id="form-submit" name="submit" value="submit">Simpan</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
