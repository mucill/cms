<section class="container clearfix topmargin">
	<div class="col_two_third clearfix">
		<?php if($custom_error == '') : ?>
		<h3 style="text-transform: uppercase;">Login Anggota</h3>
		<?php else: ?>
		<div class="alert alert-danger">		
			<h3 class="nobottommargin" style="text-transform: uppercase;">Galat</h3>
			<ul class="iconlist" style="line-height: 2;">
				<?php echo $custom_error; ?>
			</ul>
		</div>
		<?php endif; ?>
		<!-- FORM LOGIN BEGIN -->
		<?php echo form_open(current_url(), 'class="form-horizontal nobottommargin" id="login-form" name="login-form"') ?>
			<div class="panel panel-default noshadow">
				<div class="panel-body">
					<div class="form-group">
						<label for="textinput" class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
						<input name="reg_mail" value="<?php echo set_value('reg_mail') ?>" class="required sm-form-control" type="text">
						</div>
					</div>
					
					<div class="form-group">
						<label for="textinput" class="col-sm-2 control-label">Password</label>
						<div class="col-sm-10">
						<input name="reg_pass" value="<?php echo set_value('reg_pass') ?>" class="required sm-form-control" type="password">
						</div>
					</div>
				</div>
				
				<div class="text-center panel-footer">
					<div class="form-group">
						<div id="button1idGroup" class="btn-group" role="group" aria-label="Button Group">
							<button type="submit" class="button button-3d button-black" id="login-form-submit" name="submit" value="submit">Login</button>
						</div>
					</div>
				</div>
				
			</div>
			<a href="<?php echo site_url('member/register'); ?>" class="button button-3d button-rounded button-red" >Pendaftaran</a>
			<a href="<?php echo site_url('member/forgot'); ?>" class="button button-3d button-rounded button-white button-light" >Lupa Password</a>
		</form>
		<!-- FORM LOGIN END -->
	</div>
		<?php $this->load->view('sidebar') ?>
</section>