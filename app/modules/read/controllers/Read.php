<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Read extends MX_Controller {

    public $data            = array();
    private $perpage        = 1;

    public function __construct()
    {
        parent::__construct();

        // page title
        $this->data['title'] = __('summary', false);

        // set default language
        $this->media->set_language();

        // for debug purpose only
        $this->output->enable_profiler(false);
    }

    public function index($slug = '')
    {
            // most view
            $this->data['view_most']      = $this->media->blog_view_most();
            $this->data['view_latest']    = $this->media->blog_view_latest();

            // galeri
            $this->data['gallery']        = $this->media->images_list(4, 5); // $cat_id, $limit
            $this->data['footer_gallery'] = $this->media->images_list(4, 6); // $cat_id, $limit

        // get current id
        $this->db
        ->select('id')
        ->where('slug', $slug);
        $query  = $this->db->get('cms_page_lang');
        $curr   = $query->row();

        // get all data
        $this->db
        ->select('a.id, a.title, a.body, a.slug, b.created_at, b.media_id, b.updated_at, b.users_id')
        ->from('cms_page_lang a')
        ->join('cms_page b', 'b.id = a.id')
        ->where('a.lang', $this->session->userdata('lang'))
        ->where('a.id', $curr->id)
        ->limit($this->perpage);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {

            // generate output
            $this->data['category']     = $this->media->category_list('blogs');
            $this->data['blog']         = $query->row();
            $this->data['content']      = $this->load->view('list', $this->data, true);

        } else {
            // if no data
            $this->data['message']      = __('blank', false);
            $this->data['content']      = $this->load->view('master/blank', $this->data, true);
        }

        // Generate output
        $this->load->view('master/content', $this->data);
    }

}
