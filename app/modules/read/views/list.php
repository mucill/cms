<section class="container clearfix topmargin">
    <div class="postcontent nobottommargin notopmmargin clearfix">
        <div class="single-post nobottommargin">
            <div class="entry clearfix noborder">
                <div class="entry-title">
                    <h2><?php echo $blog->title ?></h2>
                </div>

                <ul class="entry-meta clearfix">
                    <li><i class="icon-calendar3"></i> <?php echo mdate('%d.%m.%Y', strtotime($blog->updated_at)) ?></li>
                    <li><i class="icon-user"></i> <?php echo $this->media->author($blog->users_id) ?></li>
                </ul>

                <?php if($blog->media_id != 0): ?>
                <div class="entry-image">
                    <img class="image_fade" src="<?php echo site_url('media/') .'/'. $this->media->images($blog->media_id) ?>" alt="<?php echo $blog->title ?>">
                </div>
                <?php endif ?>

                <div class="entry-content notopmargin">
                    <?php echo auto_typography($blog->body) ?>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('archives/sidebar') ?>
</section>