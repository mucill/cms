<?php $type = array('Jurnal', 'Proceding', 'Buku', 'Lainnya'); if(count($form) > 0): ?>
<section class="container clearfix topmargin">
    <div class="postcontent nobottommargin clearfix">
        <div id="posts" class="small-thumbs">
            <div class="row">
                <div class="col-md-12">
                    <h2>Detail <?php echo $type[$form->pub_type] ?></h2>
                    <table class="table">
                        <tbody>
                            <tr>
                                <th width="25%">Judul</th>
                                <td width="75%"><?php echo $form->pub_title ?> </td>
                            </tr>
                            <tr>
                                <th>Penulis</th>
                                <td><?php echo (($form->realname != '') ? $form->realname . '<br>' : '') . $form->pub_partner ?> </td>
                            </tr>
                            <tr>
                                <th>ISBN</th>
                                <td><?php echo $form->pub_isbn ?></td>
                            </tr>
                            <tr>
                                <th>Tahun Terbit</th>
                                <td><?php echo $form->pub_year ?></td>
                            </tr>
                            <tr>
                                <th>Tautan</th>
                                <td><a href="<?php echo $form->pub_link ?>"><?php echo $form->pub_link ?></a></td>
                            </tr>
                            <tr>
                                <th>Unduh</th>
                                <td>
                                <?php if($form->pub_file != '') : ?>
                                <a href="<?php echo site_url('proceeding/download') .'/' . $form->pub_id ?>">Download</a></td>
                                <?php else : ?>
                                <em><?php echo __('not_available') ?></em> 
                                <?php endif ?>   
                            </tr>
                        </tbody>
                    </table>
                    <p><em><?php echo nl2br($form->pub_abstract) ?></em>
                    </p>
                    <p>                        
                    <a href="<?php echo site_url('proceeding') ?>" class="button button-3d button-black nomargin">Back</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('archives/sidebar') ?>
</section>
<?php else: ?>
<?php //show_404() ?>
<?php endif ?>