<?php if(count($member) > 0): ?>
<section class="container clearfix topmargin">
    <div class="postcontent nobottommargin clearfix">
        <div id="posts" class="small-thumbs">
            <div class="row">
                <div class="col-md-12">
                    <form action="<?php echo current_url() ?>" class="form" method="post">
                        <div class="col-md-6">
                            <input type="text" name="q" class="sm-form-control" placeholder="Keyword ..." />
                            <em>You can search uses part of title or abstract</em>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="button button-3d button-black nomargin">Search</button>
                            <?php if(isset($_POST['q'])) : ?>
                            <a href="<?php echo current_url() ?>" class="button button-3d button-black nomargin">Clear Filter</a>
                            <?php endif ?>
                        </div>
                    </form>
                <br>
                <br>
                <br>
                </div>
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Judul</th>
                                <th>Penulis</th>
                                <th class="text-right">Detail</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            if($this->uri->segment(2) != '') $i = (int)$this->uri->segment(2) + 1;
                            $color = array('info', 'danger', 'success', 'warning');
                            foreach ($member as $list) : ?>
                            <tr>
                                <td><?php echo $i++; ?> </td>
                                <td><div class="label label-<?php echo $color[$list->pub_type] ?>">
                                <?php echo $this->type[$list->pub_type] ?></div>
                                <a href="<?php echo site_url('proceeding/detail') .'/' . $list->pub_id ?> ">
                                <?php echo highlight_phrase($list->pub_title, (isset($_POST['q']) ? $_POST['q'] : ''), '<strong style="color: red">', '</strong>') ?></td>
                                </a>
                                <td><?php echo highlight_phrase((($list->realname != '') ? $list->realname . '<br>' : '' ) . $list->pub_partner, (isset($_POST['q']) ? $_POST['q'] : ''), '<strong style="color: red">', '</strong>') ?></td>
                                <td class="text-right">
                                    <a href="<?php echo site_url('proceeding/detail') .'/' . $list->pub_id ?> "><i class="ion-document"></i></a>
                                </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php echo $pagination ?>
        </div>
    </div>
    <?php $this->load->view('archives/sidebar') ?>
</section>
<?php else: ?>
<?php //show_404() ?>
<?php endif ?>
