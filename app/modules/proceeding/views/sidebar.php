<!-- Sidebar
============================================= -->
<div class="sidebar nobottommargin col_last clearfix">
    <div class="sidebar-widgets-wrap">
        <div class="widget clearfix">
            <h4><?php __('news_populer') ?></h4>
            <div id="popular-post-list-sidebar">                
                <?php foreach($view_most as $list): ?>
                <div class="spost clearfix">
                    <div class="entry-image">
                        <a href="<?php echo base_url('/read').'/'.$list->slug ?>"><img class="img-circle" src="<?php echo site_url('media/') .'/'. $this->media->images($list->media_id) ?>" alt="<?php echo $list->title ?>"></a>
                    </div>
                    <div class="entry-c">
                        <div class="entry-title">
                            <h4><a href="<?php echo base_url('/read') .'/'. $list->slug ?>"><?php echo $list->title ?></a></h4>
                        </div>
                        <ul class="entry-meta clearfix">
                            <li><i class="icon-eye-open"></i> <?php echo $list->view ?></li>
                        </ul>

                    </div>
                </div>
                <?php endforeach ?>
            </div>
        </div>

        <div class="widget clearfix">
            <h4><?php __('news_latest') ?></h4>
            <div id="popular-post-list-sidebar">
                <?php foreach($view_latest as $list): ?>
                <div class="spost clearfix">
                    <div class="entry-image">
                        <a href="<?php echo base_url('/read').'/'.$list->slug ?>"><img class="img-circle" src="<?php echo site_url('media/') .'/'. $this->media->images($list->media_id) ?>" alt="<?php echo $list->title ?>"></a>
                    </div>
                    <div class="entry-c">
                        <div class="entry-title">
                            <h4><a href="<?php echo base_url('/read') .'/'. $list->slug ?>"><?php echo $list->title ?></a></h4>
                        </div>
                        <ul class="entry-meta clearfix">
                            <li><i class="icon-calendar3"></i> <?php echo mdate('%d.%m.%Y', strtotime($list->updated_at)) ?></li>
                        </ul>

                    </div>
                </div>
            <?php endforeach ?>


            </div>
        </div>

    </div>

</div><!-- .sidebar end -->