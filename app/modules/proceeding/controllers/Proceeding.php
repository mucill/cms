<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proceeding extends MX_Controller {

    public $data                = array();
    private $url                = 'proceeding';
    private $perpage            = 10;
    public $type               = array('Jurnal', 'Proceding', 'Buku', 'Lainnya');

    public function __construct()
    {
        parent::__construct();

        // load model
        $this->load->model(array('auth_model','CrudMod'));
        $this->load->helper(array('download', 'path'));

        // set default language
        $this->media->set_language();

        // set default value
        $this->data['title']    = __('proceeding', false);

        // for debug purpose only
        $this->output->enable_profiler(false);

    }

    public function index($offset = 0)
    {
        // pagination
        $this->db
        ->select('member_id')
        ->from('member_publikasi')
        ->order_by('pub_title');

        if(isset($_POST['q']) && ($_POST['q'] != '')) {
            $this->db
            ->like('pub_title', $_POST['q'])
            ->or_like('pub_abstract', $_POST['q']);
        }

        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('a.pub_id, a.pub_title, a.pub_abstract, b.realname, a.pub_partner, a.pub_type ')
        ->from('member_publikasi a')
        ->join('member b', 'b.member_id = a.member_id', 'left')
        ->limit($this->perpage, $offset )
        ->order_by('a.pub_title');

        if(isset($_POST['q']) && ($_POST['q'] != '')) {
            $this->db
            ->like('pub_title', $_POST['q'])
            ->or_like('pub_abstract', $_POST['q']);
        }

        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {

            // most view
            $this->data['view_most']        = $this->media->blog_view_most();
            $this->data['view_latest']      = $this->media->blog_view_latest();

            // galeri
            $this->data['gallery']          = $this->media->images_list(4, 5); // $cat_id, $limit
            $this->data['footer_gallery']   = $this->media->images_list(4, 6); // $cat_id, $limit

            // generate output
            $this->data['member']           = $query->result();
            $this->data['content']          = $this->load->view('list', $this->data, true);

        } else {

            $this->data['message']          = __('blank', false);
            $this->data['content']          = $this->load->view('master/blank', $this->data, true);

        }


        // Generate output
        $this->load->view('master/content', $this->data);
    }

    public function detail($id = null)
    {

        // get all data
        $this->db
        ->select('a.*, b.realname, a.pub_partner')
        ->from('member_publikasi a')
        ->join('member b', 'b.member_id = a.member_id', 'left')
        ->where('a.pub_id', $id);
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {

            // most view
            $this->data['view_most']        = $this->media->blog_view_most();
            $this->data['view_latest']      = $this->media->blog_view_latest();

            // galeri
            $this->data['gallery']          = $this->media->images_list(4, 5); // $cat_id, $limit
            $this->data['footer_gallery']   = $this->media->images_list(4, 6); // $cat_id, $limit

            // generate output
            $this->data['form']             = $query->row();
            $this->data['content']          = $this->load->view('summary', $this->data, true);

        } else {

            // if no data
            $this->data['footer_gallery']   = $this->media->images_list(4, 6);
            $this->data['message']          = __('blank', false);
            $this->data['content']          = $this->load->view('master/blank', $this->data, true);

        }
        // Generate output
        $this->load->view('master/content', $this->data);
    }

    public function download($id = null)
    {
        $this->db->where('pub_id', $id);
        $query = $this->db->get('member_publikasi');
        if($query->num_rows() > 0 ) {
            $file = $query->row();
            force_download(REPOSITORY . '/' . $file->pub_file, NULL);
            echo 'c';
        } else {
            $this->data['message']          = __('blank', false);
            $this->data['content']          = $this->load->view('master/blank', $this->data, true);
            $this->load->view('master/content', $this->data);
        }
    }

}
