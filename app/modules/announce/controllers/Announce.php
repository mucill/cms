<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Announce extends MX_Controller {

    public $data            = array();
    private $url            = 'announce';
    private $perpage        = 5;

    public function __construct() 
    {   
        parent::__construct();

        // page title
        $this->data['title'] = __('announcement', false) ;

        // set default language
        $this->media->set_language();
    }

    public function index($offset = 0)
    {
        // pagination 
        $this->db
        ->select('a.id')
        ->from('page_lang a')
        ->join('page b', 'b.id = a.id')
        ->where('b.category_id', 9)
        ->where('a.lang', $this->session->userdata('lang'));
        $query = $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('a.id, a.title, a.intro, a.slug, b.created_at, b.media_id, b.updated_at, b.users_id')
        ->from('page_lang a')
        ->join('page b', 'b.id = a.id')
        ->where('b.category_id', 9)
        ->where('a.lang', $this->session->userdata('lang'))
        ->limit($this->perpage, $offset );
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {
            // generate output
            $this->data['blog']         = $query->result();
            $this->data['content']      = $this->load->view('list', $this->data, true);
        } else {
            // if no data
            $this->data['message']      = __('blank', false);
            $this->data['content']      = $this->load->view('master/blank', $this->data, true);                    
        }

        // Generate output
        $this->data['footer_gallery']   = $this->media->images_list(4, 6); // $cat_id, $limit
        $this->load->view('master/content', $this->data);
    }

}
