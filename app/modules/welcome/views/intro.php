<section id="section-about" class="page-section">
    <div class="container clearfix">
        <div class="col_one_third nobottommargin">
            <div class="feature-box media-box">
                <div class="fbox-media"><img src="<?php echo base_url(). UPLOADS ?>/p2.jpg" alt=""></div>
                <div class="fbox-desc">
                    <h3><?php __('membership_title') ?></h3>
                    <p><?php __('membership_info') ?></p>
                    <p><a href="<?php echo site_url('read/tentang') ?>" class="more-link"><?php __('detail') ?></a></p>
                </div>
            </div>
        </div>
        <div class="col_one_third nobottommargin">
            <div class="feature-box media-box">
                <div class="fbox-media"><img src="<?php echo base_url(). UPLOADS ?>/p1.jpg" alt=""></div>
                <div class="fbox-desc">
                    <h3><?php __('news_title') ?></h3>
                    <p><?php __('news_info') ?></p>
                    <p><a href="<?php echo site_url('archives') ?>" class="more-link"><?php __('detail') ?></a></p>
                </div>
            </div>
        </div>
        <div class="col_one_third nobottommargin col_last">
            <div class="feature-box media-box">
                <div class="fbox-media"><img src="<?php echo base_url(). UPLOADS ?>/p3.jpg" alt=""></div>
                <div class="fbox-desc">
                    <h3><?php __('share_title') ?></h3>
                    <p><?php __('share_info') ?></p>
                    <p><a href="<?php echo site_url('read/tentang') ?>" class="more-link"><?php __('detail') ?></a></p>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</section>
