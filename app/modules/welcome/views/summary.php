<section id="news-title" class="section notopmargin nobottommargin" >
    <div class="container clearfix">
        <div class="heading-block center notopmargin nobottommargin noborder core-heading-block">
            <h2><?php __('news') ?></h2>
        </div>
    </div>
</section>
<section id="news-summary" class="page-section topmargin nobottommargin noborder" >
    <div class="container clearfix">
        <div id="posts" class="post-grid post-masonry grid-3 clearfix">
        <?php foreach($data as $blog): ?>
            <div class="entry clearfix noborder">
                <?php if($blog->media_id != 0): ?>
                <div class="entry-image">
                    <a href="<?php echo site_url('/read').'/'.$blog->slug ?>" style="height: 175px; overflow: hidden;"><img class="image_fade" src="<?php echo site_url('media/') .'/'. $this->media->images($blog->media_id) ?>" alt="<?php echo $blog->title ?>"></a>
                </div>
                <?php endif ?>
                <div class="entry-title">
                    <h2><a href="<?php echo site_url('/read/'.$blog->slug) ?>"><?php echo $blog->title ?></a></h2>
                </div>
                <ul class="entry-meta clearfix">
                    <li><i class="icon-calendar3"></i> <?php echo mdate('%d.%m.%Y', strtotime($blog->updated_at)) ?></li>
                    <li><i class="icon-user"></i> <?php echo $this->media->author($blog->users_id) ?></li>
                </ul>
                <div class="entry-content">
                    <p><?php echo word_limiter($blog->intro, 20) ?></p>
                    <a href="<?php echo site_url('/read'.'/'.$blog->slug) ?>"class="more-link">Selengkapnya</a>
                </div>
            </div>
        <?php endforeach ?>
        </div>
    </div>
</section>
