<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MX_Controller {

	public $data = array();

	public function __construct() 
	{	
		parent::__construct();		

        // page title
        $this->data['title'] = __('home', false);

        // set default language
    	$this->media->set_language();

        // for debug purpose only
        $this->output->enable_profiler(false);
	}

	public function index()
	{

		$this->data['data'] 			= $this->media->blog_view_latest();
        $this->data['footer_gallery']   = $this->media->images_list(4, 6);
        $this->data['slider']           = $this->load->view('welcome/slider', '', true);
        $this->data['blog']             = $this->load->view('welcome/summary', $this->data, true);

		$this->data['content'] 			= $this->load->view('welcome', $this->data, true);

		// Generate output
		$this->load->view('master/content', $this->data);
	}
}
