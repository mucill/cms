<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends MX_Controller {

    public $data            = array();
    private $url            = 'gallery';
    private $perpage        = 5;

	public function __construct() 
	{	
		parent::__construct();		

        // page title
        $this->data['title'] = __('gallery', false);

        // set default language
        $this->media->set_language();

		// for debug purpose only
		$this->output->enable_profiler(false);
	}

	public function index($offset = 0)
	{

        // pagination 
        $this->db
        ->select('a.id')
        ->from('media a')
        ->join('media_lang b', 'b.id = a.id')
        ->where('a.category_id', 3)
		->where('b.lang', $this->session->userdata('lang'));
        $query 						= $this->db->get();
        $count                      = $query->num_rows();
        $config['per_page']         = $this->perpage;
        $this->pagination->initialize($this->general_model->pagination_rules(site_url($this->url), $count, $this->perpage));
        $this->data['pagination']   = $this->pagination->create_links();

        // get all data
        $this->db
        ->select('a.id, b.alt')
        ->from('media a')
        ->join('media_lang b', 'b.id = a.id')
        ->where('a.category_id', 3)
		->where('b.lang', $this->session->userdata('lang'))
        ->limit($this->perpage, $offset );
        $query = $this->db->get();

        // if not empty
        if($query->num_rows() > 0) {
            // generate output
            $this->data['galleries'] 	= $query->result();
            $this->data['content']      = $this->load->view('list', $this->data, true);
        } else {
            // if no data
            $this->data['message']      = __('blank', false);
            $this->data['content']      = $this->load->view('master/blank', $this->data, true);                    
        }

		// $this->data['galleries'] 		= $query->result();

		// // Get Most View
		// $this->data['view_most'] 		= $this->media->blog_view_most();
		// $this->data['view_latest'] 		= $this->media->blog_view_latest();

		// // Get Galeri
		// $this->data['gallery'] 			= $this->media->images_list(4, 5); // $cat_id, $limit
		$this->data['footer_gallery']	= $this->media->images_list(4, 6); // $cat_id, $limit
		

		// Generate output
		$this->load->view('master/content', $this->data);
	}

	public function term()
	{
		$view['content'] = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero laudantium neque ab quidem nobis quas delectus ipsa sequi consequatur vitae laborum modi temporibus facere, omnis quam beatae blanditiis quaerat animi.';
		$this->load->view('master/blank', $view);
	}

    public function media($alias)
    {
        $this->db
        ->where('alias', $alias);
        $query = $this->db->get('media');
        if($query->num_rows() > 0 ) {
            $read   = $query->row();
            $mime   = $read->type;
            $path   = MEDIA . DIRECTORY_SEPARATOR . $read->filename;
        } else {
            $path   = MEDIA . DIRECTORY_SEPARATOR . 'blank.png';            
            $mime   = 'image/png';
        }
        header("Content-Type: ".$mime);
        readfile($path);
        exit;
    }
}
