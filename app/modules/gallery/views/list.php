<section class="container clearfix topmargin">
    <div class="postcontent nobottommargin notopmmargin clearfix">
        <div class="single-post nobottommargin">
            <div class="entry clearfix noborder">
                <div class="masonry-thumbs col-6" data-big="3" data-lightbox="gallery">
                <?php foreach($galleries as $pic) : ?>
                <a href="<?php echo site_url('media/') .'/'. $this->media->images($pic->id) ?>" data-lightbox="gallery-item">
                <img class="image_fade" src="<?php echo site_url('media/') .'/'. $this->media->images($pic->id) ?>" alt="<?php echo $pic->alt ?>">
                </a>
                <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
</section>

