<?php if(count($lists) > 0) : ?>
<div class="list-group">
    <?php foreach($lists as $list) : ?>
    <a href="<?php echo site_url('message/read/'.$list->id) ?>" class="list-group-item row">
        <?php if($list->status == 0 && $this->uri->segment(2) != 'outbox') : ?><strong><?php endif ?>
        <div class="col-md-3"><?php echo $list->name ?></div>
        <div class="col-md-9"><?php echo $list->subject ?></div>
        <?php if($list->status == 0) : ?></strong><?php endif ?>
    </a>
    <?php endforeach ?>
</div>
<?php else : ?>
No Message Founded.
<?php endif ?>
