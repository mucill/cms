<div class="panel panel-default noshadow">
    <div class="panel-body ">
        <?php echo form_open(current_url(), 'class="form-horizontal" role="form"')?>
            <div class="form-group">
                <label class="col-md-2" style="padding-top:10px;">To</label>
                <div class="col-md-10">
                <?php echo form_hidden('to', $to_id) ?>
                <div class="form-control"><?php echo $to ?></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2" style="padding-top:10px;">From</label>
                <div class="col-md-10">                    
                <?php echo form_hidden('from', $from_id) ?>
                <div class="form-control"><?php echo $from ?></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2" style="padding-top:10px;">Subject</label>
                <div class="col-md-10">          
                <input type="text" name="subject" class="noshadow form-control" value="<?php echo $subject ?>">        
                <?php echo form_error('subject','<small class="text-danger">', '</small>'); ?>
                </div>
            </div>
            <div class="form-group">
            <div class="col-md-12">  
            <textarea name="message" class="noshadow form-control" cols="30" rows="5"><?php echo $message ?></textarea>
            <?php echo form_error('message','<small class="text-danger">', '</small>'); ?>
            </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-primary">Send Message</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(".chosen-select").chosen();
    $('input[type="text"], textarea, .chosen-container').on('click', function(){
        $(this).next().hide();
    })
</script>