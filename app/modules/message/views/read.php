<div class="panel panel-default noshadow">
    <div class="panel-body ">
        <?php echo form_open(current_url(), 'class="form-horizontal" role="form"')?>
            <div class="form-group">
                <label class="col-md-2" style="padding-top:10px;">From</label>
                <div class="col-md-10">
                    <div class="form-control noshadow"><?php echo $to ?></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2" style="padding-top:10px;">To</label>
                <div class="col-md-10">
                    <div class="form-control noshadow"><?php echo $from ?></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2" style="padding-top:10px;">Subject</label>
                <div class="col-md-10">                
                    <div class="form-control noshadow"><?php echo $subject ?></div>
                </div>
            </div>
            <div class="form-group">
            <div class="col-md-12">  
            <textarea rows="5" class="form-control noshadow" readonly="readonly" style="cursor:default; background-color: #fff;"><?php echo $message ?></textarea>
            </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <?php if($this->session->userdata('realname') === $from) : ?>
                    <a href="<?php echo site_url('message/reply/'.$this->uri->segment(3)) ?>" class="btn btn-primary">Reply</a>
                    <?php else : ?>
                    <button type="button" class="btn btn-primary" onClick="history.back()">Back</button>
                    <?php endif ?>
                </div>
            </div>
        </form>
    </div>
</div>
