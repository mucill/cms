<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Langs extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function setup($langs = '', $current_url = '')
    {
        $this->session->set_userdata('lang', $langs);
        $uri = str_replace('.', '/', $current_url);
        redirect(site_url($uri), 'location', 301);
    }

}

/* End of file Langs.php */
