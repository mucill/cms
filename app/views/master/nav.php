<header id="header">
<div id="header-wrap">
    <div class="container clearfix">
        <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
        <div id="logo">
            <a href="<?php echo base_url() ?>">
                <img class="tm-logo" src="<?php echo base_url('assets')?>/img/logo.png" alt="<?php echo get_conf('site_name') ?>">
                <div class="tm-logo-title"><?php echo get_conf('site_name') ?><br><div><?php echo get_conf('site_tagline') ?></div></div>
            </a>
        </div>
        <nav id="primary-menu">
        <?php echo widget_nav('main') ?>
        </nav>
    </div>
</div>
</header>