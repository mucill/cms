<?php
header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<title><?php echo get_conf('site_name') ?></title>
<meta charset="utf-8" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="<?php echo get_conf('site_tagline') ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="description" content="<?php echo get_conf('meta_description') ?>">
<meta name="robots" content="index, follow">
<link rel="icon" type="image/x-icon" href="<?php echo base_url('favicon.ico') ?>" />
<link rel="stylesheet" href="<?php echo base_url() . CSS ?>bootstrap.css" />
<link rel="stylesheet" href="<?php echo base_url() . CSS ?>style.css?<?php echo date('this') ?>" />
<link rel="stylesheet" href="<?php echo base_url() . CSS ?>font-icons.css" />
<link rel="stylesheet" href="<?php echo base_url() . CSS ?>animate.css" />
<link rel="stylesheet" href="<?php echo base_url() . CSS ?>magnific-popup.css" />
<link rel="stylesheet" href="<?php echo base_url() . CSS ?>responsive.css" />
<link rel="stylesheet" href="<?php echo base_url() . CSS ?>dark.css" />

<link rel="stylesheet" href="<?php echo base_url() . PLUGINS ?>ionicons-min/css/ionicons.min.css" />
<link rel="stylesheet" href="<?php echo base_url() . PLUGINS ?>chosen/chosen.min.css" />
<link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url() . CSS ?>custom.css?<?php echo date('this') ?>" />
<!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
<script type="text/javascript" src="<?php echo base_url() . JS ?>jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url() . PLUGINS ?>chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() . JS ?>plugins.js"></script>
</head>
<body class="stretched no-transition">

    <div id="wrapper" class="clearfix">
        <div id="top-bar" class="hidden-xs">
            <div class="container clearfix">
                <div class="col_half nobottommargin">
                    <p class="nobottommargin"><strong>Hubungi kami:</strong> <?php echo get_conf('phone') ?> | <strong>Email:</strong> <?php echo get_conf('email')  ?></p>
                </div>
                <div class="col_half col_last fright nobottommargin">
                    <!-- Top Links
                    ============================================= -->
                    <div class="top-links">
                        <ul>
                            <li><a href="#">Login</a>
                                <div class="top-link-section">
                                    <form id="top-login" role="form">
                                        <div class="input-group" id="top-login-username">
                                            <span class="input-group-addon"><i class="icon-user"></i></span>
                                            <input type="email" class="form-control" placeholder="Email address" required="">
                                        </div>
                                        <div class="input-group" id="top-login-password">
                                            <span class="input-group-addon"><i class="icon-key"></i></span>
                                            <input type="password" class="form-control" placeholder="Password" required="">
                                        </div>
                                        <label class="checkbox">
                                          <input type="checkbox" value="remember-me"> Remember me
                                        </label>
                                        <button class="btn btn-danger btn-block" type="submit">Sign in</button>
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </div><!-- .top-links end -->

                </div>

            </div>

        </div>

        <?php $this->load->view('master/nav') ?>

        <?php if($this->uri->segment(1) != '') : ?>
        <section id="page-title" class="nomargin page-title-dark" style="background: transparent url(<?php echo base_url() . UPLOADS ?>/slider-1.jpg) center center no-repeat; background-size: cover;">
            <div class="container clearfix">
                <h1><?php echo $title ?></h1>
            </div>
        </section>
        <?php endif ?>

        <?php if(! $this->uri->segment(1)) $this->load->view('slider') ?>

        <section id="content">
            <?php echo $content ?>
        </section>


        <footer id="footer" class="dark noborder">
            <div class="container">
                <?php get_widget('widget-1') ?>
            </div>

            <!-- Copyrights
            ============================================= -->
            <div id="copyrights">

                <div class="container clearfix">

                    <div class="col_half">
                        &copy; <?php echo get_conf('site_tagline') ?> <?php echo date('Y') ?> <br><?php __('share') ?>
                    </div>

                    <div class="col_half col_last tright">
                        <div class="fright clearfix">
                            <?php __('maintenance_by') ?><br><?php echo get_conf('site_tagline') ?>
                        </div>
                    </div>

                </div>

            </div><!-- #copyrights end -->

        </footer><!-- #footer end -->
    </div>

    <div id="gotoTop" class="icon-angle-up"></div>
    <script type="text/javascript" src="<?php echo base_url('assets/js/functions.js') ?>"></script>
</body>
</html>
