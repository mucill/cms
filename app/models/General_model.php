<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General_model extends CI_Model
{

    public function __construct() {
        parent::__construct();
    }
    
    public function get($table, $fields, $where, $join, $group, $perpage=0, $start=0, $order='', $one=false) 
    {        
        $this->db->select($fields)
                 ->from($table);

        if($perpage) {
            $this->db->limit($perpage, $start);
        }

        if($join) {
            $this->db->join($join[0], $join[1], $join[2]);
        }

        if($where) {    
            $this->db->like($where);
        }

        if($group) {    
            $this->db->group_by($group);
        }

        if($order) {
            $this->db->order_by($order);
        }
            
        $query  = $this->db->get();
        $result =!$one ? $query->result() : $query->row() ;
        // debug($result);
        return $result;
    }
    
    public function add($table, $data) 
    {
        $this->db->insert($table, $data);         
        if ($this->db->affected_rows() == '1') {
			return TRUE;
		}		
		return FALSE;       
    }
    
    public function edit($table, $data, $fieldID, $ID)
    {
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) {
			return TRUE;
		}
		
		return FALSE;       
    }
    
    public function delete($table, $fieldID, $ID)
    {
        $this->db->where($fieldID,$ID);
        $this->db->delete($table);
        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;        
    }   
	
	public function count($table, $join, $where, $group){
        // debug($where);
        if($join) {
            $this->db->join($join);
        }
        if($where) {
            $this->db->where($where);
        }
        if($group) {
            $this->db->group_by($group);
        }
        $get_count = $this->db->get($table);
        return $get_count->num_rows();
	}

    public function pagination_rules($uri, $count, $perpage = 10)
    {
        $config = array(
            'base_url'          => $uri,
            'per_page'          => $perpage,
            'total_rows'        => $count,
            'full_tag_open'     => '<ul class="pagination">',
            'first_tag_open'    => '<li>',
            'last_tag_open'     => '<li>',
            'next_tag_open'     => '<li>',
            'prev_tag_open'     => '<li>',
            'cur_tag_open'      => '<li class="active"><a href="#">',
            'num_tag_open'      => '<li>',
            'full_tag_close'    => '</ul>',
            'first_tag_close'   => '</li>',
            'last_tag_close'    => '</li>',
            'next_tag_close'    => '</li>',
            'prev_tag_close'    => '</li>',
            'cur_tag_close'     => '</a></li>',
            'num_tag_close'     => '</li>',
            'anchor_class'      => 'class="btn btn-inverse"'
        );

        return $config;        
    }
}