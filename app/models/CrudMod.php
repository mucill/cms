<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CrudMod extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getProfx($memID)
	{
		$this->db->select('m.*, ma.*, md.*, me.*, mj.*, mo.*, mp.*, mr.*');
		$this->db->from('member as m');
		$this->db->join('member_abdi as ma', 'ma.member_id = m.member_id', 'left');
		$this->db->join('member_data as md', 'md.member_id = m.member_id', 'left');
		$this->db->join('member_edu as me', 'me.member_id = m.member_id', 'left');
		$this->db->join('member_jabatan as mj', 'mj.member_id = m.member_id', 'left');
		$this->db->join('member_office as mo', 'mo.member_id = m.member_id', 'left');
		$this->db->join('member_publikasi as mp', 'mp.member_id = m.member_id', 'left');
		$this->db->join('member_riset as mr', 'mr.member_id = m.member_id', 'left');
		$this->db->where('m.member_id', $memID);
		
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		
		return false;
	}
	
	public function getData($data)
	{
		$this->db->select('m.*, ma.*, md.*, me.*, mj.*, mo.*, mp.*, mr.*');
		$this->db->from('member as m');
		$this->db->where('m.email', $data['email']);
		$this->db->join('member_abdi as ma', 'ma.member_id = m.member_id', 'left');
		$this->db->join('member_data as md', 'md.member_id = m.member_id', 'left');
		$this->db->join('member_edu as me', 'me.member_id = m.member_id', 'left');
		$this->db->join('member_jabatan as mj', 'mj.member_id = m.member_id', 'left');
		$this->db->join('member_office as mo', 'mo.member_id = m.member_id', 'left');
		$this->db->join('member_publikasi as mp', 'mp.member_id = m.member_id', 'left');
		$this->db->join('member_riset as mr', 'mr.member_id = m.member_id', 'left');
		
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach ($query->result() as $data) {
				$getData['member_id'] = $data;
			}
			return $getData;
		}
	}
	
	public function getProf($data)
	{
		$this->db->select('m.*, md.*');
		$this->db->from('member as m');
		$this->db->where('m.email', $data['email']);
		$this->db->join('member_data as md', 'md.member_id = m.member_id', 'left');
		
		$query = $this->db->get();
		if ($query->num_rows() >0){
			return $query->row();
		}
	}
	
	public function getProfID($data)
	{
		$this->db->select('m.*, md.*');
		$this->db->from('member as m');
		$this->db->where('m.email', $data['email']);
		$this->db->join('member_data as md', 'md.member_id = m.member_id', 'left');
		
		$query = $this->db->get();
		if ($query->num_rows() >0){
			foreach ($query->result() as $data) {
				$getProfID[] = $data;
			}
			return $getProfID;
		}
	}
	
	public function addBio()
	{	
		$nama 	= array('realname' => $this->input->post('prof_nama'));		
		$this->db->where('member_id', $this->input->post('member_id'));
		$this->db->update('member', $nama);
		
		$biodata = array(
			'member_id'		=> $this->input->post('member_id'),
			'birth_city'	=> $this->input->post('prof_birth_city'),
			'birth_date'	=> $this->input->post('prof_birth_date'),
			'gender'		=> $this->input->post('prof_gender'),
			'home_addr'		=> $this->input->post('prof_home_addr'),
			'home_city'		=> $this->input->post('prof_home_city'),
			'home_prov'		=> $this->input->post('prof_home_prov'),
			'phone'			=> $this->input->post('prof_phone'),
			'ahli_bid'		=> $this->input->post('prof_ahli'),
			'website'		=> $this->input->post('prof_web'),
			'socmed_fb'		=> $this->input->post('prof_fb'),
			'socmed_tw'		=> $this->input->post('prof_tw'),
			'last_update'	=> date('Y-m-d H:i:s')
		);
		
		$query = $this->db->get_where('member_data', array('member_id' => $this->input->post('member_id')), 1);
		$result = $query->result();		

		if ($count->num_rows() > 0) {			
			$insert = $this->db->update('member_data', $biodata);
			$this->db->query($insert);
			return $this->db->insert_id();
			
		} else {
			$insert = $this->db->insert('member_data', $biodata);
			$this->db->query($insert);
			return $this->db->insert_id();
		}
	}
	
	public function getAvatar($data)
	{
		$this->db->select('m.*, md.*');
		$this->db->from('member as m');
		$this->db->where('m.email', $data['email']);
		$this->db->join('member_data as md', 'md.member_id = m.member_id', 'left');
		
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
	}
	
	public function addJab()
	{
		$biodata = array(
			'member_id'		=> $this->input->post('member_id'),
			'jbt_type'		=> $this->input->post('prof_jbt_type'),
			'jbt_office'	=> $this->input->post('prof_jbt_offc'),
			'jbt_periode'	=> $this->input->post('prof_jbt_year'),
			'last_update'	=> date('Y-m-d H:i:s')
		);
		
		$insert = $this->db->insert('member_jabatan', $biodata);
		$this->db->query($insert);
		return $this->db->insert_id();
	}
	
	public function getJab($data)
	{
		$this->db->select('m.*, md.*, mj.*');
		$this->db->from('member as m');
		$this->db->where('m.email', $data['email']);
		$this->db->join('member_data as md', 'md.member_id = m.member_id', 'left');
		$this->db->join('member_jabatan as mj', 'mj.member_id = m.member_id', 'left');
		
		$query = $this->db->get();
		if ($query->num_rows() >0){
			foreach ($query->result() as $data) {
				$getJab[] = $data;
			}
			return $getJab;
		}
	}
	
	public function getJabID($jabID)
	{
		$this->db->select('m.*, mj.*');
		$this->db->from('member as m');
		$this->db->join('member_jabatan as mj', 'mj.member_id = m.member_id', 'left');
		$this->db->where('mj.jabatan_id', $jabID);
		$query = $this->db->get();
		if ($query->num_rows() >0){
			foreach ($query->result() as $jabID) {
				$getJabID[] = $jabID;
			}
			return $getJabID;
		}
	}
	
	public function updJab($jabID, $data)
	{
		$this->db->where('jabatan_id', $jabID);
		$this->db->update('member_jabatan', $data);
	}
}
