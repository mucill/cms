<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Media {

    public function __construct()
    {
        // Assign the CodeIgniter super-object
        $this->ci =& get_instance();

    }

    public function images($id)
    {
        $query = $this->ci->db->where('id', $id)
        ->limit(1)
        ->get('cms_media');
        if($query->num_rows() > 0) {
            $data = $query->row();
            return $data->alias;
        } else {
            return 0;
        }
    }

    public function images_list($cat_id, $limit)
    {
        $query = $this->ci->db->where('media_lang.lang', $this->ci->session->userdata('lang'))
        ->where('media.category_id', $cat_id)
        ->join('media_lang', 'media_lang.id = media.id')
        ->limit($limit)
        ->get('media');
        $data = $query->result();
        return $data;
    }

    public function author($id)
    {
        $query = $this->ci->db->where('id', $id)
        ->limit(1)
        ->get('cms_users');
        $data = $query->row();
        return $data->first_name . '' . $data->last_name ;
    }

    public function category($id)
    {
        $query = $this->ci->db->where('id', $id)
        ->where('lang', $this->ci->session->userdata('lang'))
        ->where('id', $id)
        ->limit(1)
        ->get('cms_category_lang');
        $data = $query->row();
        return $data->name;
    }

    public function category_slug($id)
    {
        $query = $this->ci->db->where('id', $id)
        ->where('lang', $this->ci->session->userdata('lang'))
        ->where('id', $id)
        ->limit(1)
        ->get('cms_category_lang');
        $data = $query->row();
        return $data->slug;
    }

    public function category_list($slug = '')
    {
        $data = array();
        $query = $this->ci->db
        ->from('cms_category_lang a', true)
        ->join('cms_category b', 'b.id = a.id','left')
        ->where('type', 'post')
        ->where('lang', $this->ci->session->userdata('lang'))
        ->get();
        if(count($query->result()) > 0) {
            foreach($query->result() as $list) {
                $data[] = array('slug' => $list->slug, 'name' => $list->name);
            }
            return $data;
        } else {
            return false;
        }
    }

    public function blog_view_most()
    {
        $query = $this->ci->db->where('page_lang.lang', $this->ci->session->userdata('lang'))
        ->where('page.type','post')
        ->join('page','page.id = page_lang.id')
        ->limit(5)
        ->order_by('view', 'DESC')
        ->get('page_lang');
        $data = $query->result();
        return $data;
    }

    public function blog_view_latest()
    {
        $query = $this->ci->db->where('page_lang.lang', $this->ci->session->userdata('lang'))
        ->where('page.type','post')
        ->join('page','page.id = page_lang.id')
        ->limit(5)
        ->order_by('page.updated_at', 'DESC')
        ->get('page_lang');
        $data = $query->result();
        return $data;
    }

    public function avail_language()
    {
        // set default language
        $this->ci->db
        ->where('shortname <>', $this->ci->session->userdata('lang'));
        $query = $this->ci->db->get('cms_lang');
        return $query->result();
    }

    public function set_language()
    {
        // set default language
        $this->ci->db->where('is_default', 1);
        $query = $this->ci->db->get('cms_lang');
        $lang  = $query->row();
        if(null == $this->ci->session->userdata('lang')) {
            $this->ci->session->set_userdata(array('lang' => $lang->shortname ));
        }
    }
}
